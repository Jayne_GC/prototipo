#include <iostream>
#include <cstring>
#include <stdio.h>
#include <regex>
#include <vector>
#include <fstream>

using namespace std;
ofstream flow_file;
ofstream file_YOYO;

class Atributo{
   private:
        string atributo_nome;
        string atributo_id;
        string atributo_nome_classe;
   public:
        vector<string> atributo_outros_nomes;
        /* Constructor */
        Atributo(){ atributo_nome=""; atributo_id=""; atributo_nome_classe=""; atributo_outros_nomes.clear(); }
        /* atributo_nome */
        string get_atributo_nome(){ return atributo_nome;};
        void set_atributo_nome(string nome){ atributo_nome = nome;};
        /* atributo_id */
        string get_atributo_id(){ return atributo_id;};
        void set_atributo_id(string id){ atributo_id = id;};
        /* atributo_nome_classe */
        string get_atributo_nome_classe(){ return atributo_nome_classe;};
        void set_atributo_nome_classe(string nome){ atributo_nome_classe = nome;};
        /* atributo_outros_nomes */
        vector<string> get_atributo_outros_nomes(){ return atributo_outros_nomes;};
        void clear_atributo_outros_nomes(){ atributo_outros_nomes.clear();};
        void add_atributo_outros_nomes(string new_element){ atributo_outros_nomes.push_back(new_element);};
};

class Objeto{
   private:
        string objeto_nome;
        string objeto_classe;
        int objeto_ordem;
   public:
        /* Constructor */
        Objeto(){ objeto_nome=""; objeto_classe=""; objeto_ordem=0;}
        /* objeto_nome */
        string get_objeto_nome(){ return objeto_nome;};
        void set_objeto_nome(string nome){ objeto_nome = nome;};
        /* objeto_classe */
        string get_objeto_classe(){ return objeto_classe;};
        void set_objeto_classe(string classe){ objeto_classe = classe;};
        /* objeto_ordem */
        int get_objeto_ordem(){ return objeto_ordem;};
        void set_objeto_ordem(int ordem){ objeto_ordem = ordem;};
};

class Def_ou_Uso{
   private:
        char def_ou_uso_tipo; /* D para def, U para uso, C para chamadas e P para vtable calls */
        int def_ou_uso_ordem;
        string def_ou_uso_objeto_nome;
        string def_ou_uso_objeto_classe;
   public:
        Atributo *def_ou_uso_atributo;
        /* Constructor */
        Def_ou_Uso(){   def_ou_uso_tipo='N'; def_ou_uso_ordem = -1;
                        def_ou_uso_objeto_nome = "";
                        def_ou_uso_objeto_classe = "";
                        def_ou_uso_atributo = NULL;}
        /* def_ou_uso_tipo */
        char get_def_ou_uso_tipo(){ return def_ou_uso_tipo;};
        void set_def_ou_uso_tipo(char tipo){ def_ou_uso_tipo = tipo;};
        /* def_ou_uso_ordem */
        int get_def_ou_uso_ordem(){ return def_ou_uso_ordem;};
        void set_def_ou_uso_ordem(int ordem){ def_ou_uso_ordem = ordem;};
        /* def_ou_uso_objeto_nome */
        string get_def_ou_uso_objeto_nome(){ return def_ou_uso_objeto_nome;};
        void set_def_ou_uso_objeto_nome(string nome){ def_ou_uso_objeto_nome = nome;};
        /* def_ou_uso_objeto_classe */
        string get_def_ou_uso_objeto_classe(){ return def_ou_uso_objeto_classe;};
        void set_def_ou_uso_objeto_classe(string nome){ def_ou_uso_objeto_classe = nome;};
};

class Chamada : public Def_ou_Uso{
    private:
        string chamada_nome_classe_chamada;
        string chamada_nome_metodo_chamado;
        string chamada_vfn_table_id;
        string chamada_vfn_table_posicao;
    public:
        Chamada(){
            chamada_nome_classe_chamada = "";
            chamada_nome_metodo_chamado = "";
            chamada_vfn_table_id = "";
            chamada_vfn_table_posicao = "";
        }
        /* chamada_nome_classe_chamada */
        string get_chamada_nome_classe_chamada(){ return chamada_nome_classe_chamada;};
        void set_chamada_nome_classe_chamada(string nome){ chamada_nome_classe_chamada = nome;};
        /* chamada_nome_metodo_chamado */
        string get_chamada_nome_metodo_chamado(){ return chamada_nome_metodo_chamado;};
        void set_chamada_nome_metodo_chamado(string nome){ chamada_nome_metodo_chamado = nome;};
        /* chamada_vfn_table_id */
        string get_chamada_vfn_table_id(){ return chamada_vfn_table_id;};
        void set_chamada_vfn_table_id(string nome){ chamada_vfn_table_id = nome;};
        /* chamada_vfn_table_posicao */
        string get_chamada_vfn_table_posicao(){ return chamada_vfn_table_posicao;};
        void set_chamada_vfn_table_posicao(string nome){ chamada_vfn_table_posicao = nome;};
};

class Metodo{
    private:
        string metodo_nome;
        string metodo_classe_nome;
    public:
        vector<Chamada> metodo_chamadas;
        vector<Def_ou_Uso> metodo_usos;
        vector<Def_ou_Uso> metodo_defs;
        vector<Objeto> metodo_objetos;
        /* Constructor */
        Metodo(){ metodo_nome="No name"; metodo_defs.clear(); metodo_usos.clear(); metodo_chamadas.clear(); metodo_objetos.clear();}
        /* metodo_nome */
        string get_metodo_nome(){ return metodo_nome;};
        void set_metodo_nome(string nome){ metodo_nome = nome;};
        /* metodo_classe_nome */
        string get_metodo_classe_nome(){ return metodo_classe_nome;};
        void set_metodo_classe_nome(string nome){ metodo_classe_nome = nome;};
        /* metodo_defs */
        vector<Def_ou_Uso> get_metodo_defs(){ return metodo_defs;};
        void clear_metodo_defs(){ metodo_defs.clear();};
        void add_metodo_defs(Def_ou_Uso new_element){ metodo_defs.push_back(new_element);};
        /* metodo_usos */
        vector<Def_ou_Uso> get_metodo_usos(){ return metodo_usos;};
        void clear_metodo_usos(){ metodo_usos.clear();};
        void add_metodo_usos(Def_ou_Uso new_element){ metodo_usos.push_back(new_element);};
        /* metodo_chamadas */
        vector<Chamada> get_metodo_chamadas(){ return metodo_chamadas;};
        void clear_metodo_chamadas(){ metodo_chamadas.clear();};
        void add_metodo_chamadas(Chamada new_element){ metodo_chamadas.push_back(new_element);};
        /* metodo_objetos */
        vector<Objeto> get_metodo_objetos(){ return metodo_objetos;};
        void clear_metodo_objetos(){ metodo_objetos.clear();};
        void add_metodo_objetos(Objeto new_element){ metodo_objetos.push_back(new_element);};
        /* Output */
        vector<string> output_remove_repetidos(vector<vector<string>> vtable, int posicao){
            vector<string> saida;
            /* copiar metodos da posicao desejada */
            for(int i = 0; i < vtable.size(); i++){
                saida.push_back(vtable[i][posicao]);
            }
            /* para cada item, remover todos os iguais */
            for(int i = 0; i < saida.size(); i++){
                for(int j = i+1; j < saida.size(); j++){
                    if(j < saida.size()){
                        if(saida[i] == saida[j]){
                            saida.erase(saida.begin() + j);
                        }
                    }
                }
            }
            return saida;
        }
        void show_all_methods(vector<Metodo> todos_os_metodos, int espacos, vector<vector<string>> vtable, string objeto_classe){
            for(int i=0; i < espacos; i++){ flow_file << "\t";}
            flow_file << "-> " << this->metodo_nome << " (" << this->metodo_classe_nome << ")" << "\n";
            if(espacos > 50){flow_file << "LOOP..."; return;}
            for(int i = 0; i != this->metodo_chamadas.size(); i++){
                int found = 0;
                if( this->metodo_chamadas[i].get_def_ou_uso_tipo() == 'P' ){
                    /* Incluir chamada polimorfica - salvar objeto inicial */
                    int valor_k = 0;
                    found = 0;
                    string posicao = this->metodo_chamadas[i].get_chamada_vfn_table_posicao();
                    int maior = 0;
                    for(int k = 0; k != vtable.size(); k++){
                        if(vtable[k][0] == objeto_classe){valor_k = k;}
                        if(maior < vtable[k].size()){maior = vtable[k].size();}
                    }
                    for(int j = 0; j != maior && found == 0; j++){
                        if(to_string(j) == posicao){
                            for(int m = 0; m != todos_os_metodos.size() && found == 0; m++){
                                if(vtable[valor_k][j+1] == todos_os_metodos[m].get_metodo_nome()){
                                    todos_os_metodos[m].show_all_methods(todos_os_metodos,(espacos+1),vtable,objeto_classe);
                                    found = 1;
                                 }
                            }
                        }
                    }
                }else{
                    for(int j = 0; j != todos_os_metodos.size() && found == 0; j++){
                        if(this->metodo_chamadas[i].get_chamada_nome_metodo_chamado() == todos_os_metodos[j].get_metodo_nome()){
                            if(this->metodo_chamadas[i].get_def_ou_uso_objeto_nome() != "this_value"){
                                    objeto_classe = this->metodo_chamadas[i].get_def_ou_uso_objeto_classe();
                            }
                            todos_os_metodos[j].show_all_methods(todos_os_metodos,(espacos+1),vtable,objeto_classe);
                            found = 1;
                        }
                    }
                }
            }
        }
        void show_all_methods_no_poli(vector<Metodo> todos_os_metodos, int espacos, vector<vector<string>> vtable){
            for(int i=0; i < espacos; i++){ file_YOYO << "\t";}
            file_YOYO << "-> " << this->metodo_nome << " (" << this->metodo_classe_nome << ")" << "\n";
            if(espacos > 50){file_YOYO << "LOOP...\n"; return;}
            for(int i = 0; i != this->metodo_chamadas.size(); i++){
                if(this->metodo_chamadas[i].get_def_ou_uso_tipo() == 'P'){
                    /* Incluir chamada polimorfica */
                    vector<string> poli_methods;
                    int posicao = 1 + atoi(this->metodo_chamadas[i].get_chamada_vfn_table_posicao().c_str());
                    poli_methods = output_remove_repetidos(vtable,posicao);
                    if(poli_methods.size() > 0){
                            for(int i=0; i < espacos + 1; i++){ file_YOYO << "\t";}
                            file_YOYO << "-> P (";
                    }
                    for(int i = 0; i < poli_methods.size(); i++){
                        file_YOYO << poli_methods[i];
                        if(i == poli_methods.size()-1){file_YOYO << ")\n";}
                        else{file_YOYO << " ";}
                    }
                    for(int i = 0; i < poli_methods.size(); i++){
                        bool found = false;
                        for(int m = 0; m != todos_os_metodos.size() && !found; m++){
                            if(poli_methods[i] == todos_os_metodos[m].get_metodo_nome()){
                                todos_os_metodos[m].show_all_methods_no_poli(todos_os_metodos,(espacos+2),vtable);
                                found = true;
                            }
                        }
                    }
                }else{
                    bool found = false;
                    for(int j = 0; j != todos_os_metodos.size() && !found; j++){
                        if(this->metodo_chamadas[i].get_chamada_nome_metodo_chamado() == todos_os_metodos[j].get_metodo_nome()){
                            todos_os_metodos[j].show_all_methods_no_poli(todos_os_metodos,(espacos+1),vtable);
                            found = true;
                        }
                    }
                }
            }
        }
};

class Classe {
   private:
        string classe_nome;
   public:
        vector<Atributo> classe_atributos;
        vector<Metodo> classe_metodos;
        vector<string> classe_pais;
        //vector<string> classe_filhos;
        vector<string> classe_vtable;
        /* Constructor */
        Classe(){}
        /* classe_nome */
        string get_classe_nome(){ return classe_nome;};
        void set_classe_nome(string nome){ classe_nome = nome;};
        /* classe_atributos */
        vector<Atributo> get_classe_atributos(){ return classe_atributos;};
        void clear_classe_atributos(){ classe_atributos.clear();};
        void add_classe_atributos(string name, string id){
            /* Se a classe ainda n�o possui atributos, incluir novo */
            if(classe_atributos.empty()){
                Atributo novo_atributo;
                novo_atributo.set_atributo_nome(name);
                novo_atributo.set_atributo_id(id);
                novo_atributo.set_atributo_nome_classe(this->get_classe_nome());
                classe_atributos.push_back(novo_atributo);
            }
            else{
                /* Se a classe possui atributos, testar se id j� existe */
                int is_in_the_list = 0;
                for(int j = 0; j != classe_atributos.size(); j++){
                    if(classe_atributos[j].get_atributo_id() == id){
                            /* ID ja existe */
                            is_in_the_list = 1;
                            /* Ver se name ja existe na lista de outros nomes */
                            int is_in_the_list_outros = 0;
                            if(classe_atributos[j].get_atributo_nome() == name){is_in_the_list_outros = 1;}
                            for(int k = 0; k != classe_atributos[j].atributo_outros_nomes.size(); k++){
                                if(classe_atributos[j].atributo_outros_nomes[k] == name){is_in_the_list_outros = 1;}
                            }
                            if(is_in_the_list_outros == 0){ classe_atributos[j].add_atributo_outros_nomes(name); }
                    }
                }
                if(is_in_the_list == 0){
                    /* ID ainda nao existe, incluir elemento */
                    Atributo novo_atributo;
                    novo_atributo.set_atributo_nome(name);
                    novo_atributo.set_atributo_id(id);
                    novo_atributo.set_atributo_nome_classe(this->get_classe_nome());
                    classe_atributos.push_back(novo_atributo);
                }
            }
        };
        /* classe_metodos */
        vector<Metodo> get_classe_metodos(){ return classe_metodos;};
        void clear_classe_metodos(){ classe_metodos.clear();};
        void add_classe_metodos(string new_element){
            Metodo novo_metodo;
            novo_metodo.set_metodo_nome(new_element);
            novo_metodo.clear_metodo_defs();
            novo_metodo.clear_metodo_usos();
            novo_metodo.clear_metodo_chamadas();
            novo_metodo.clear_metodo_objetos();
            novo_metodo.set_metodo_classe_nome(classe_nome);
            classe_metodos.push_back(novo_metodo);
        };
        /* classe_pais */
        vector<string> get_classe_pais(){ return classe_pais;};
        void clear_classe_pais(){ classe_pais.clear();};
        void add_classe_pais(string new_element){ classe_pais.push_back(new_element);};
        /* classe_filhos
        vector<string> get_classe_filhos(){ return classe_filhos;};
        void clear_classe_filhos(){ classe_filhos.clear();};
        void add_classe_filhos(string new_element){ classe_filhos.push_back(new_element);};*/
        /* classe_vtable */
        vector<string> get_classe_vtable(){ return classe_vtable;};
        void clear_classe_vtable(){ classe_vtable.clear();};
        void add_classe_vtable(string new_element){ classe_vtable.push_back(new_element);};
};

class Output{
   private:
   public:
        /* Constructor */
        Output(){}
        /* All */
        bool output_compare_DU(vector<Def_ou_Uso> vetor_metodo_1, vector<Def_ou_Uso> vetor_metodo_2){
            if(vetor_metodo_1.size() != vetor_metodo_2.size()){
                return false;
            }else{
                for(int l = 0; l < vetor_metodo_1.size(); l++){
                    if(vetor_metodo_1[l].get_def_ou_uso_tipo() != vetor_metodo_2[l].get_def_ou_uso_tipo()){
                        return false;
                    }else
                    if(vetor_metodo_1[l].get_def_ou_uso_objeto_nome() != vetor_metodo_2[l].get_def_ou_uso_objeto_nome()){
                        return false;
                    }else
                    if(vetor_metodo_1[l].get_def_ou_uso_tipo() == 'D' || vetor_metodo_1[l].get_def_ou_uso_tipo() == 'U'){
                        if(vetor_metodo_1[l].def_ou_uso_atributo != vetor_metodo_2[l].def_ou_uso_atributo){
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        bool output_compare_Chamada(vector<Chamada> vetor_metodo_1, vector<Chamada> vetor_metodo_2){
            if(vetor_metodo_1.size() != vetor_metodo_2.size()){
                return false;
            }else{
                for(int l = 0; l < vetor_metodo_1.size(); l++){
                    if(vetor_metodo_1[l].get_def_ou_uso_tipo() != vetor_metodo_2[l].get_def_ou_uso_tipo()){
                        return false;
                    }else
                    if(vetor_metodo_1[l].get_def_ou_uso_objeto_nome() != vetor_metodo_2[l].get_def_ou_uso_objeto_nome()){
                        return false;
                    }else
                    if(vetor_metodo_1[l].get_def_ou_uso_tipo() == 'P'){
                        if(vetor_metodo_1[l].get_chamada_vfn_table_posicao() != vetor_metodo_2[l].get_chamada_vfn_table_posicao()){
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        void output_print_DU(vector<Def_ou_Uso> vetor_DU, ofstream **file_new, int espacos, bool com_obj, bool com_ordem){
            for(int l = 0; l < vetor_DU.size(); l++){
                for(int i=0;i<espacos;i++){ **file_new << "\t"; }
                if(vetor_DU[l].get_def_ou_uso_tipo() == 'U'){**file_new << "Tipo: Uso - ";}
                else if(vetor_DU[l].get_def_ou_uso_tipo() == 'D'){**file_new << "Tipo: Defini��o - ";}

                if(vetor_DU[l].def_ou_uso_atributo != NULL && (vetor_DU[l].get_def_ou_uso_tipo() == 'D' || vetor_DU[l].get_def_ou_uso_tipo() == 'U')){
                    **file_new << "   \tAtributo: " << vetor_DU[l].def_ou_uso_atributo->get_atributo_nome();
                }
                if(com_obj){**file_new << "   \tObjeto_nome: " << vetor_DU[l].get_def_ou_uso_objeto_nome();
                            **file_new << "   \tObjeto_classe: " << vetor_DU[l].get_def_ou_uso_objeto_classe();
                }
                if(com_ordem){**file_new << "   \tOrdem: " << vetor_DU[l].get_def_ou_uso_ordem(); }
                **file_new << "\n";
            }
        }
        void output_print_Chamada(vector<Chamada> vetor_DU, ofstream **file_new, int espacos, bool com_obj, bool com_ordem){
            for(int l = 0; l < vetor_DU.size(); l++){
                for(int i=0;i<espacos;i++){ **file_new << "\t"; }
                if(vetor_DU[l].get_def_ou_uso_tipo() == 'C'){**file_new << "Tipo: Chamada - ";}
                else if(vetor_DU[l].get_def_ou_uso_tipo() == 'P'){**file_new << "Tipo: Chamada Polim�rfica - ";}

                if(vetor_DU[l].get_def_ou_uso_tipo() == 'P'){
                    **file_new << "   \tPosicao_na_tabela_de_polimorfismo: " << vetor_DU[l].get_chamada_vfn_table_posicao();
                }
                else if(vetor_DU[l].get_def_ou_uso_tipo() == 'C'){
                    **file_new << "   \tMetodo_Chamado: " << vetor_DU[l].get_chamada_nome_metodo_chamado();
                    **file_new << "   \tMetodo_Chamado_Classe: " << vetor_DU[l].get_chamada_nome_classe_chamada();
                }
                if(com_obj){**file_new << "   \tObjeto_nome: " << vetor_DU[l].get_def_ou_uso_objeto_nome();
                            **file_new << "   \tObjeto_classe: " << vetor_DU[l].get_def_ou_uso_objeto_classe();
                }
                if(com_ordem){**file_new << "   \tOrdem: " << vetor_DU[l].get_def_ou_uso_ordem(); }
                **file_new << "\n";
            }
        }
        void output_print_1_DU (Def_ou_Uso DU, ofstream **file_new, int espacos, bool com_obj, bool com_ordem){
            for(int i=0;i<espacos;i++){ **file_new << "\t"; }
            if(DU.get_def_ou_uso_tipo() == 'U'){**file_new << "Tipo: Uso - ";}
            else if(DU.get_def_ou_uso_tipo() == 'D'){**file_new << "Tipo: Defini��o - ";}

            if(DU.def_ou_uso_atributo != NULL && (DU.get_def_ou_uso_tipo() == 'D' || DU.get_def_ou_uso_tipo() == 'U')){
                **file_new << "   \tAtributo: " << DU.def_ou_uso_atributo->get_atributo_nome();
                **file_new << "   \tAtributo Classe: " << DU.def_ou_uso_atributo->get_atributo_nome_classe();
            }
            if(com_obj){**file_new << "   \tObjeto_nome: " << DU.get_def_ou_uso_objeto_nome();
                        **file_new << "   \tObjeto_classe: " << DU.get_def_ou_uso_objeto_classe();
            }
            if(com_ordem){**file_new << "   \tOrdem: " << DU.get_def_ou_uso_ordem(); }
            **file_new << "\n";
        }
        void output_print_1_Chamada (Chamada DU, ofstream **file_new, int espacos, bool com_obj, bool com_ordem){
            for(int i=0;i<espacos;i++){ **file_new << "\t"; }
            if(DU.get_def_ou_uso_tipo() == 'C'){**file_new << "Tipo: Chamada - ";}
            else if(DU.get_def_ou_uso_tipo() == 'P'){**file_new << "Tipo: Chamada Polim�rfica - ";}

            if(DU.get_def_ou_uso_tipo() == 'P'){
                **file_new << "   \tPosicao_na_tabela_de_polimorfismo: " << DU.get_chamada_vfn_table_posicao();
            }
            else if(DU.get_def_ou_uso_tipo() == 'C'){
                **file_new << "   \tMetodo_Chamado: " << DU.get_chamada_nome_metodo_chamado();
                **file_new << "   \tMetodo_Chamado_Classe: " << DU.get_chamada_nome_classe_chamada();
            }
            if(com_obj){**file_new << "   \tObjeto_nome: " << DU.get_def_ou_uso_objeto_nome();
                        **file_new << "   \tObjeto_classe: " << DU.get_def_ou_uso_objeto_classe();
            }
            if(com_ordem){**file_new << "   \tOrdem: " << DU.get_def_ou_uso_ordem(); }
            **file_new << "\n";
        }
        void output_print_OBJ(vector<Objeto> vetor_OBJ, ofstream **file_new, int espacos){
            for(int l = 0; l < vetor_OBJ.size(); l++){
                for(int i=0;i<espacos;i++){ **file_new << "\t"; }
                **file_new << vetor_OBJ[l].get_objeto_nome()
                           << "   \tObjeto_classe: " << vetor_OBJ[l].get_objeto_classe() << "\n";
            }
        }
        void output_print_ATR(vector<Atributo> vetor_ATR, ofstream **file_new, int espacos, bool com_outros_nomes){
            for(int l = 0; l < vetor_ATR.size(); l++){
                for(int i=0;i<espacos;i++){ **file_new << "\t"; }
                **file_new << vetor_ATR[l].get_atributo_nome() << "\n";
                if(com_outros_nomes){
                    for(int k = 0; k != vetor_ATR[l].atributo_outros_nomes.size(); k++){
                        for(int i=0;i<espacos+1;i++){ **file_new << "\t"; }
                        **file_new << vetor_ATR[l].atributo_outros_nomes[k] << "\n";
                    }
                }
            }
        }
        /* ITU */
        vector<Classe> output_find_relatives_only(Classe filho, vector<Classe> all_classes, bool first){
            vector<Classe> all_relatives;
            if(!first){all_relatives.push_back(filho);}
            for(int i=0;i<filho.classe_pais.size();i++){
                for(int j=0;j<all_classes.size();j++){
                    if(all_classes[j].get_classe_nome() == filho.classe_pais[i]){
                        vector<Classe> tmp;
                        tmp = output_find_relatives_only(all_classes[j],all_classes,false);
                        all_relatives.reserve(all_relatives.size()+tmp.size());
                        all_relatives.insert(all_relatives.end(),tmp.begin(),tmp.end());
                    }
                }
            }
            return all_relatives;
        }
        bool output_classe_eh_parente(vector<Classe> all_classes, Classe filho, string nome_pai){
            vector<Classe> all_parents = output_find_relatives_only(filho,all_classes,true);
            for(int i=0;i < all_parents.size();i++){
                if(all_parents[i].get_classe_nome() == nome_pai){
                    return true;
                }
            }
            return false;
        }
        bool output_pertence_a_classe_parente(vector<Classe> all_classes, Classe filho, string nome_metodo){
            vector<Classe> all_parents = output_find_relatives_only(filho,all_classes,true);
            for(int i=0;i < all_parents.size();i++){
                for(int j=0;j < all_parents[i].classe_metodos.size();j++){
                    if(all_parents[i].classe_metodos[j].get_metodo_nome() == nome_metodo){
                        return true;
                    }
                }
            }
            return false;
        }
        void output_check_ITU(vector<Classe> all_classes,vector<vector<string>> vtable, ofstream *file_ITU){
            /* ITU - Para cada par pai e filho, exibir para todos os metodos defs, uses e chamadas */
            *file_ITU << "ITU - USO DE TIPO INCONSISTENTE\n";
            *file_ITU << "� uma anomalia que pode ocorrer quando o relacionamento de heran�a � usado e contextos nos quais n�o existe uma rela��o de subtipo entre as classes pai e filha.\n";
            *file_ITU << "SA�DA: Para cada classe existente, s�o exibidos todos os seus m�todos que efetuam chamadas para m�todos de classes herdadas.\n";
            *file_ITU << "TESTE: Para cada chamada entre m�todos descrita abaixo, verificar se o objeto da classe herdeira � usado no contexto da classe herdada.\n";
            /* imprimir chamadas que chamam metodos dos pais --------- */
            bool encontrei_pelo_menos_1_caso = false;
            for(int i=0;i < all_classes.size();i++){ bool new_classe = true;
                for(int j=0;j < all_classes[i].classe_metodos.size();j++){ bool new_metodo = true;
                    for(int k=0;k < all_classes[i].classe_metodos[j].metodo_chamadas.size();k++){
                        if(all_classes[i].classe_metodos[j].metodo_chamadas[k].get_def_ou_uso_tipo() == 'C'){
                            bool pertence = output_classe_eh_parente(all_classes,all_classes[i],all_classes[i].classe_metodos[j].metodo_chamadas[k].get_chamada_nome_classe_chamada());
                            if(pertence){/* Achei metodo que chama metodos da classe pai */
                                if(new_classe){
                                    *file_ITU << "\n\n------------------------------------------------------------------------------------------\n";
                                    *file_ITU << "Chamadas a partir dos m�todos da Classe: " << all_classes[i].get_classe_nome() << "\n";
                                    new_classe = false;
                                    encontrei_pelo_menos_1_caso = true;
                                }
                                if(new_metodo){
                                    *file_ITU << "\tM�todo: " << all_classes[i].classe_metodos[j].get_metodo_nome() << "\n";
                                    *file_ITU << "\t\tChamadas:\n";
                                    new_metodo = false;
                                }
                                output_print_1_Chamada(all_classes[i].classe_metodos[j].metodo_chamadas[k],&file_ITU,3,true,false);
                            }
                        }
                    }
                }
            }
            if(!encontrei_pelo_menos_1_caso){
                *file_ITU << "------------------------------------------------------------------------------------------\n";
                *file_ITU << "Nenhum caso encontrado no c�digo analisado!\n";
            }
        };
        /* SDA */
        void output_check_SDA(vector<Metodo> all_methods, vector<vector<string>> vtable, ofstream *file_SDA){
            *file_SDA << "SDA - ANOMALIA DE DEFINI��O DE ESTADO\n";
            *file_SDA << "� uma anomalia que ocorre quando as intera��es de estado de um descendente n�o s�o consistentes com as do ancestral, ou seja, quando os m�todos da classe herdeira n�o fornecem as mesmas defini��es para as vari�veis de estado dos m�todos da classe herdada que foram sobrescritos.\n";
            *file_SDA << "SA�DA: Foram comparados todos os m�todos polim�rficos existentes entre si, os que apresentam diverg�ncias quanto �s defini��es ou chamadas de fun��es ser�o exibidos abaixo.\n";
            *file_SDA << "TESTE: Os seguintes m�todos podem apresentar esta anomalia entre si e devem ser testados a fim de verificar as intera��es de estado.\n\n";
            bool encontrei_pelo_menos_1_caso = false;
            vector<int> sizes;
            for(int l = 0; l < vtable.size(); l++){
                sizes.push_back(vtable[l].size());
            }
            if(sizes.size() > 0){
                for(int i = 1; i < sizes[i-1] && i-1 < sizes.size(); i++){
                    Metodo previous_method;
                    bool match_2 = true;
                    bool match_3 = true;
                    for(int j = 0; j < vtable.size(); j++){
                        /* Compara todos os DUCP entre eles e se tiverem coisas diferentes, exibir*/
                        int found = 0;
                        for(int k = 0; k < all_methods.size() && found == 0; k++){
                            if(vtable[j][i] == all_methods[k].get_metodo_nome()){
                                found = 1;
                                if(j == 0){previous_method = all_methods[k];}
                                if(match_2 == true){ match_2 = output_compare_DU(previous_method.metodo_defs,all_methods[k].metodo_defs);}
                                if(match_3 == true){ match_3 = output_compare_Chamada(previous_method.metodo_chamadas,all_methods[k].metodo_chamadas);}
                                previous_method = all_methods[k];
                            }
                        }
                    }
                    /* Imprimir a lista falsa */
                    if(match_2 == false || match_3 == false ){
                        encontrei_pelo_menos_1_caso = true;
                        *file_SDA << "------------------------------------------------------------------------------------------\n";
                        *file_SDA << "\nPoss�vel SDA nos seguintes metodos polimorficos entre si: \n";
                        if(vtable.size() > 0){*file_SDA << vtable[0][i] << " ";}
                        for(int j = 1; j < vtable.size(); j++){
                            if(vtable[j-1][i] != vtable[j][i]){*file_SDA << vtable[j][i] << " ";}
                        }
                        *file_SDA << "\n";
                        *file_SDA << "Os metodos diferem quanto as";
                        if(match_2 == false){ *file_SDA << " DEFINI��ES";}
                        if(match_2 == false && match_3 == false){ *file_SDA << " e";}
                        if(match_3 == false){ *file_SDA << " CHAMADAS";}
                        *file_SDA << ", por favor conferir as informacoes abaixo: \n";
                        vector<string> limpo = all_methods[0].output_remove_repetidos(vtable,i);
                        for(int j = 0; j < limpo.size(); j++){
                            int found = 0;
                            bool skip = false;
                            if(j+1 < limpo.size()){
                                if(limpo[j] == limpo[j+1]){ skip = true; }
                            }
                            if(!skip){
                                for(int k = 0; k < all_methods.size() && found == 0; k++){
                                    if(limpo[j] == all_methods[k].get_metodo_nome()){
                                        found = 1;
                                        *file_SDA << "\tMetodo: " << limpo[j] << "\n";
                                        //if(match_1 == false){output_print_DU(all_methods[k].metodo_usos,&file_SDA,2,true,false);}
                                        if(match_2 == false){output_print_DU(all_methods[k].metodo_defs,&file_SDA,2,true,false);}
                                        if(match_3 == false){output_print_Chamada(all_methods[k].metodo_chamadas,&file_SDA,2,true,false);}
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if(!encontrei_pelo_menos_1_caso){
                *file_SDA << "------------------------------------------------------------------------------------------\n";
                *file_SDA << "Nenhum caso encontrado no c�digo analisado!\n";
            }
        };
        /* SDIH */
        vector<vector<string>> output_ordenar_vector(vector<vector<string>> vetor){
            string aux;
            for(int i=vetor[0].size()-1; i >= 1; i--){
                for( int j=0; j < i ; j++){
                    if(vetor[0][j]>vetor[0][j+1]){
                        aux = vetor[0][j];
                        vetor[0][j] = vetor[0][j+1];
                        vetor[0][j+1] = aux;
                        aux = vetor[1][j];
                        vetor[1][j] = vetor[1][j+1];
                        vetor[1][j+1] = aux;
                    }
                }
            }
            return vetor;
        }
        vector<vector<string>> output_classes_SDIH(Classe current_classe, vector<Classe> all_classes){
            vector<vector<string>> current_atributos;
            //vector<vector<string>> concatenados;
            //concatenados.resize(2);
            current_atributos.resize(2);
            for(int i=0;i<current_classe.classe_atributos.size();i++){
                current_atributos[0].push_back(current_classe.classe_atributos[i].get_atributo_nome());
                current_atributos[1].push_back(current_classe.get_classe_nome());
            }
            for(int i=0;i<current_classe.classe_pais.size();i++){
                for(int j=0;j<all_classes.size();j++){
                    if(all_classes[j].get_classe_nome() == current_classe.classe_pais[i]){
                        vector<vector<string>> tmp;
                        tmp = output_classes_SDIH(all_classes[j],all_classes);
                        /* concateno tmp com current_atributos */
                        current_atributos[0].reserve(current_atributos[0].size()+tmp[0].size());
                        current_atributos[0].insert(current_atributos[0].end(),tmp[0].begin(),tmp[0].end());
                        current_atributos[1].reserve(current_atributos[1].size()+tmp[1].size());
                        current_atributos[1].insert(current_atributos[1].end(),tmp[1].begin(),tmp[1].end());
                    }
                }
            }
            //if(current_classe.classe_pais.size() == 0){concatenados = current_atributos;}
            return current_atributos;
        }
        void output_check_SDIH(vector<Classe> all_classes, ofstream *file_SDIH){
            *file_SDIH << "SDIH - INCONSIST�NCIA DE DEFINI��O DE ESTADO\n";
            *file_SDIH << "Ocorre quando a inclus�o de uma vari�vel local em uma classe herdeira sobrescreve a vari�vel da classe herdada, deixando esta escondida, podendo assim causar uma anomalia de fluxo de dados.\n";
            *file_SDIH << "SA�DA: Foram comparados todos os atributos entre classes herdeiras e herdadas, todos os que possuem mesmo nome ser�o exibidos abaixo.\n";
            *file_SDIH << "TESTE: Os atributos exibidos devem ser testados a fim de identificar se as vari�veis de estado est�o sendo alteradas de acordo com a especifica��o.\n\n";
            bool encontrei_pelo_menos_1_caso = false;
            for(int i=0;i<all_classes.size();i++){
                /* Chamando funcao recursiva */
                vector<vector<string>> all_atributes;
                all_atributes = output_classes_SDIH(all_classes[i],all_classes);
                all_atributes = output_ordenar_vector(all_atributes);
                /* Para todas as listas geradas, identificar conflitos */
                for(int j=0;j<all_atributes[0].size();j++){
                    if(j+1 < all_atributes[0].size()){
                        if(all_atributes[0][j] == all_atributes[0][j+1]){
                            encontrei_pelo_menos_1_caso = true;
                            if(j==0){
                                *file_SDIH << "------------------------------------------------------------------------------------------\n";
                                *file_SDIH << "Para a classe: " << all_classes[i].get_classe_nome() << "\n";
                            }
                            *file_SDIH << "----- Atributos de mesmo nome encontrados -----\n";
                            *file_SDIH << "Nome do Atributo: " << all_atributes[0][j] << "\n";
                            *file_SDIH << "Este atributo existe nas classes: \n";
                            *file_SDIH << all_atributes[1][j] << "\n";
                            while(j+1 < all_atributes[0].size() && all_atributes[0][j] == all_atributes[0][j+1]){
                                *file_SDIH << all_atributes[1][j+1] << "\n";
                                j++;
                            }
                            *file_SDIH << "\n";
                        }
                    }
                }
            }
            if(!encontrei_pelo_menos_1_caso){
                *file_SDIH << "------------------------------------------------------------------------------------------\n";
                *file_SDIH << "Nenhum caso encontrado no c�digo analisado!\n";
            }
        };
        /* SDI */
        Metodo output_encontra_metodo(string nome_metodo_corrente, vector<Metodo> all_methods){
            for(int k = 0; k < all_methods.size(); k++){
                if(nome_metodo_corrente == all_methods[k].get_metodo_nome()){
                    return all_methods[k];
                }
            }
        }
        vector<Classe> output_find_relatives(Classe filho, vector<Classe> all_classes){
            vector<Classe> all_relatives;
            all_relatives.push_back(filho);
            for(int i=0;i<filho.classe_pais.size();i++){
                for(int j=0;j<all_classes.size();j++){
                    if(all_classes[j].get_classe_nome() == filho.classe_pais[i]){
                        vector<Classe> tmp;
                        tmp = output_find_relatives(all_classes[j],all_classes);
                        all_relatives.reserve(all_relatives.size()+tmp.size());
                        all_relatives.insert(all_relatives.end(),tmp.begin(),tmp.end());
                    }
                }
            }
            return all_relatives;
        }
        void output_check_SDI(vector<Classe> all_classes, vector<Metodo> all_methods, vector<vector<string>> vtable, ofstream *file_SDI){
            *file_SDI << "SDI - DEFINI��O INCORRETA DE ESTADO\n";
            *file_SDI << "Ocorre quando o m�todo herdeiro define a mesma vari�vel de estado que o m�todo antecessor possui e a computa��o feita pelo m�todo herdeiro n�o � semanticamente correspondente � computa��o do m�todo sobrescrito em rela��o � mesma vari�vel de estado, sendo assim uma anomalia de comportamento em potencial.\n";
            *file_SDI << "SA�DA: Para cada classe existente e para cada vari�vel de estado dentro de sua hierarquia, todos os m�todos polim�rficos que a definem ser�o exibidos.\n";
            *file_SDI << "TESTE: A sem�ntica de defini��o da vari�vel de todos os m�todos exibidos deve ser analisada e testada.\n\n";
            bool encontrei_pelo_menos_1_caso = false;
            bool skip = false;
            bool new_classe = true;
            /* Para a arvore geneologica da classe tal - retorna nome de classes e todos os atributos - output_classes_SDIH */
            for(int i=0;i<all_classes.size() && vtable.size() > 0;i++){
                new_classe = true;
                vector<vector<string>> all_atributes;
                all_atributes = output_classes_SDIH(all_classes[i],all_classes);
                all_atributes = output_ordenar_vector(all_atributes);
                vector<Classe> all_relatives;
                all_relatives = output_find_relatives(all_classes[i],all_classes);
                for(int j=0;j<all_atributes[0].size();j++){
                    bool new_atr = true;
                    skip = false;
                    /* Para cada diferente atributo existente nela */
                    if(j>0){ /* Se ele for igual ao atributo anterior, ignora */
                        if(all_atributes[0][j-1] == all_atributes[0][j] && all_atributes[1][j-1] == all_atributes[1][j]){skip = true;}
                    }
                    if(!skip){
                         /* Para todos os metodos polimorficos */
                         vector<Metodo> all_met_found;
                         vector<int> sizes;
                         for(int l = 0; l < vtable.size(); l++){
                            sizes.push_back(vtable[l].size());
                         }
                         for(int k = 1; k < sizes[k-1] && k-1 < sizes.size(); k++){
                            for(int l = 0; l < vtable.size(); l++){
                                Metodo metodo_corrente = output_encontra_metodo(vtable[l][k],all_methods);
                                /* checkar se classe do metodo existe na hierarquia em all_relatives - posicao zero do array */
                                bool eh_metodo_de_classe_valida = false;
                                for(int m = 0; m < all_relatives.size() && eh_metodo_de_classe_valida == false; m++){
                                    if(all_relatives[m].get_classe_nome() == metodo_corrente.get_metodo_classe_nome()){
                                        eh_metodo_de_classe_valida = true;
                                    }
                                }
                                if(eh_metodo_de_classe_valida){
                                    /* Checkar se o m�todo define o all_atributes[j] */
                                    for(int m = 0; m < metodo_corrente.metodo_defs.size(); m++){
                                        if(metodo_corrente.metodo_defs[m].def_ou_uso_atributo != NULL){
                                            if(metodo_corrente.metodo_defs[m].def_ou_uso_atributo->get_atributo_nome() == all_atributes[0][j] &&
                                               metodo_corrente.metodo_defs[m].def_ou_uso_atributo->get_atributo_nome_classe() == all_atributes[1][j]){
                                                /* Exibir cada defini��o encontrada */
                                                all_met_found.push_back(metodo_corrente);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if(all_met_found.size() > 1){
                            /* Comparar e remover iguais */
                            vector<Metodo> all_met_found_clear;
                            all_met_found_clear.push_back(all_met_found[0]);
                            for(int m = 1; m < all_met_found.size(); m++){
                                if(all_met_found[m-1].get_metodo_nome() != all_met_found[m].get_metodo_nome()){
                                    all_met_found_clear.push_back(all_met_found[m]);
                                }
                            }
                            if(all_met_found_clear.size() > 1){
                                for(int m = 0; m < all_met_found_clear.size(); m++){
                                    if(new_classe){new_classe = false;
                                        *file_SDI << "------------------------------------------------------------------------------------------\n";
                                        *file_SDI << "An�lise a partir da Classe: " << all_classes[i].get_classe_nome() << "\n";
                                    }
                                    if(new_atr){new_atr = false;
                                        *file_SDI << "A vari�vel de estado " << all_atributes[0][j] << " da classe " << all_atributes[1][j] <<" � definida nos m�todos:\n";
                                    }
                                    *file_SDI << "\t" << all_met_found_clear[m].get_metodo_nome() << ":\n";
                                    for(int n = 0; n < all_met_found_clear[m].metodo_defs.size(); n++){
                                        if(all_met_found_clear[m].metodo_defs[n].def_ou_uso_atributo != NULL){
                                            if(all_met_found_clear[m].metodo_defs[n].def_ou_uso_atributo->get_atributo_nome() == all_atributes[0][j] &&
                                               all_met_found_clear[m].metodo_defs[n].def_ou_uso_atributo->get_atributo_nome_classe() == all_atributes[1][j]){
                                                /* Exibir cada defini��o encontrada */
                                                output_print_1_DU(all_met_found_clear[m].metodo_defs[n],&file_SDI,2,false,false);
                                            }
                                        }
                                    }
                                    encontrei_pelo_menos_1_caso = true;
                                }
                            }
                        }
                    }
                }
            }
            if(!encontrei_pelo_menos_1_caso){
                *file_SDI << "------------------------------------------------------------------------------------------\n";
                *file_SDI << "Nenhum caso encontrado no c�digo analisado!\n";
            }
        }
        /* ACB1 */
        void output_check_ACB1(vector<Metodo> all_methods, vector<vector<string>> vtable, ofstream *file_ACB1){
            *file_ACB1 << "ACB1 - COMPORTAMENTO AN�MALO DE CONSTRU��O\n";
            *file_ACB1 << "Pode ocorrer quando o construtor de uma classe ancestral executa um m�todo polim�rfico na classe herdeira, se o m�todo chamado utiliza vari�veis de estado que n�o foram previamente definidas, pois sua defini��o encontra-se no construtor da classe herdeira.\n";
            *file_ACB1 << "SA�DA: Abaixo se encontram todos os construtores que possuem chamadas polim�rficas, e, para cada m�todo polim�rfico, todos os seus respectivos usos que referenciam atributos de sua pr�pria classe.\n";
            *file_ACB1 << "TESTE: Os testes devem ser gerados com foco na verifica��o do estado das vari�veis afetadas.\n\n";
            bool encontrei_pelo_menos_1_caso = false;
            /* Para todos os contrutores */
            smatch match_new;
            regex find_constructor ("_ZN[0-9]*(.*)C[0-9][0-9]*E.*");
            for(int i=0;i < all_methods.size();i++){
                bool new_method = true;
                string nome = all_methods[i].get_metodo_nome();
                if(regex_search(nome,match_new,find_constructor)){
                    /* Ver se existe chamada polimorfica */
                    for(int j=0;j < all_methods[i].metodo_chamadas.size();j++){
                        if(all_methods[i].metodo_chamadas[j].get_def_ou_uso_tipo() == 'P'){
                            if(new_method){
                                *file_ACB1 << "------------------------------------------------------------------------------------------\n";
                                *file_ACB1 << "Construtor: " << all_methods[i].get_metodo_nome()
                                           << "\tClasse: " << all_methods[i].get_metodo_classe_nome() << "\n";
                                new_method = false;
                            }
                            *file_ACB1 << "\tChamada polim�rfica para: ";
                            int posicao = 1 + atoi(all_methods[i].metodo_chamadas[j].get_chamada_vfn_table_posicao().c_str());
                            for(int l = 0; l < vtable.size(); l++){
                                *file_ACB1 << vtable[l][posicao] << "\t";
                            }
                            *file_ACB1 << "\n\n";
                            for(int l = 0; l < vtable.size(); l++){
                                Metodo metodo_corrente = output_encontra_metodo(vtable[l][posicao],all_methods);
                                *file_ACB1 << "\t\tUsos do M�todo: " << vtable[l][posicao] << "\tM�todo Classe: " << metodo_corrente.get_metodo_classe_nome() << "\n";
                                bool encontrei_pelo_menos_1_caso_de_uso = false;
                                if(metodo_corrente.metodo_usos.size() != 0){
                                    for(int n=0;n < metodo_corrente.metodo_usos.size();n++){
                                        /* Testar se o uso � de algum atributo da classe filha */
                                        if(metodo_corrente.get_metodo_classe_nome() == metodo_corrente.metodo_usos[n].def_ou_uso_atributo->get_atributo_nome_classe()){
                                            output_print_1_DU(metodo_corrente.metodo_usos[n],&file_ACB1,3,false,false);
                                            encontrei_pelo_menos_1_caso_de_uso = true;
                                        }
                                    }
                                }
                                if(!encontrei_pelo_menos_1_caso_de_uso){*file_ACB1 << "\t\t\tEste m�todo n�o possui usos que fa�am uso de atributos de sua pr�pria classe!\n";}
                                *file_ACB1 << "\n";
                            }
                            encontrei_pelo_menos_1_caso = true;
                        }
                    }
                }
            }
            if(!encontrei_pelo_menos_1_caso){
                *file_ACB1 << "------------------------------------------------------------------------------------------\n";
                *file_ACB1 << "Nenhum caso encontrado no c�digo analisado!\n";
            }
        }
        /* ACB2 */
        bool output_atributo_pertence_a_ancestral(string atr_classe,vector<Classe> all_classes, string classe_nome, int i){
            bool saida = false;
            if(i != 0){
                /* Testar se a classe do atributo � a classe corrente que � pai da inicial */
                if(atr_classe == classe_nome){
                    saida = true;
                }
            }
            for(int i=0;i<all_classes.size();i++){
                if(all_classes[i].get_classe_nome() == classe_nome){
                    for(int j=0;j<all_classes[i].classe_pais.size();j++){
                        bool tmp = false;
                        tmp = output_atributo_pertence_a_ancestral(atr_classe, all_classes, all_classes[i].classe_pais[j], 1);
                        if(tmp){saida = tmp;}
                    }
                }
            }
            return saida;
        }
        void output_check_ACB2(vector<Classe> all_classes, vector<Metodo> all_methods, vector<vector<string>> vtable, ofstream *file_ACB2){
            *file_ACB2 << "ACB2 - COMPORTAMENTO AN�MALO DE CONSTRU��O 2\n";
            *file_ACB2 << "Ocorre quando um m�todo polim�rfico � chamado por um construtor em uma subclasse e faz uso de vari�veis de estado de sua superclasse, sem que os mesmos tenham sido previamente definidos, pois o m�todo que o faz foi sobrescrito pelo m�todo chamado.\n";
            *file_ACB2 << "SA�DA: Abaixo se encontram todos os construtores que possuem chamadas polim�rficas, e, para cada m�todo polim�rfico, todos os seus respectivos usos que referenciam atributos de classes herdadas.\n";
            *file_ACB2 << "TESTE: Os testes devem ser gerados com foco na verifica��o do estado das vari�veis afetadas.\n\n";
            bool encontrei_pelo_menos_1_caso = false;
            /* Para todos os contrutores */
            smatch match_new;
            regex find_constructor ("_ZN[0-9]*(.*)C[0-9][0-9]*E.*");
            for(int i=0;i < all_methods.size();i++){
                bool new_method = true;
                string nome = all_methods[i].get_metodo_nome();
                if(regex_search(nome,match_new,find_constructor)){
                    /* Ver se existe chamada polimorfica */
                    for(int j=0;j < all_methods[i].metodo_chamadas.size();j++){
                        if(all_methods[i].metodo_chamadas[j].get_def_ou_uso_tipo() == 'P'){
                            if(new_method){
                                *file_ACB2 << "------------------------------------------------------------------------------------------\n";
                                *file_ACB2 << "Construtor: " << all_methods[i].get_metodo_nome()
                                           << "\tClasse: " << all_methods[i].get_metodo_classe_nome() << "\n";
                                new_method = false;
                            }
                            *file_ACB2 << "\tChamada polim�rfica para: ";
                            int posicao = 1 + atoi(all_methods[i].metodo_chamadas[j].get_chamada_vfn_table_posicao().c_str());
                            for(int l = 0; l < vtable.size(); l++){
                                *file_ACB2 << vtable[l][posicao] << "\t";
                            }
                            *file_ACB2 << "\n\n";
                            for(int l = 0; l < vtable.size(); l++){
                                Metodo metodo_corrente = output_encontra_metodo(vtable[l][posicao],all_methods);
                                *file_ACB2 << "\t\tUsos do m�todo: " << vtable[l][posicao] << "\tM�todo Classe: " << metodo_corrente.get_metodo_classe_nome() << "\n";
                                bool encontrei_pelo_menos_1_caso_de_uso = false;
                                if(metodo_corrente.metodo_usos.size() != 0){
                                    for(int n=0;n < metodo_corrente.metodo_usos.size();n++){
                                        /* Testar se o uso � de algum atributo da classe pai */
                                        bool pertence = output_atributo_pertence_a_ancestral(metodo_corrente.metodo_usos[n].def_ou_uso_atributo->get_atributo_nome_classe(),all_classes,metodo_corrente.get_metodo_classe_nome(),0);
                                        if(pertence){output_print_1_DU(metodo_corrente.metodo_usos[n],&file_ACB2,3,false,false);
                                        encontrei_pelo_menos_1_caso_de_uso = true;}
                                    }
                                }
                                if(!encontrei_pelo_menos_1_caso_de_uso){*file_ACB2 << "\t\t\tEste m�todo n�o possui usos que fa�am uso de atributos de alguma classe ancestral!\n";}
                                *file_ACB2 << "\n";
                            }
                            encontrei_pelo_menos_1_caso = true;
                        }
                    }
                }
            }
            if(!encontrei_pelo_menos_1_caso){
                *file_ACB2 << "------------------------------------------------------------------------------------------\n";
                *file_ACB2 << "Nenhum caso encontrado no c�digo analisado!\n";
            }
        }
        /* IC */
        vector<Atributo> output_remover_atributo(Atributo elemento,vector<Atributo> vec){
            for(int i=0;i < vec.size();i++){
                if(vec[i].get_atributo_nome() == elemento.get_atributo_nome() &&
                   vec[i].get_atributo_nome_classe() == elemento.get_atributo_nome_classe()){
                    vec.erase(vec.begin() + i);
                    return vec;
                }
            }
            return vec;
        }
        void output_check_IC(vector<Classe> all_classes, ofstream *file_IC){
            *file_IC << "IC - CONSTRU��O INCOMPLETA\n";
            *file_IC << "Ocorre quando o estado inicial do objeto � indefinido, ou seja, quando o construtor de sua classe n�o define todos os atributos da classe.\n";
            *file_IC << "SA�DA: Abaixo encontram-se todos os construtores que n�o definem os atributos de sua classe, seguidos pelos nomes dos atributos n�o declarados.\n";
            *file_IC << "TESTE: Os valores dos atributos listados abaixo devem ser verificados a fim de evitar que os mesmos sejam usados sem a devida inicializa��o.\n\n";
            bool encontrei_pelo_menos_1_caso = false;
            /* Para todos os contrutores */
            smatch match_new;
            regex find_constructor ("_ZN[0-9]*(.*)C[0-9][0-9]*E.*");
            for(int i=0;i < all_classes.size();i++){
                bool new_class = true;
                for(int j=0;j < all_classes[i].classe_metodos.size();j++){
                    bool new_method = true;
                    vector<Atributo> atributos_da_classe = all_classes[i].get_classe_atributos();
                    string nome = all_classes[i].classe_metodos[j].get_metodo_nome();
                    if(regex_search(nome,match_new,find_constructor)){
                        /* Percorrer lista de atributos a fim de identificar os atributos n�o definidos. */
                        for(int k = 0;k < atributos_da_classe.size();k++){
                            for(int l = 0;l < all_classes[i].classe_metodos[j].metodo_defs.size();l++){
                                if(all_classes[i].classe_metodos[j].metodo_defs[l].def_ou_uso_atributo != NULL){
                                    if(all_classes[i].classe_metodos[j].metodo_defs[l].def_ou_uso_atributo->get_atributo_nome() == atributos_da_classe[k].get_atributo_nome()
                                    && all_classes[i].classe_metodos[j].metodo_defs[l].def_ou_uso_atributo->get_atributo_nome_classe() == atributos_da_classe[k].get_atributo_nome_classe()){
                                        atributos_da_classe = output_remover_atributo(atributos_da_classe[k],atributos_da_classe);
                                    }
                                }
                            }
                        }
                        if(atributos_da_classe.size() == 0){
                            *file_IC << "------------------------------------------------------------------------------------------\n";
                            *file_IC << "O Construtor " << all_classes[i].classe_metodos[j].get_metodo_nome()
                                     << " da Classe: " << all_classes[i].get_classe_nome()
                                     << " possui declara��o para todos os atributos da classe!\n\n";
                        }else{
                            *file_IC << "------------------------------------------------------------------------------------------\n";
                            *file_IC << "O Construtor " << all_classes[i].classe_metodos[j].get_metodo_nome()
                                     << " da Classe: " << all_classes[i].get_classe_nome()
                                     << " n�o possui declara��o para os atributos abaixo:\n";
                            for(int m = 0;m < atributos_da_classe.size();m++){
                                *file_IC << atributos_da_classe[m].get_atributo_nome() << "\n";
                            }
                            *file_IC << "\n";
                            encontrei_pelo_menos_1_caso = true;
                        }
                    }
                }
            }
            if(!encontrei_pelo_menos_1_caso){
                *file_IC << "------------------------------------------------------------------------------------------\n";
                *file_IC << "Nenhum caso encontrado no c�digo analisado!\n";
            }
        }
        /* YoYo */
        void output_gerar_yoyo(vector<Metodo> all_methods, vector<vector<string>> vtable){
             for(int i = 0; i < all_methods.size(); i++){
                file_YOYO << "\n------------------------------------------------------------------------------------------\n";
                file_YOYO << "M�todo: " << all_methods[i].get_metodo_nome() << " (" << all_methods[i].get_metodo_classe_nome() << ")\n\n";
                all_methods[i].show_all_methods_no_poli(all_methods,1,vtable);
             }
        }
        /* ALL */
        void output_all_classes(vector<Classe> all_classes, ofstream *file_new){
            for(int i = 0; i != all_classes.size(); i++){
                *file_new << "------------------------------------------------------------------------------------------\n";
                *file_new << "\nClasse: " << all_classes[i].get_classe_nome();
                if(all_classes[i].classe_pais.size() > 0){ *file_new << "\t Filho de: "; }
                for(int j = 0; j != all_classes[i].classe_pais.size(); j++){
                    *file_new << all_classes[i].classe_pais[j];
                    if(j+1 != all_classes[i].classe_pais.size()){ *file_new << ", "; }
                }
                *file_new << "\n";
                *file_new << "\tAtributos: \n";
                output_print_ATR(all_classes[i].classe_atributos,&file_new,2,true);
                *file_new << "\tMetodos: \n";
                for(int j = 0; j != all_classes[i].classe_metodos.size(); j++){
                    *file_new << "\t\t" << all_classes[i].classe_metodos[j].get_metodo_nome() << "\n";
                    *file_new << "\t\t\tObjetos: \n";
                    output_print_OBJ(all_classes[i].classe_metodos[j].metodo_objetos,&file_new,4);
                    *file_new << "\t\t\tDefini��es: \n";
                    output_print_DU(all_classes[i].classe_metodos[j].metodo_defs,&file_new,4,true,true);
                    *file_new << "\t\t\tUsos: \n";
                    output_print_DU(all_classes[i].classe_metodos[j].metodo_usos,&file_new,4,true,true);
                    *file_new << "\t\t\tChamadas: \n";
                    output_print_Chamada(all_classes[i].classe_metodos[j].metodo_chamadas,&file_new,4,true,true);
                }
            }
        }
        void output_main_method(Metodo main_method, ofstream *file_new){
            *file_new << "------------------------------------------------------------------------------------------\n";
            *file_new << "\t\t" << main_method.get_metodo_nome() << "\n";
            *file_new << "\t\t\tObjetos: \n";
            output_print_OBJ(main_method.metodo_objetos,&file_new,4);
            *file_new << "\t\t\tDefini��es: \n";
            output_print_DU(main_method.metodo_defs,&file_new,4,true,true);
            *file_new << "\t\t\tUsos: \n";
            output_print_DU(main_method.metodo_usos,&file_new,4,true,true);
            *file_new << "\t\t\tChamadas: \n";
            output_print_Chamada(main_method.metodo_chamadas,&file_new,4,true,true);
        };
};

int main (){
    /* Vari�veis */
    Metodo main_method;
    vector<Classe> all_classes;
    vector<Metodo> all_destitute_methods;
    vector<Metodo> all_methods_thathaveclassesbutarenothtere;

    /* File */
    ifstream arquivo;
    string new_line_input;
    string file_name = "";
    string file_name_folder = "input/";
    string file_name_ext = ".ll";
    string file_name_tmp = "";
    cout << "Arquivo de entrada: "; cin >> file_name_tmp; cout << "\n";
    file_name.append(file_name_folder);
    file_name.append(file_name_tmp);
    file_name.append(file_name_ext);

    /* Match */
    smatch match;
    smatch match_inside_while;
    smatch match_inside_while_du;
    smatch get_method_definition;

    /* Class */
    regex find_class ("%class\\.[^\\.]* = type.*");
    regex clear_class ("^%class.");
    regex clear_class2 (" .*");
    regex if_class_has_parents ("%class\\.(.*) = type (<)*\\{ %class\\.(.*)");
    regex clear_class_heranca_parents_list (".*type (<)*\\{ ");
    regex if_class_parent_split_is_a_class ("(.*)%class\\.(.*)");
    regex clear_class_heranca ("(.*)%class\\.");
    regex clear_class_heranca2 ("[,| |\\.].*");

    /* Metodos */
    regex find_method ("^define linkonce_odr .* @_(.*)%class\\.(.*)");
    regex find_method_void ("^define void @_(.*)%class\\.(.*)");
    regex clear_method ("define linkonce_odr [^ ]* @");
    regex clear_method2 ("\\((.*)");
    regex clear_method_class ("^(.*)%class\\.");
    regex clear_method_class2 ("\\*(.*)");
    regex find_method_definition ("^define(.*)");
    regex find_method_definition_end ("^\\}$");
    regex find_if_method_has_class_definition ("^(.*)%class\\.(.*)");
    regex clear_method_class_definition ("^(.*)%class\\.");
    regex clear_method_class_definition2 ("\\*(.*)");
    regex clear_method_definition ("@");
    regex clear_method_definition2 ("\\(");
    regex find_main_method ("^define (.*) @main(.*)");
    regex clear_method_void ("define void @");

    /* Atributos */
    regex find_attribute ("^  %(.*)getelementptr inbounds %class(.*),(.*),(.*),(.*)");
    regex clear_attribute_name (" = .*");
    regex clear_attribute_name2 (" *%");
    regex clear_attribute_id (".*, ");
    regex clear_attribute_class (".*class.");
    regex clear_attribute_class2 ("\\*.*");

    /* Def-Use */
    vector<string> All_possible_future_DUs_atributo;
    vector<string> All_possible_future_DUs_classe;
    vector<string> All_possible_future_DUs_obj;
    regex find_method_name_definition ("@[^ ]*\\(");
    regex find_alloca ("%[^ ]* = alloca %class\\.[^ ]*,(.*)");
    regex clear_alloca_obj_name ("^%");
    regex clear_alloca_obj_name2 (" = alloca.*");
    regex clear_alloca_class_name (".*%class\\.");
    regex clear_alloca_class_name2 ("\\**, align.*");
    regex find_possivel_futuro_du (".* = getelementptr inbounds.*");
    regex clear_find_possivel_futuro_du (" = getelementptr inbounds.*");
    regex clear_find_possivel_futuro_du2 (" *%");
    regex clear_find_possivel_futuro_du_class (".*%class\\.");
    regex clear_find_possivel_futuro_du_class2 ("\\* %.*");
    regex clear_find_possivel_futuro_du_obj (".*%.*%.*%.*%");
    regex clear_find_possivel_futuro_du_obj2 (",.*");
    regex find_store (".*store.*");
    regex find_load (".*load.*");
    regex find_local_obj ("this[0-9]*");
    regex find_local_obj2 ("this\\.addr");
    regex find_call_void (".*call void.*");
    regex find_call_call (".*%call[0-9]* = call.*");
    regex find_invoke_void (".*invoke void.*");
    regex find_call_invoke (".*%call[0-9]* = invoke.*");
    regex find_llvm_global ("^@llvm\\.global_ctors =(.*)");

    /* Polimorfismo */
    vector<string> All_names_var_temp_poli;
    vector<string> All_names_obj_poli;
    vector<string> All_vfn_declaratios_id;
    vector<string> All_vfn_declaratios_table_position;
    regex find_vtable_declaration ("^@_ZTV(.*)");
    regex find_vtable_element ("_[^ ]*");
    regex clear_vtable_element_name (" (.*)");
    regex find_bitcast ("^( *)%[0-9]* = bitcast %class\\.[^ ]* %[^ ]* to %class\\.[^ ]*");
    regex clear_bitcast_tmp_name ("^( *)%");
    regex clear_bitcast_tmp_name2 (" = bitcast %class\\.[^ ]* %[^ ]* to %class\\.[^ ]*");
    regex clear_bitcast_obj_name ("^( *)%[0-9]* = bitcast %class\\.[^ ]* %");
    regex clear_bitcast_obj_name2 (" to %class\\.[^ ]*");
    regex clear_bitcast_classe1 ("^( *)%[0-9]* = bitcast %class\\.");
    regex clear_bitcast_classe12 ("(\\*)* %[^ ]* to %class\\.[^ ]*");
    regex clear_bitcast_classe2 ("^( *)%[0-9]* = bitcast %class\\.[^ ]* %[^ ]* to %class\\.");
    regex clear_bitcast_classe22 ("(\\*)*");
    regex this_found ("this[0-9]*");
    regex find_vtable_prev_call ("%vfn[0-9]* = getelementptr inbounds (.*)");
    regex clear_vfn_posicao (" [0-9]*$");
    regex clear_vfn_posicao2 (" *");
    regex check_vfn_id_declaration ("%[0-9]* = load (.*) %vfn[0-9]*");
    regex clear_vfn_id ("^ *%[0-9]*");
    regex clear_vfn_id2 ("^ *%");
    regex find_poli_call_call ("^ *%call[0-9]* = call.* %[0-9]*\\((.*)%[^ ]* %[^ ]*(.*)\\)");
    regex find_poli_call_void ("^ *call void %[0-9]*\\((.*)%[^ ]* %[^ ]*(.*)\\)");
    regex find_poli_invoke_void ("^ *invoke void %[0-9]*\\((.*)%[^ ]* %[^ ]*(.*)\\)");
    regex find_poli_call_invoke ("^ *%call[0-9]* = invoke.* %[0-9]*\\((.*)%[^ ]* %[^ ]*(.*)\\)");

    /* Inicializando variaveis */
    all_classes.clear();
    all_destitute_methods.clear();
    all_methods_thathaveclassesbutarenothtere.clear();

/* ------------------ Abrir arquivo de entrada - ler Classes, Metodos e Atributos */
    arquivo.open(file_name, std::ios::in);
    if(!arquivo.is_open()){ cout << "Erro ao tentar abrir o arquivo " << file_name << "\n"; return 0; }
    cout << "Carregando Classes, Metodos e Atributos...\n";
    while(!arquivo.eof()){
        getline(arquivo,new_line_input);
        /* Declara��o de classe */
        if(regex_search(new_line_input,match,find_class)){
            string g = regex_replace(match.str(),clear_class,"$2");
            g = regex_replace(g,clear_class2,"$2");
            Classe tmp;
            tmp.clear_classe_atributos();
            tmp.clear_classe_metodos();
            tmp.set_classe_nome(g);
            tmp.clear_classe_pais();
            tmp.clear_classe_vtable();
            all_classes.push_back(tmp);
        }
        /* Declara��o de metodos */
        else if(regex_search(new_line_input,match,find_method)){
            string last_match = match.str();
            string g = regex_replace(last_match,clear_method,"$2");
            g = regex_replace(g,clear_method2,"$2");
            string h = regex_replace(last_match,clear_method_class,"$2");
            h = regex_replace(h,clear_method_class2,"$2");
            for(int i = 0; i != all_classes.size(); i++){
                if(all_classes[i].get_classe_nome() == h){
                    all_classes[i].add_classe_metodos(g);
                }
            }
        }
        /* Declara��o de metodos void */
        else if(regex_search(new_line_input,match,find_method_void)){
            string last_match = match.str();
            string g = regex_replace(last_match,clear_method_void,"$2");
            g = regex_replace(g,clear_method2,"$2");
            string h = regex_replace(last_match,clear_method_class,"$2");
            h = regex_replace(h,clear_method_class2,"$2");
            for(int i = 0; i != all_classes.size(); i++){
                if(all_classes[i].get_classe_nome() == h){
                    all_classes[i].add_classe_metodos(g);
                }
            }
        }
        /* Declara��o de atributo */
        else if(regex_search(new_line_input,match,find_attribute)){
            /* - pegar nome, ID e classe */
            string name = regex_replace(match.str(),clear_attribute_name,"$2");
            name = regex_replace(name,clear_attribute_name2,"$2");
            string id = regex_replace(match.str(),clear_attribute_id,"$2");
            string name_class = regex_replace(match.str(),clear_attribute_class,"$2");
            name_class = regex_replace(name_class,clear_attribute_class2,"$2");
            /* verificar se ja existe na lista de atributos */
            for(int i = 0; i != all_classes.size(); i++){
                if(all_classes[i].get_classe_nome() == name_class){
                    /* Acha a classe e chama o metodo */
                    all_classes[i].add_classe_atributos(name,id);
                }
            }
        }
    }
    arquivo.close();

/* ------------------ Abrir arquivo de entrada - ler Heranca e Polimorfismo */
    arquivo.open(file_name, std::ios::in);
    if(!arquivo.is_open()){ cout << "Erro ao tentar abrir o arquivo " << file_name << "\n"; return 0; }
    new_line_input = "";
    cout << "Carregando Heranca e Polimorfismo...\n";
    while(!arquivo.eof() && !regex_search(new_line_input,get_method_definition,find_llvm_global)){
        getline(arquivo,new_line_input);
        /* Declara��o de classe - Achar pais e linkar */
        if(regex_search(new_line_input,match,find_class)
           && regex_search(new_line_input,match_inside_while,if_class_has_parents)){
            string name_class = regex_replace(match.str(),clear_class,"$2");
            name_class = regex_replace(name_class,clear_class2,"$2");
            string class_parents_string = regex_replace(match.str(),clear_class_heranca_parents_list,"$2");
            /* Achar a classe filha e preencher ponteiro  */
            Classe *classe_filha = NULL;
            for(int i = 0; i != all_classes.size(); i++){
                if(all_classes[i].get_classe_nome() == name_class){
                    classe_filha = &all_classes[i];
                }
            }
            /* Incluir todos os pais em caso de heran�a multipla */
            stringstream ss(class_parents_string);
            string item;
            while (getline(ss, item, ',')) {
                if(regex_search(item,match_inside_while,if_class_parent_split_is_a_class)){
                    string class_parent = regex_replace(item,clear_class_heranca,"$2");
                    class_parent = regex_replace(class_parent,clear_class_heranca2,"$2");
                    /* Find dad */
                    for(int i = 0; i != all_classes.size(); i++){
                        if(all_classes[i].get_classe_nome() == class_parent){
                            classe_filha->add_classe_pais(class_parent);
                            //all_classes[i].add_classe_filhos(classe_filha->get_classe_nome());
                        }
                    }
                }
            }
        }
        /* Tabela de polimorfismo */
        if(regex_search(new_line_input,match,find_vtable_declaration)){
            Classe *classe_vtable = NULL;
            string declaration_name = regex_replace(match.str(),clear_vtable_element_name,"$2");
            /* Achar classe da qual essa vtable declaration pertence e setar ponteiro classe_vtable */
            for(int i = 0; i != all_classes.size(); i++){
                regex find_vtable_classe ("@_ZTV[0-9]" + all_classes[i].get_classe_nome());
                if(regex_search(declaration_name,match_inside_while,find_vtable_classe)){
                    classe_vtable = &all_classes[i];
                }
            }
            /* Split declaration */
            stringstream ss(match.str());
            string item;
            while (getline(ss, item, ',')) {
                if(regex_search(item,match_inside_while,find_vtable_element)){
                    /* Checkar se nome_metodo � um metodo conhecido */
                    string nome_metodo = match_inside_while.str();
                    int found = 0;
                    for(int i = 0; i != all_classes.size() && found == 0; i++){
                        for(int j = 0; j != all_classes[i].classe_metodos.size() && found == 0; j++){
                            if(all_classes[i].classe_metodos[j].get_metodo_nome() == nome_metodo){
                                classe_vtable->add_classe_vtable(nome_metodo);
                                found = 1;
                            }
                        }
                    }
                }
            }
        }
    }
    //arquivo.close();

/* ------------------ Abrir arquivo de entrada - ler def-use pairs */
    cout << "Carregando DU pairs...\n";
    while(!arquivo.eof()){
        getline(arquivo,new_line_input);
        /* Pegar defines - sempre que chegar em um, pegar todas as proximas linhas para analise at� achar }*/
        if(regex_search(new_line_input,match_inside_while,find_method_definition)){
            string nome_metodo = match_inside_while.str();
            string nome_classe_metodo = match_inside_while.str();
            regex_search(nome_metodo, get_method_definition, find_method_name_definition);
            nome_metodo = get_method_definition.str();
            nome_metodo = regex_replace(nome_metodo,clear_method_definition,"$2");
            nome_metodo = regex_replace(nome_metodo,clear_method_definition2,"$2");
            Metodo *novo_metodo_du_ponteiro = new Metodo();
            Metodo novo_metodo_du;
            bool skip = false;
            /* testes se o metodo nao � o main, se for, marcar como main */
            if(regex_search(new_line_input,match_inside_while,find_main_method)){
                main_method.set_metodo_nome("main");
                main_method.clear_metodo_defs();
                main_method.clear_metodo_usos();
                main_method.clear_metodo_objetos();
                main_method.clear_metodo_chamadas();
                main_method.set_metodo_classe_nome("");
                novo_metodo_du_ponteiro = &main_method;
            }else{
                if(regex_search(new_line_input,match_inside_while,find_if_method_has_class_definition)){
                    nome_classe_metodo = regex_replace(nome_classe_metodo,clear_method_class_definition,"$2");
                    nome_classe_metodo = regex_replace(nome_classe_metodo,clear_method_class_definition2,"$2");
                    /* Achar a classe dele e criar variavel para apontar para metodo que deve ser atualizado */
                    int found_method = 0;
                    for(int i3 = 0; i3 < all_classes.size() && found_method == 0; i3++){
                        if(all_classes[i3].get_classe_nome() == nome_classe_metodo){
                            for(int j3 = 0; j3 < all_classes[i3].classe_metodos.size() && found_method == 0; j3++){
                                if(all_classes[i3].classe_metodos[j3].get_metodo_nome() == nome_metodo){
                                    novo_metodo_du_ponteiro = &all_classes[i3].classe_metodos[j3];
                                    found_method = 1;
                                }
                            }
                        }
                    }
                    if(found_method == 0){
                        skip = true;
                    }
                }else{
                    skip = true;
                }
            }
            if(!skip){
                int ordem = 1;
                getline(arquivo,new_line_input); /* Pega a segunda linha do metodo */
                All_possible_future_DUs_atributo.clear();
                All_possible_future_DUs_classe.clear();
                All_possible_future_DUs_obj.clear();
                All_names_var_temp_poli.clear();
                All_names_obj_poli.clear();
                All_vfn_declaratios_id.clear();
                All_vfn_declaratios_table_position.clear();
                while(!regex_search(new_line_input,match_inside_while,find_method_definition_end)){
                        /* Analisa todo o metodo at� o fim */
                        /* Testar se � uma possivel declaracao polimorfica*/
                        if(regex_search(new_line_input,match_inside_while_du,find_vtable_prev_call)) {
                            /* Sou uma pr� vtable call - %vfn = getelementptr inbounds i32 (%class.A*)*, i32 (%class.A*)** %vtable, i64 2 */
                            /* Testar se minhas classes sao conhecidas */
                            int valida_classe_vfn = 0;
                            for(int j = 0; j != all_classes.size() && valida_classe_vfn == 0; j++){
                                regex sou_classe_conhecida ("%class\\." + all_classes[j].get_classe_nome() + "\\*");
                                if(regex_search(new_line_input,match_inside_while_du,sou_classe_conhecida)){
                                    valida_classe_vfn = 1;
                                }
                            }
                            if(valida_classe_vfn == 1){
                                /* Salvar posicao na tabela para a qual existe referencia */
                                regex_search(new_line_input,match_inside_while_du,clear_vfn_posicao);
                                string vfn_posicao = regex_replace(match_inside_while_du.str(),clear_vfn_posicao2,"$2");
                                /* ler proxima linha do arquivo para encontrar id */
                                getline(arquivo,new_line_input);
                                if(regex_search(new_line_input,match_inside_while_du,check_vfn_id_declaration)){
                                    /* � uma linha valida, salvar id */
                                    regex_search(new_line_input,match_inside_while_du,clear_vfn_id);
                                    string vfn_id = regex_replace(match_inside_while_du.str(),clear_vfn_id2,"$2");
                                    /* Incluir na lista vfn  - id e posicao */
                                    All_vfn_declaratios_id.push_back(vfn_id);
                                    All_vfn_declaratios_table_position.push_back(vfn_posicao);
                                }else{ cout << "ERROR - Erro de analise\n"; }
                            }
                        }else
                        if(regex_search(new_line_input,match_inside_while_du,find_poli_call_call) ||
                           regex_search(new_line_input,match_inside_while_du,find_poli_call_void) ||
                           regex_search(new_line_input,match_inside_while_du,find_poli_call_invoke) ||
                           regex_search(new_line_input,match_inside_while_du,find_poli_invoke_void)) {
                            /* Achei possivel chamada polimorfica - %call2 = call i32 %1(%class.D* %this1) */
                                /* Incluir uso para objeto */
                                Def_ou_Uso new_du;
                                new_du.def_ou_uso_atributo = NULL;
                                int found_it = 0;
                                /* Procura Objeto do use */
                                string line = new_line_input;
                                if(regex_search(line,match_inside_while_du,find_local_obj)
                                    || regex_search(line,match_inside_while_du,find_local_obj2)){
                                                line = line + " %this_value)";}
                                for(int j = 0; j != novo_metodo_du_ponteiro->metodo_objetos.size() && found_it == 0; j++){
                                    regex tem_objeto_dentro (" %" + novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_nome() + "\\)");
                                    if(regex_search(line,match_inside_while_du,tem_objeto_dentro)){
                                        new_du.set_def_ou_uso_objeto_nome(novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_nome());
                                        new_du.set_def_ou_uso_objeto_classe(novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_classe());
                                        found_it = 1;
                                    }
                                }
                                if(found_it == 0){
                                    /* Ver se o objeto � Poli Objetos */
                                    for(int i = 0; i != All_names_var_temp_poli.size() && found_it == 0; i++){
                                        regex find_object_called (" %" + All_names_var_temp_poli[i] + "[\\)|,]");
                                        if(regex_search(line,match_inside_while_du,find_object_called)){
                                            for(int j = 0; j != novo_metodo_du_ponteiro->metodo_objetos.size() && found_it == 0; j++){
                                                if(All_names_obj_poli[i] == novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_nome()){
                                                    new_du.set_def_ou_uso_objeto_nome(novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_nome());
                                                    new_du.set_def_ou_uso_objeto_classe(novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_classe());
                                                    found_it = 1;
                                                }
                                            }
                                        }
                                    }
                                }
                                if(found_it == 1){
                                    /* Atualizando demais atributos */
                                    if(regex_search(new_line_input,match_inside_while_du,find_poli_call_call) ||
                                       regex_search(new_line_input,match_inside_while_du,find_poli_call_invoke)){
                                        new_du.set_def_ou_uso_tipo('D');
                                        new_du.set_def_ou_uso_ordem(ordem);
                                        novo_metodo_du_ponteiro->add_metodo_defs(new_du);
                                    }
                                    else if(regex_search(new_line_input,match_inside_while_du,find_poli_call_void) ||
                                            regex_search(new_line_input,match_inside_while_du,find_poli_invoke_void)){
                                        new_du.set_def_ou_uso_tipo('U');
                                        new_du.set_def_ou_uso_ordem(ordem);
                                        novo_metodo_du_ponteiro->add_metodo_usos(new_du);
                                    }
                                }
                            /* Adicionar chamada polimorfica se ("%[0-9]*\\(") for igual a algum ("%" + ID + "\\(")*/
                                /* criar nova chamada */
                                Chamada new_call;
                                new_call.def_ou_uso_atributo = NULL;
                                new_call.set_def_ou_uso_tipo('P');
                                new_call.set_def_ou_uso_ordem(ordem);
                                new_call.set_chamada_nome_classe_chamada("");
                                new_call.set_chamada_nome_metodo_chamado("");
                                new_call.set_def_ou_uso_objeto_nome(new_du.get_def_ou_uso_objeto_nome());
                                new_call.set_def_ou_uso_objeto_classe(new_du.get_def_ou_uso_objeto_classe());
                                /* Ver se vfn ID � conhecido */
                                found_it = 0;
                                for(int j = 0; j < All_vfn_declaratios_id.size() && found_it == 0; j++){
                                    regex tem_id_dentro ("%" + All_vfn_declaratios_id[j] + "\\(");
                                    if(regex_search(new_line_input,match_inside_while_du,tem_id_dentro)){
                                        new_call.set_chamada_vfn_table_id(All_vfn_declaratios_id[j]);
                                        new_call.set_chamada_vfn_table_posicao(All_vfn_declaratios_table_position[j]);
                                        found_it = 1;
                                    }
                                }
                                if(found_it == 1){
                                    /* Se poli ID foi encontrado, incluir na lista de chamadas do metodo */
                                    novo_metodo_du_ponteiro->add_metodo_chamadas(new_call);
                                }
                        }else
                        /* Testar se a linha � uma aloca��o de objeto - %p1 = alloca %class.Pessoa, align 8 */
                        if(regex_search(new_line_input,match_inside_while_du,find_alloca)){
                            string alloca_obj_nome = regex_replace(match_inside_while_du.str(),clear_alloca_obj_name,"$2");
                            alloca_obj_nome = regex_replace(alloca_obj_nome,clear_alloca_obj_name2,"$2");
                            string alloca_classe_nome = regex_replace(match_inside_while_du.str(),clear_alloca_class_name,"$2");
                            alloca_classe_nome = regex_replace(alloca_classe_nome,clear_alloca_class_name2,"$2");
                            Objeto novo_obj;
                            novo_obj.set_objeto_nome(alloca_obj_nome);
                            if(alloca_obj_nome == "this.addr"){novo_obj.set_objeto_nome("this_value");}
                            novo_obj.set_objeto_classe(alloca_classe_nome);
                            novo_obj.set_objeto_ordem(ordem);
                            novo_metodo_du_ponteiro->add_metodo_objetos(novo_obj);
                            /* Cria��o de Objeto nao � mais def-use */
                        }
                        else /* Testar se a proximas linhas tem um possivel def use */
                        if(regex_search(new_line_input,match_inside_while_du,find_possivel_futuro_du)){
                            /* se for, adicionar na lista de usos */
                            string possivel_futuro_du = regex_replace(match_inside_while_du.str(),clear_find_possivel_futuro_du,"$2");
                            possivel_futuro_du = regex_replace(possivel_futuro_du,clear_find_possivel_futuro_du2,"$2");
                            string possivel_futuro_du_class = regex_replace(match_inside_while_du.str(),clear_find_possivel_futuro_du_class,"$2");
                            possivel_futuro_du_class = regex_replace(possivel_futuro_du_class,clear_find_possivel_futuro_du_class2,"$2");
                            string possivel_futuro_du_obj = regex_replace(match_inside_while_du.str(),clear_find_possivel_futuro_du_obj,"$2");
                            possivel_futuro_du_obj = regex_replace(possivel_futuro_du_obj,clear_find_possivel_futuro_du_obj2,"$2");
                            /* Adicionar a lista de usos somente se ele pertencer a uma classe */
                            int jah_add = 0;
                            for(int i = 0; i != all_classes.size() && jah_add == 0; i++){
                                if(all_classes[i].get_classe_nome() == possivel_futuro_du_class){
                                    /* E se ele � um atributo da classe */
                                    for(int j = 0; j != all_classes[i].classe_atributos.size() && jah_add == 0; j++){
                                        if(all_classes[i].classe_atributos[j].get_atributo_nome() == possivel_futuro_du){
                                            /* Salva atributo, classe e objeto */
                                            All_possible_future_DUs_atributo.push_back(possivel_futuro_du);
                                            All_possible_future_DUs_classe.push_back(possivel_futuro_du_class);
                                            All_possible_future_DUs_obj.push_back(possivel_futuro_du_obj);
                                            jah_add = 1;
                                        }else{
                                            for(int k = 0; k != all_classes[i].classe_atributos[j].atributo_outros_nomes.size() && jah_add == 0; k++){
                                                if(all_classes[i].classe_atributos[j].atributo_outros_nomes[k] == possivel_futuro_du){
                                                    /* Salva atributo, classe e objeto */
                                                    All_possible_future_DUs_atributo.push_back(possivel_futuro_du);
                                                    All_possible_future_DUs_classe.push_back(possivel_futuro_du_class);
                                                    All_possible_future_DUs_obj.push_back(possivel_futuro_du_obj);
                                                    jah_add = 1;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else /* Se for um store, � um def */
                        if(regex_search(new_line_input,match_inside_while_du,find_store)) {
                            /* Ver se algum dos atributos encontrados � utilizado - nao entra aqui */
                            for(int i = 0; i != All_possible_future_DUs_atributo.size(); i++){
                                regex find_nome_do_atributo (".*%" + All_possible_future_DUs_atributo[i] + ",.*");
                                if(regex_search(new_line_input,match_inside_while_du,find_nome_do_atributo)){
                                        /* criar uma linha de defini��o */
                                        Def_ou_Uso new_du;
                                        new_du.def_ou_uso_atributo = NULL;
                                        int found_it = 0;
                                        /* Procura Objeto do def */
                                        if(regex_search(All_possible_future_DUs_obj[i],match_inside_while_du,find_local_obj)
                                           || regex_search(All_possible_future_DUs_obj[i],match_inside_while_du,find_local_obj2)){
                                            All_possible_future_DUs_obj[i] = "this_value";}
                                        for(int j = 0; j != novo_metodo_du_ponteiro->metodo_objetos.size() && found_it == 0; j++){
                                            if(All_possible_future_DUs_obj[i] == novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_nome()){
                                                new_du.set_def_ou_uso_objeto_nome(novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_nome());
                                                new_du.set_def_ou_uso_objeto_classe(novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_classe());
                                                found_it = 1;
                                            }
                                        }
                                        /* Procura Atributo do def */
                                        found_it = 0;
                                        for(int j = 0; j != all_classes.size() && found_it == 0; j++){
                                            for(int k = 0; k != all_classes[j].classe_atributos.size() && found_it == 0; k++){
                                                if(All_possible_future_DUs_atributo[i] == all_classes[j].classe_atributos[k].get_atributo_nome()
                                                   && All_possible_future_DUs_classe[i] == all_classes[j].get_classe_nome()){
                                                    new_du.def_ou_uso_atributo = &all_classes[j].classe_atributos[k];
                                                    found_it = 1;
                                                } else{
                                                    for(int l = 0; l != all_classes[j].classe_atributos[k].atributo_outros_nomes.size() && found_it == 0; l++){
                                                        if(All_possible_future_DUs_atributo[i] == all_classes[j].classe_atributos[k].atributo_outros_nomes[l]
                                                           && All_possible_future_DUs_classe[i] == all_classes[j].get_classe_nome()){
                                                            new_du.def_ou_uso_atributo = &all_classes[j].classe_atributos[k];
                                                            found_it = 1;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if(found_it == 1){
                                            /* Atualizando demais atributos */
                                            new_du.set_def_ou_uso_tipo('D');
                                            new_du.set_def_ou_uso_ordem(ordem);
                                            novo_metodo_du_ponteiro->add_metodo_defs(new_du);
                                            //cout << "Achei um D para: " << All_possible_future_DUs_atributo[i] << " em " << novo_metodo_du_ponteiro->get_metodo_nome() << "\n";
                                        }
                                }
                            }
                        }else /* Se for um load, � um use */
                        if(regex_search(new_line_input,match_inside_while_du,find_load)) {
                            /* Ver se algum dos atributos encontrados � utilizado */
                            for(int i = 0; i != All_possible_future_DUs_atributo.size(); i++){
                                regex find_nome_do_atributo (".*%" + All_possible_future_DUs_atributo[i] + ",.*");
                                if(regex_search(new_line_input,match_inside_while_du,find_nome_do_atributo)){
                                        /* criar uma linha de defini��o */
                                        Def_ou_Uso new_du;
                                        new_du.def_ou_uso_atributo = NULL;
                                        int found_it = 0;
                                        /* Procura Objeto do use */
                                        if(regex_search(All_possible_future_DUs_obj[i],match_inside_while_du,find_local_obj)
                                           || regex_search(All_possible_future_DUs_obj[i],match_inside_while_du,find_local_obj2)){
                                            All_possible_future_DUs_obj[i] = "this_value";}
                                        for(int j = 0; j != novo_metodo_du_ponteiro->metodo_objetos.size() && found_it == 0; j++){
                                            if(All_possible_future_DUs_obj[i] == novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_nome()){
                                                new_du.set_def_ou_uso_objeto_nome(novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_nome());
                                                new_du.set_def_ou_uso_objeto_classe(novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_classe());
                                                found_it = 1;
                                            }
                                        }
                                        /* Procura Atributo do use */
                                        found_it = 0;
                                        for(int j = 0; j != all_classes.size() && found_it == 0; j++){
                                            for(int k = 0; k != all_classes[j].classe_atributos.size() && found_it == 0; k++){
                                                if(All_possible_future_DUs_atributo[i] == all_classes[j].classe_atributos[k].get_atributo_nome()
                                                   && All_possible_future_DUs_classe[i] == all_classes[j].get_classe_nome()){
                                                    new_du.def_ou_uso_atributo = &all_classes[j].classe_atributos[k];
                                                    found_it = 1;
                                                } else{
                                                    for(int l = 0; l != all_classes[j].classe_atributos[k].atributo_outros_nomes.size() && found_it == 0; l++){
                                                        if(All_possible_future_DUs_atributo[i] == all_classes[j].classe_atributos[k].atributo_outros_nomes[l]
                                                           && All_possible_future_DUs_classe[i] == all_classes[j].get_classe_nome()){
                                                            new_du.def_ou_uso_atributo = &all_classes[j].classe_atributos[k];
                                                            found_it = 1;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        /* Atualizando demais atributos */
                                        new_du.set_def_ou_uso_tipo('U');
                                        new_du.set_def_ou_uso_ordem(ordem);
                                        novo_metodo_du_ponteiro->add_metodo_usos(new_du);
                                        //cout << "Achei um U para: " << All_possible_future_DUs_atributo[i] << " em " << novo_metodo_du_ponteiro->get_metodo_nome() << "\n";
                                }
                            }
                        }else /* Se for uma chamada void - � um USE, entao incluir na lista de uses +
                                 criar chamada com ponteiro pro use e pro metodo*/
                        if(regex_search(new_line_input,match_inside_while_du,find_call_void)
                           || regex_search(new_line_input,match_inside_while_du,find_invoke_void)) {
                            /* Ver se algum dos atributos encontrados � utilizado */
                            for(int i = 0; i != All_possible_future_DUs_atributo.size(); i++){
                                regex find_nome_do_atributo (".*%" + All_possible_future_DUs_atributo[i] + ".*");
                                /* TESTE1 - INCLUIR USOS PARA TODOS OS ATRIBUTOS na lista de usos do metodo */
                                if(regex_search(new_line_input,match_inside_while_du,find_nome_do_atributo)){
                                        /* criar uma linha de defini��o */
                                        Def_ou_Uso new_du;
                                        new_du.def_ou_uso_atributo = NULL;
                                        int found_it = 0;
                                        /* Procura Objeto do use */
                                        if(regex_search(All_possible_future_DUs_obj[i],match_inside_while_du,find_local_obj)
                                           || regex_search(All_possible_future_DUs_obj[i],match_inside_while_du,find_local_obj2)){
                                            All_possible_future_DUs_obj[i] = "this_value";}
                                        for(int j = 0; j != novo_metodo_du_ponteiro->metodo_objetos.size() && found_it == 0; j++){
                                            if(All_possible_future_DUs_obj[i] == novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_nome()){
                                                new_du.set_def_ou_uso_objeto_nome(novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_nome());
                                                new_du.set_def_ou_uso_objeto_classe(novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_classe());
                                                found_it = 1;
                                            }
                                        }
                                        if(found_it == 0){
                                            /* Ver se o objeto � Poli Objetos */
                                            for(int k = 0; k != All_names_var_temp_poli.size() && found_it == 0; k++){
                                                regex find_object_called (" %" + All_names_var_temp_poli[k] + "[\\)|,]");
                                                if(regex_search(new_line_input,match_inside_while_du,find_object_called)){
                                                    for(int l = 0; l != novo_metodo_du_ponteiro->metodo_objetos.size() && found_it == 0; l++){
                                                        if(All_names_obj_poli[k] == novo_metodo_du_ponteiro->metodo_objetos[l].get_objeto_nome()){
                                                            new_du.set_def_ou_uso_objeto_nome(novo_metodo_du_ponteiro->metodo_objetos[l].get_objeto_nome());
                                                            new_du.set_def_ou_uso_objeto_classe(novo_metodo_du_ponteiro->metodo_objetos[l].get_objeto_classe());
                                                            found_it = 1;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        /* Procura Atributo do use */
                                        found_it = 0;
                                        for(int j = 0; j != all_classes.size() && found_it == 0; j++){
                                            for(int k = 0; k != all_classes[j].classe_atributos.size() && found_it == 0; k++){
                                                if(All_possible_future_DUs_atributo[i] == all_classes[j].classe_atributos[k].get_atributo_nome()
                                                   && All_possible_future_DUs_classe[i] == all_classes[j].get_classe_nome()){
                                                    new_du.def_ou_uso_atributo = &all_classes[j].classe_atributos[k];
                                                    found_it = 1;
                                                } else{
                                                    for(int l = 0; l != all_classes[j].classe_atributos[k].atributo_outros_nomes.size() && found_it == 0; l++){
                                                        if(All_possible_future_DUs_atributo[i] == all_classes[j].classe_atributos[k].atributo_outros_nomes[l]
                                                           && All_possible_future_DUs_classe[i] == all_classes[j].get_classe_nome()){
                                                            new_du.def_ou_uso_atributo = &all_classes[j].classe_atributos[k];
                                                            found_it = 1;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if(found_it == 1){
                                            /* Atualizando demais atributos */
                                            new_du.set_def_ou_uso_tipo('U');
                                            new_du.set_def_ou_uso_ordem(ordem);
                                            novo_metodo_du_ponteiro->add_metodo_usos(new_du);
                                            //cout << "Achei um U de C para: " << All_possible_future_DUs_atributo[i] << " em " << novo_metodo_du_ponteiro->get_metodo_nome() << "\n";
                                        }
                                }
                            }
                            /* TESTE2 - INCLUIR CHAMADA NA LISTA DE CHAMADAS SE METODO EXISTIR - + OBJ*/
                            /* criar nova chamada */
                            Chamada new_call;
                            new_call.def_ou_uso_atributo = NULL;
                            new_call.set_def_ou_uso_tipo('C');
                            new_call.set_def_ou_uso_ordem(ordem);
                            new_call.set_chamada_nome_classe_chamada("");
                            new_call.set_chamada_nome_metodo_chamado("");
                            /* Find entre todos os metodos, qual � o chamado */
                            /* FAZENDO ISSO ESTOU IGNORANDO AS CHAMADAS PARA METODOS QUE NAO SAO DAS CLASSES */
                            int found = 0;
                            for(int i = 0; i != all_classes.size() && found == 0; i++){
                                for(int j = 0; j != all_classes[i].classe_metodos.size() && found == 0; j++){
                                    regex find_method_called (".*" + all_classes[i].classe_metodos[j].get_metodo_nome() + ".*");
                                    if(regex_search(new_line_input,match_inside_while_du,find_method_called)){
                                        new_call.set_chamada_nome_metodo_chamado(all_classes[i].classe_metodos[j].get_metodo_nome());
                                        new_call.set_chamada_nome_classe_chamada(all_classes[i].get_classe_nome());
                                        found = 1;
                                    }
                                }
                            }
                            if(found == 1){
                                int validando_obj = 0;
                                /* Find objeto, entre todos os objetos do metodo*/
                                vector <Objeto> objs_do_metodo_corrente = novo_metodo_du_ponteiro->get_metodo_objetos();
                                for(int i = 0; i != objs_do_metodo_corrente.size() && validando_obj == 0; i++){
                                    if(objs_do_metodo_corrente[i].get_objeto_nome() == "this_value"){
                                        regex find_object_called (".*%this.*");
                                        if(regex_search(new_line_input,match_inside_while_du,find_object_called)){
                                            new_call.set_def_ou_uso_objeto_nome(objs_do_metodo_corrente[i].get_objeto_nome());
                                            new_call.set_def_ou_uso_objeto_classe(objs_do_metodo_corrente[i].get_objeto_classe());
                                            validando_obj = 1;
                                        }
                                    }else{
                                        regex find_object_called (".*%" + objs_do_metodo_corrente[i].get_objeto_nome() + "[\\)|,].*");
                                        if(regex_search(new_line_input,match_inside_while_du,find_object_called)){
                                            new_call.set_def_ou_uso_objeto_nome(objs_do_metodo_corrente[i].get_objeto_nome());
                                            new_call.set_def_ou_uso_objeto_classe(objs_do_metodo_corrente[i].get_objeto_classe());
                                            validando_obj = 1;
                                        }
                                    }
                                }
                                /* Poli Objetos */
                                if(validando_obj == 0){
                                    for(int i = 0; i != All_names_var_temp_poli.size() && validando_obj == 0; i++){
                                        regex find_object_called (".*%" + All_names_var_temp_poli[i] + "[\\)|,].*");
                                        if(regex_search(new_line_input,match_inside_while_du,find_object_called)){
                                            for(int j = 0; j != objs_do_metodo_corrente.size() && validando_obj == 0; j++){
                                               if(All_names_obj_poli[i] == objs_do_metodo_corrente[j].get_objeto_nome()){
                                                    new_call.set_def_ou_uso_objeto_nome(objs_do_metodo_corrente[j].get_objeto_nome());
                                                    new_call.set_def_ou_uso_objeto_classe(objs_do_metodo_corrente[j].get_objeto_classe());
                                                    validando_obj = 1;
                                                }
                                            }
                                        }
                                    }
                                }
                                /* Se metodo foi encontrado, incluir na lista de chamadas do metodo */
                                novo_metodo_du_ponteiro->add_metodo_chamadas(new_call);
                                //cout << "Achei um C para: " << new_call.get_chamada_nome_metodo_chamado();  if(new_call.def_ou_uso_objeto != NULL){ cout << " do objeto " << new_call.def_ou_uso_objeto->get_objeto_nome();} cout  << "\n";
                            }
                        }else
                        if(regex_search(new_line_input,match_inside_while_du,find_call_call)
                           || regex_search(new_line_input,match_inside_while_du,find_call_invoke)) {
                            /* Ver se algum dos atributos encontrados � utilizado */
                            for(int i = 0; i != All_possible_future_DUs_atributo.size(); i++){
                                regex find_nome_do_atributo (".*%" + All_possible_future_DUs_atributo[i] + ".*");
                                /* TESTE1 - INCLUIR USOS PARA TODOS OS ATRIBUTOS na lista de usos do metodo */
                                if(regex_search(new_line_input,match_inside_while_du,find_nome_do_atributo)){
                                        /* criar uma linha de defini��o */
                                        Def_ou_Uso new_du;
                                        new_du.def_ou_uso_atributo = NULL;
                                        int found_it = 0;
                                        /* Procura Objeto do use */
                                        if(regex_search(All_possible_future_DUs_obj[i],match_inside_while_du,find_local_obj)
                                           || regex_search(All_possible_future_DUs_obj[i],match_inside_while_du,find_local_obj2)){
                                            All_possible_future_DUs_obj[i] = "this_value";}
                                        for(int j = 0; j != novo_metodo_du_ponteiro->metodo_objetos.size() && found_it == 0; j++){
                                            if(All_possible_future_DUs_obj[i] == novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_nome()){
                                                new_du.set_def_ou_uso_objeto_nome(novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_nome());
                                                new_du.set_def_ou_uso_objeto_classe(novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_classe());
                                                found_it = 1;
                                            }
                                        }
                                        if(found_it == 0){
                                            /* Ver se o objeto � Poli Objetos */
                                            for(int k = 0; k != All_names_var_temp_poli.size() && found_it == 0; k++){
                                                regex find_object_called (" %" + All_names_var_temp_poli[k] + "[\\)|,]");
                                                if(regex_search(new_line_input,match_inside_while_du,find_object_called)){
                                                    for(int l = 0; l != novo_metodo_du_ponteiro->metodo_objetos.size() && found_it == 0; l++){
                                                        if(All_names_obj_poli[k] == novo_metodo_du_ponteiro->metodo_objetos[l].get_objeto_nome()){
                                                            new_du.set_def_ou_uso_objeto_nome(novo_metodo_du_ponteiro->metodo_objetos[l].get_objeto_nome());
                                                            new_du.set_def_ou_uso_objeto_classe(novo_metodo_du_ponteiro->metodo_objetos[l].get_objeto_classe());
                                                            found_it = 1;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        /* Procura Atributo do use */
                                        found_it = 0;
                                        for(int j = 0; j != all_classes.size() && found_it == 0; j++){
                                            for(int k = 0; k != all_classes[j].classe_atributos.size() && found_it == 0; k++){
                                                if(All_possible_future_DUs_atributo[i] == all_classes[j].classe_atributos[k].get_atributo_nome()
                                                   && All_possible_future_DUs_classe[i] == all_classes[j].get_classe_nome()){
                                                    new_du.def_ou_uso_atributo = &all_classes[j].classe_atributos[k];
                                                    found_it = 1;
                                                } else{
                                                    for(int l = 0; l != all_classes[j].classe_atributos[k].atributo_outros_nomes.size() && found_it == 0; l++){
                                                        if(All_possible_future_DUs_atributo[i] == all_classes[j].classe_atributos[k].atributo_outros_nomes[l]
                                                           && All_possible_future_DUs_classe[i] == all_classes[j].get_classe_nome()){
                                                            new_du.def_ou_uso_atributo = &all_classes[j].classe_atributos[k];
                                                            found_it = 1;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if(found_it == 1){
                                            /* Atualizando demais atributos */
                                            new_du.set_def_ou_uso_tipo('D');
                                            new_du.set_def_ou_uso_ordem(ordem);
                                            novo_metodo_du_ponteiro->add_metodo_defs(new_du);
                                            //cout << "Achei um D de C para: " << All_possible_future_DUs_atributo[i] << " em " << novo_metodo_du_ponteiro->get_metodo_nome() << "\n";
                                        }
                                }
                            }
                            /* TESTE2 - INCLUIR CHAMADA NA LISTA DE CHAMADAS SE METODO EXISTIR - + OBJ*/
                            /* criar nova chamada */
                            Chamada new_call;
                            new_call.def_ou_uso_atributo = NULL;
                            new_call.set_def_ou_uso_tipo('C');
                            new_call.set_def_ou_uso_ordem(ordem);
                            new_call.set_chamada_nome_classe_chamada("");
                            new_call.set_chamada_nome_metodo_chamado("");
                            /* Find entre todos os metodos, qual � o chamado */
                            /* FAZENDO ISSO ESTOU IGNORANDO AS CHAMADAS PARA METODOS QUE NAO SAO DAS CLASSES */
                            int found = 0;
                            for(int i = 0; i != all_classes.size() && found == 0; i++){
                                for(int j = 0; j != all_classes[i].classe_metodos.size() && found == 0; j++){
                                    regex find_method_called (".*" + all_classes[i].classe_metodos[j].get_metodo_nome() + ".*");
                                    if(regex_search(new_line_input,match_inside_while_du,find_method_called)){
                                        new_call.set_chamada_nome_metodo_chamado(all_classes[i].classe_metodos[j].get_metodo_nome());
                                        new_call.set_chamada_nome_classe_chamada(all_classes[i].get_classe_nome());
                                        found = 1;
                                    }
                                }
                            }
                            if(found == 1){
                                int validando_obj = 0;
                                /* Find objeto, entre todos os objetos do metodo*/
                                vector <Objeto> objs_do_metodo_corrente = novo_metodo_du_ponteiro->get_metodo_objetos();
                                for(int i = 0; i != objs_do_metodo_corrente.size() && validando_obj == 0; i++){
                                    if(objs_do_metodo_corrente[i].get_objeto_nome() == "this_value"){
                                        regex find_object_called (".*%this.*");
                                        if(regex_search(new_line_input,match_inside_while_du,find_object_called)){
                                            new_call.set_def_ou_uso_objeto_nome(objs_do_metodo_corrente[i].get_objeto_nome());
                                            new_call.set_def_ou_uso_objeto_classe(objs_do_metodo_corrente[i].get_objeto_classe());
                                            validando_obj = 1;
                                        }
                                    }else{
                                        regex find_object_called (".*%" + objs_do_metodo_corrente[i].get_objeto_nome() + "[\\)|,].*");
                                        if(regex_search(new_line_input,match_inside_while_du,find_object_called)){
                                            new_call.set_def_ou_uso_objeto_nome(objs_do_metodo_corrente[i].get_objeto_nome());
                                            new_call.set_def_ou_uso_objeto_classe(objs_do_metodo_corrente[i].get_objeto_classe());
                                            validando_obj = 1;
                                        }
                                    }
                                }
                                /* Poli Objetos */
                                if(validando_obj == 0){
                                    for(int i = 0; i != All_names_var_temp_poli.size() && validando_obj == 0; i++){
                                        regex find_object_called (".*%" + All_names_var_temp_poli[i] + "[\\)|,].*");
                                        if(regex_search(new_line_input,match_inside_while_du,find_object_called)){
                                            for(int j = 0; j != objs_do_metodo_corrente.size() && validando_obj == 0; j++){
                                               if(All_names_obj_poli[i] == objs_do_metodo_corrente[j].get_objeto_nome()){
                                                    new_call.set_def_ou_uso_objeto_nome(objs_do_metodo_corrente[j].get_objeto_nome());
                                                    new_call.set_def_ou_uso_objeto_classe(objs_do_metodo_corrente[j].get_objeto_classe());
                                                    validando_obj = 1;
                                                }
                                            }
                                        }
                                    }
                                }
                                /* Se metodo foi encontrado, incluir na lista de chamadas do metodo */
                                novo_metodo_du_ponteiro->add_metodo_chamadas(new_call);
                                //cout << "Achei um C para: " << new_call.get_chamada_nome_metodo_chamado();  if(new_call.def_ou_uso_objeto != NULL){ cout << " do objeto " << new_call.def_ou_uso_objeto->get_objeto_nome();} cout  << "\n";
                            }
                        }else /* se for um = bitcast de polimorfismo */
                        if(regex_search(new_line_input,match_inside_while_du,find_bitcast)) {
                            /* achei possivel declara��o temporaria de polimorfismo */
                            string name_tmp = regex_replace(match_inside_while_du.str(),clear_bitcast_tmp_name,"$2");
                            name_tmp = regex_replace(name_tmp,clear_bitcast_tmp_name2,"$2");
                            string obj_name = regex_replace(match_inside_while_du.str(),clear_bitcast_obj_name,"$2");
                            obj_name = regex_replace(obj_name,clear_bitcast_obj_name2,"$2");
                            string classe_1 = regex_replace(match_inside_while_du.str(),clear_bitcast_classe1,"$2");
                            classe_1 = regex_replace(classe_1,clear_bitcast_classe12,"$2");
                            string classe_2 = regex_replace(match_inside_while_du.str(),clear_bitcast_classe2,"$2");
                            classe_2 = regex_replace(classe_2,clear_bitcast_classe22,"$2");
                            /* incluir declara��o na lista temporaria s� se ambas classes e obj existirem */
                            int valida_obj = 0;
                            int valida_classe1 = 0;
                            int valida_classe2 = 0;
                            /* Testar se obj existe */
                            if(regex_search(obj_name,match_inside_while_du,this_found)){obj_name = "this_value";}
                            for(int j = 0; j != novo_metodo_du_ponteiro->metodo_objetos.size() && valida_obj == 0; j++){
                                if(obj_name == novo_metodo_du_ponteiro->metodo_objetos[j].get_objeto_nome()){
                                    valida_obj = 1;
                                }
                            }
                            /* Testar se classe_1 e classe_2 existe */
                            for(int j = 0; j != all_classes.size(); j++){
                                if(classe_1 == all_classes[j].get_classe_nome()){valida_classe1 = 1;}
                                if(classe_2 == all_classes[j].get_classe_nome()){valida_classe2 = 1;}
                            }
                            //cout << "name_tmp : " << name_tmp << " obj_name : " << obj_name << " classe_1 : " << classe_1 << " classe_2 : " << classe_2 << "\n";
                            /* valida��o ok, incluir na lista */
                            if(valida_obj == 1 && valida_classe1 == 1 && valida_classe2 == 1){
                                All_names_var_temp_poli.push_back(name_tmp);
                                All_names_obj_poli.push_back(obj_name);
                            }
                        }
                    /* Analisei toda esta linha, partir pra proxima */
                    ordem++;
                    getline(arquivo,new_line_input);
                }
            }
        }
    }
    arquivo.close();


/* ------------------ Criar estruturas basicas de busca */
            cout << "Criando estruturas...\n";
            /* Criar uma lista com todos os metodos */
            vector<Metodo> all_methods;
            all_methods.clear();
            for(int i = 0; i != all_classes.size(); i++){
                for(int j = 0; j != all_classes[i].classe_metodos.size(); j++){
                    all_methods.push_back(all_classes[i].classe_metodos[j]);
                }
            }
            /* Criar uma lista com vtable */
            int pos_2 = -1;
            vector<vector<string>> vtable;
            int vtable_size = 0;
            for(int i = 0; i != all_classes.size(); i++){
                if(all_classes[i].classe_vtable.size() > 0){vtable_size++;}
            }
            vtable.resize(vtable_size);
            for(int i = 0; i != all_classes.size(); i++){
                if(all_classes[i].classe_vtable.size() > 0){
                    pos_2++;
                    vtable[pos_2].push_back(all_classes[i].get_classe_nome());
                    for(int j = 0; j != all_classes[i].classe_vtable.size(); j++){
                        vtable[pos_2].push_back(all_classes[i].classe_vtable[j]);
                    }
                }
            }

/* ------------------ Salvando estruturas em arquivos */
    /* ----Salvar Tabela de Polimorfismo em arquivo---- */
        cout << "Gravando tabela de polimorfismo...\n";
        ofstream polimorfismo_file;
        int maior_lista = 0;
        for(int i = 0; i != all_classes.size(); i++){
            if(all_classes[i].classe_vtable.size() > maior_lista){maior_lista = all_classes[i].classe_vtable.size();}
        }
        polimorfismo_file.open ("output/Tabela de Polimorfismo.txt");
        if(maior_lista > 0){
            for(int i = 0; i < maior_lista; i++){
                polimorfismo_file << "\t" << i;
            }polimorfismo_file << "\n";
            for(int i = 0; i < all_classes.size(); i++){
                polimorfismo_file << all_classes[i].get_classe_nome() << "\t";
                for(int j = 0; j != all_classes[i].classe_vtable.size(); j++){
                    polimorfismo_file << all_classes[i].classe_vtable[j] << "\t";
                }
                polimorfismo_file << "\n";
            }
        }else{ polimorfismo_file << "N�o existem tabelas de polimorfismo para o codigo selecionado!\n"; }
        polimorfismo_file.close();

    /* ---Salvar todas as classes e metodos em arquivo--- */
        cout << "Gravando classes...\n";
        ofstream all_file;
        all_file.open ("output/Classes.txt");
        all_file << "\n\n";
        /* Exibir todas as classes */
        Output out;
        out.output_all_classes(all_classes,&all_file);
        /* Exibir classe Main */
        out.output_main_method(main_method,&all_file);
        all_file.close();

    /* Imprimir FLOW em arquivo */
        cout << "Gravando fluxo de chamadas...\n";
        flow_file.open ("output/Fluxo.txt");
        /* Chamar impress�o de metodos chamados - com Polimorfismo */
        main_method.show_all_methods(all_methods,1,vtable,"");
        flow_file.close();


/* ------------------ Identificando issues - gerando arquivos */
        /* ITU - Para cada par pai e filho, exibir defs, uses e chamadas */
            cout << "Verificando ITU...\n";
            ofstream file_ITU;
            file_ITU.open ("output/ITU.txt"); file_ITU << "\n";
            out.output_check_ITU(all_classes,vtable,&file_ITU);
            file_ITU.close();
        /* SDA - para cada polimorfismo encontrado, exibir compara��o entre def e uses */
            cout << "Verificando SDA...\n";
            ofstream file_SDA;
            file_SDA.open ("output/SDA.txt"); file_SDA << "\n";
            out.output_check_SDA(all_methods,vtable,&file_SDA);
            file_SDA.close();
        /* SDIH - para cada cadeia de pais e filhos, exibir todos os atributos de mesmo nome */
            cout << "Verificando SDIH...\n";
            ofstream file_SDIH;
            file_SDIH.open ("output/SDIH.txt"); file_SDIH << "\n";
            out.output_check_SDIH(all_classes,&file_SDIH);
            file_SDIH.close();
        /* SDI - para cada cadeia de pais e filhos, exibir todos os atributos de mesmo nome */
            cout << "Verificando SDI...\n";
            ofstream file_SDI;
            file_SDI.open ("output/SDI.txt"); file_SDI << "\n";
            out.output_check_SDI(all_classes,all_methods,vtable,&file_SDI);
            file_SDI.close();
        /* ACB1 */
            cout << "Verificando ACB1...\n";
            ofstream file_ACB1;
            file_ACB1.open ("output/ACB1.txt"); file_ACB1 << "\n";
            out.output_check_ACB1(all_methods,vtable,&file_ACB1);
            file_ACB1.close();
        /* ACB2 */
            cout << "Verificando ACB2...\n";
            ofstream file_ACB2;
            file_ACB2.open ("output/ACB2.txt"); file_ACB2 << "\n";
            out.output_check_ACB2(all_classes,all_methods,vtable,&file_ACB2);
            file_ACB2.close();
        /* IC */
            cout << "Verificando IC...\n";
            ofstream file_IC;
            file_IC.open ("output/IC.txt"); file_IC << "\n";
            out.output_check_IC(all_classes,&file_IC);
            file_IC.close();
        /* Gerar YoYo */
            cout << "Verificando YoYo...\n";
            file_YOYO.open ("output/YoYo.txt"); file_YOYO << "\n";
            out.output_gerar_yoyo(all_methods,vtable);
            file_YOYO.close();
    return 0;
}
