; ModuleID = 'IC.cpp'
target datalayout = "e-m:w-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-w64-windows-gnu"

%"class.std::ios_base::Init" = type { i8 }
%"class.std::basic_ostream" = type { i32 (...)**, %"class.std::basic_ios" }
%"class.std::basic_ios" = type { %"class.std::ios_base", %"class.std::basic_ostream"*, i8, i8, %"class.std::basic_streambuf"*, %"class.std::ctype"*, %"class.std::num_put"*, %"class.std::num_get"* }
%"class.std::ios_base" = type { i32 (...)**, i64, i64, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"class.std::locale" }
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"class.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type <{ i8*, i32, [4 x i8] }>
%"class.std::locale" = type { %"class.std::locale::_Impl"* }
%"class.std::locale::_Impl" = type { i32, %"class.std::locale::facet"**, i64, %"class.std::locale::facet"**, i8** }
%"class.std::locale::facet" = type <{ i32 (...)**, i32, [4 x i8] }>
%"class.std::basic_streambuf" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"class.std::locale" }
%"class.std::ctype" = type <{ %"class.std::locale::facet.base", [4 x i8], i32*, i8, [7 x i8], i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8, [6 x i8] }>
%"class.std::locale::facet.base" = type <{ i32 (...)**, i32 }>
%"class.std::num_put" = type { %"class.std::locale::facet.base", [4 x i8] }
%"class.std::num_get" = type { %"class.std::locale::facet.base", [4 x i8] }
%class.D = type <{ i32 (...)**, i32, i8, i8, [2 x i8], i32, [4 x i8], %class.C }>
%class.C = type { i32 (...)**, i32, i8, %"class.std::__cxx11::basic_string" }
%"class.std::__cxx11::basic_string" = type { %"struct.std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider", i64, %union.anon }
%"struct.std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider" = type { i8* }
%union.anon = type { i64, [8 x i8] }

$_ZN1DC1Ev = comdat any

$_ZN1DD1Ev = comdat any

$_ZN1CC2Ev = comdat any

$_ZN1D1fEv = comdat any

$_ZTv0_n24_N1D1fEv = comdat any

$_ZN1C1fEv = comdat any

$_ZN1DD2Ev = comdat any

$_ZN1CD2Ev = comdat any

$_ZTV1D = comdat any

$_ZTT1D = comdat any

$_ZTS1D = comdat any

$_ZTS1C = comdat any

$_ZTI1C = comdat any

$_ZTI1D = comdat any

$_ZTV1C = comdat any

@_ZStL8__ioinit = internal global %"class.std::ios_base::Init" zeroinitializer, align 1
@_ZTV1D = linkonce_odr unnamed_addr constant [8 x i8*] [i8* inttoptr (i64 24 to i8*), i8* null, i8* bitcast ({ i8*, i8*, i32, i32, i8*, i32 }* @_ZTI1D to i8*), i8* bitcast (void (%class.D*)* @_ZN1D1fEv to i8*), i8* inttoptr (i64 -24 to i8*), i8* inttoptr (i64 -24 to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i8*, i32 }* @_ZTI1D to i8*), i8* bitcast (void (%class.D*)* @_ZTv0_n24_N1D1fEv to i8*)], comdat, align 8
@_ZTT1D = linkonce_odr unnamed_addr constant [2 x i8*] [i8* bitcast (i8** getelementptr inbounds ([8 x i8*], [8 x i8*]* @_ZTV1D, i64 0, i64 3) to i8*), i8* bitcast (i8** getelementptr inbounds ([8 x i8*], [8 x i8*]* @_ZTV1D, i64 0, i64 7) to i8*)], comdat
@_ZTVN10__cxxabiv121__vmi_class_type_infoE = external global i8*
@_ZTS1D = linkonce_odr constant [3 x i8] c"1D\00", comdat
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS1C = linkonce_odr constant [3 x i8] c"1C\00", comdat
@_ZTI1C = linkonce_odr constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([3 x i8], [3 x i8]* @_ZTS1C, i32 0, i32 0) }, comdat
@_ZTI1D = linkonce_odr constant { i8*, i8*, i32, i32, i8*, i32 } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv121__vmi_class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([3 x i8], [3 x i8]* @_ZTS1D, i32 0, i32 0), i32 0, i32 1, i8* bitcast ({ i8*, i8* }* @_ZTI1C to i8*), i32 -6141 }, comdat
@_ZTV1C = linkonce_odr unnamed_addr constant [3 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI1C to i8*), i8* bitcast (void (%class.C*)* @_ZN1C1fEv to i8*)], comdat, align 8
@_ZSt4cout = external global %"class.std::basic_ostream", align 8
@.str = private unnamed_addr constant [7 x i8] c"C.f()\0A\00", align 1
@.str.1 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.2 = private unnamed_addr constant [7 x i8] c"D.f()\0A\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_IC.cpp, i8* null }]

define internal void @__cxx_global_var_init() #0 {
entry:
  call void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  %0 = call i32 @atexit(void ()* @__dtor__ZStL8__ioinit) #2
  ret void
}

declare void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"*) #0

; Function Attrs: nounwind
declare void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"*) #1

define internal void @__dtor__ZStL8__ioinit() #0 {
entry:
  call void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  ret void
}

; Function Attrs: nounwind
declare i32 @atexit(void ()*) #2

; Function Attrs: uwtable
define i32 @main() #3 {
entry:
  %retval = alloca i32, align 4
  %obj_d = alloca %class.D, align 8
  store i32 0, i32* %retval
  call void @_ZN1DC1Ev(%class.D* %obj_d)
  store i32 0, i32* %retval
  call void @_ZN1DD1Ev(%class.D* %obj_d) #2
  %0 = load i32, i32* %retval
  ret i32 %0
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN1DC1Ev(%class.D* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.D*, align 8
  store %class.D* %this, %class.D** %this.addr, align 8
  %this1 = load %class.D*, %class.D** %this.addr
  %0 = bitcast %class.D* %this1 to i8*
  %1 = getelementptr inbounds i8, i8* %0, i64 24
  %2 = bitcast i8* %1 to %class.C*
  call void @_ZN1CC2Ev(%class.C* %2)
  %3 = bitcast %class.D* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([8 x i8*], [8 x i8*]* @_ZTV1D, i64 0, i64 3) to i32 (...)**), i32 (...)*** %3
  %4 = bitcast %class.D* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %4, i64 24
  %5 = bitcast i8* %add.ptr to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([8 x i8*], [8 x i8*]* @_ZTV1D, i64 0, i64 7) to i32 (...)**), i32 (...)*** %5
  %b = getelementptr inbounds %class.D, %class.D* %this1, i32 0, i32 3
  store i8 0, i8* %b, align 1
  ret void
}

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN1DD1Ev(%class.D* %this) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.D*, align 8
  store %class.D* %this, %class.D** %this.addr, align 8
  %this1 = load %class.D*, %class.D** %this.addr
  call void @_ZN1DD2Ev(%class.D* %this1, i8** getelementptr inbounds ([2 x i8*], [2 x i8*]* @_ZTT1D, i64 0, i64 0)) #2
  %0 = bitcast %class.D* %this1 to i8*
  %1 = getelementptr inbounds i8, i8* %0, i64 24
  %2 = bitcast i8* %1 to %class.C*
  call void @_ZN1CD2Ev(%class.C* %2) #2
  ret void
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN1CC2Ev(%class.C* %this) unnamed_addr #5 comdat align 2 {
entry:
  %this.addr = alloca %class.C*, align 8
  store %class.C* %this, %class.C** %this.addr, align 8
  %this1 = load %class.C*, %class.C** %this.addr
  %0 = bitcast %class.C* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([3 x i8*], [3 x i8*]* @_ZTV1C, i64 0, i64 2) to i32 (...)**), i32 (...)*** %0
  %z = getelementptr inbounds %class.C, %class.C* %this1, i32 0, i32 3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* %z) #2
  %x = getelementptr inbounds %class.C, %class.C* %this1, i32 0, i32 1
  store i32 0, i32* %x, align 4
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN1D1fEv(%class.D* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.D*, align 8
  store %class.D* %this, %class.D** %this.addr, align 8
  %this1 = load %class.D*, %class.D** %this.addr
  %x = getelementptr inbounds %class.D, %class.D* %this1, i32 0, i32 1
  %0 = load i32, i32* %x, align 4
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* @_ZSt4cout, i32 %0)
  %call2 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.1, i32 0, i32 0))
  %call3 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.2, i32 0, i32 0))
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZTv0_n24_N1D1fEv(%class.D* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.D*, align 8
  store %class.D* %this, %class.D** %this.addr, align 8
  %this1 = load %class.D*, %class.D** %this.addr
  %0 = bitcast %class.D* %this1 to i8*
  %1 = bitcast i8* %0 to i8**
  %2 = load i8*, i8** %1
  %3 = getelementptr inbounds i8, i8* %2, i64 -24
  %4 = bitcast i8* %3 to i64*
  %5 = load i64, i64* %4
  %6 = getelementptr inbounds i8, i8* %0, i64 %5
  %7 = bitcast i8* %6 to %class.D*
  call void @_ZN1D1fEv(%class.D* %7)
  ret void
}

; Function Attrs: nounwind
declare void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"*) #1

; Function Attrs: uwtable
define linkonce_odr void @_ZN1C1fEv(%class.C* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.C*, align 8
  store %class.C* %this, %class.C** %this.addr, align 8
  %this1 = load %class.C*, %class.C** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str, i32 0, i32 0))
  ret void
}

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272), i8*) #0

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"*, i32) #0

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN1DD2Ev(%class.D* %this, i8** %vtt) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.D*, align 8
  %vtt.addr = alloca i8**, align 8
  store %class.D* %this, %class.D** %this.addr, align 8
  store i8** %vtt, i8*** %vtt.addr, align 8
  %this1 = load %class.D*, %class.D** %this.addr
  %vtt2 = load i8**, i8*** %vtt.addr
  ret void
}

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN1CD2Ev(%class.C* %this) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.C*, align 8
  store %class.C* %this, %class.C** %this.addr, align 8
  %this1 = load %class.C*, %class.C** %this.addr
  %0 = bitcast %class.C* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([3 x i8*], [3 x i8*]* @_ZTV1C, i64 0, i64 2) to i32 (...)**), i32 (...)*** %0
  %z = getelementptr inbounds %class.C, %class.C* %this1, i32 0, i32 3
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %z) #2
  ret void
}

; Function Attrs: nounwind
declare void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"*) #1

define internal void @_GLOBAL__sub_I_IC.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }
attributes #3 = { uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
