; ModuleID = 'Classes.cpp'
target datalayout = "e-m:w-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-w64-windows-gnu"

%"class.std::ios_base::Init" = type { i8 }
%"class.std::basic_ostream" = type { i32 (...)**, %"class.std::basic_ios" }
%"class.std::basic_ios" = type { %"class.std::ios_base", %"class.std::basic_ostream"*, i8, i8, %"class.std::basic_streambuf"*, %"class.std::ctype"*, %"class.std::num_put"*, %"class.std::num_get"* }
%"class.std::ios_base" = type { i32 (...)**, i64, i64, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"class.std::locale" }
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"class.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type <{ i8*, i32, [4 x i8] }>
%"class.std::locale" = type { %"class.std::locale::_Impl"* }
%"class.std::locale::_Impl" = type { i32, %"class.std::locale::facet"**, i64, %"class.std::locale::facet"**, i8** }
%"class.std::locale::facet" = type <{ i32 (...)**, i32, [4 x i8] }>
%"class.std::basic_streambuf" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"class.std::locale" }
%"class.std::ctype" = type <{ %"class.std::locale::facet.base", [4 x i8], i32*, i8, [7 x i8], i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8, [6 x i8] }>
%"class.std::locale::facet.base" = type <{ i32 (...)**, i32 }>
%"class.std::num_put" = type { %"class.std::locale::facet.base", [4 x i8] }
%"class.std::num_get" = type { %"class.std::locale::facet.base", [4 x i8] }
%class.Person = type <{ %"class.std::__cxx11::basic_string", i32, [4 x i8] }>
%"class.std::__cxx11::basic_string" = type { %"struct.std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider", i64, %union.anon }
%"struct.std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider" = type { i8* }
%union.anon = type { i64, [8 x i8] }
%"class.std::allocator" = type { i8 }

$_ZN6PersonC2Ev = comdat any

$_ZN6Person7setnomeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE = comdat any

$__clang_call_terminate = comdat any

$_ZN6Person7setpesoEi = comdat any

$_ZN6Person7getnomeEv = comdat any

$_ZN6Person7getpesoEv = comdat any

$_ZN6PersonD2Ev = comdat any

@_ZStL8__ioinit = internal global %"class.std::ios_base::Init" zeroinitializer, align 1
@.str = private unnamed_addr constant [6 x i8] c"Maria\00", align 1
@_ZSt4cout = external global %"class.std::basic_ostream", align 8
@.str.1 = private unnamed_addr constant [47 x i8] c"Outputting person data\0A======================\0A\00", align 1
@.str.2 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.3 = private unnamed_addr constant [17 x i8] c"No nome assigned\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_Classes.cpp, i8* null }]

define internal void @__cxx_global_var_init() #0 {
entry:
  call void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  %0 = call i32 @atexit(void ()* @__dtor__ZStL8__ioinit) #1
  ret void
}

declare void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"*) #0

declare void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"*) #0

define internal void @__dtor__ZStL8__ioinit() #0 {
entry:
  call void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  ret void
}

; Function Attrs: nounwind
declare i32 @atexit(void ()*) #1

; Function Attrs: uwtable
define i32 @main() #2 personality i8* bitcast (i32 (...)* @__gxx_personality_seh0 to i8*) {
entry:
  %retval = alloca i32, align 4
  %p1 = alloca %class.Person, align 8
  %agg.tmp = alloca %"class.std::__cxx11::basic_string", align 8
  %ref.tmp = alloca %"class.std::allocator", align 1
  %exn.slot = alloca i8*
  %ehselector.slot = alloca i32
  %ref.tmp8 = alloca %"class.std::__cxx11::basic_string", align 8
  store i32 0, i32* %retval
  call void @_ZN6PersonC2Ev(%class.Person* %p1)
  call void @_ZNSaIcEC1Ev(%"class.std::allocator"* %ref.tmp) #1
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1EPKcRKS3_(%"class.std::__cxx11::basic_string"* %agg.tmp, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str, i32 0, i32 0), %"class.std::allocator"* dereferenceable(1) %ref.tmp)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  invoke void @_ZN6Person7setnomeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Person* %p1, %"class.std::__cxx11::basic_string"* %agg.tmp)
          to label %invoke.cont.2 unwind label %lpad.1

invoke.cont.2:                                    ; preds = %invoke.cont
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %agg.tmp)
          to label %invoke.cont.3 unwind label %lpad

invoke.cont.3:                                    ; preds = %invoke.cont.2
  call void @_ZNSaIcED1Ev(%"class.std::allocator"* %ref.tmp) #1
  invoke void @_ZN6Person7setpesoEi(%class.Person* %p1, i32 50)
          to label %invoke.cont.6 unwind label %lpad.5

invoke.cont.6:                                    ; preds = %invoke.cont.3
  %call = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.1, i32 0, i32 0))
          to label %invoke.cont.7 unwind label %lpad.5

invoke.cont.7:                                    ; preds = %invoke.cont.6
  invoke void @_ZN6Person7getnomeEv(%"class.std::__cxx11::basic_string"* sret %ref.tmp8, %class.Person* %p1)
          to label %invoke.cont.9 unwind label %lpad.5

invoke.cont.9:                                    ; preds = %invoke.cont.7
  %call12 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, %"class.std::__cxx11::basic_string"* dereferenceable(32) %ref.tmp8)
          to label %invoke.cont.11 unwind label %lpad.10

invoke.cont.11:                                   ; preds = %invoke.cont.9
  %call14 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call12, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.2, i32 0, i32 0))
          to label %invoke.cont.13 unwind label %lpad.10

invoke.cont.13:                                   ; preds = %invoke.cont.11
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %ref.tmp8)
          to label %invoke.cont.15 unwind label %lpad.5

invoke.cont.15:                                   ; preds = %invoke.cont.13
  %call19 = invoke i32 @_ZN6Person7getpesoEv(%class.Person* %p1)
          to label %invoke.cont.18 unwind label %lpad.5

invoke.cont.18:                                   ; preds = %invoke.cont.15
  %call21 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* @_ZSt4cout, i32 %call19)
          to label %invoke.cont.20 unwind label %lpad.5

invoke.cont.20:                                   ; preds = %invoke.cont.18
  %call23 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call21, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.2, i32 0, i32 0))
          to label %invoke.cont.22 unwind label %lpad.5

invoke.cont.22:                                   ; preds = %invoke.cont.20
  store i32 0, i32* %retval
  call void @_ZN6PersonD2Ev(%class.Person* %p1)
  %0 = load i32, i32* %retval
  ret i32 %0

lpad:                                             ; preds = %invoke.cont.2, %entry
  %1 = landingpad { i8*, i32 }
          cleanup
  %2 = extractvalue { i8*, i32 } %1, 0
  store i8* %2, i8** %exn.slot
  %3 = extractvalue { i8*, i32 } %1, 1
  store i32 %3, i32* %ehselector.slot
  br label %ehcleanup

lpad.1:                                           ; preds = %invoke.cont
  %4 = landingpad { i8*, i32 }
          cleanup
  %5 = extractvalue { i8*, i32 } %4, 0
  store i8* %5, i8** %exn.slot
  %6 = extractvalue { i8*, i32 } %4, 1
  store i32 %6, i32* %ehselector.slot
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %agg.tmp)
          to label %invoke.cont.4 unwind label %terminate.lpad

invoke.cont.4:                                    ; preds = %lpad.1
  br label %ehcleanup

ehcleanup:                                        ; preds = %invoke.cont.4, %lpad
  call void @_ZNSaIcED1Ev(%"class.std::allocator"* %ref.tmp) #1
  br label %ehcleanup.24

lpad.5:                                           ; preds = %invoke.cont.20, %invoke.cont.18, %invoke.cont.15, %invoke.cont.13, %invoke.cont.7, %invoke.cont.6, %invoke.cont.3
  %7 = landingpad { i8*, i32 }
          cleanup
  %8 = extractvalue { i8*, i32 } %7, 0
  store i8* %8, i8** %exn.slot
  %9 = extractvalue { i8*, i32 } %7, 1
  store i32 %9, i32* %ehselector.slot
  br label %ehcleanup.24

lpad.10:                                          ; preds = %invoke.cont.11, %invoke.cont.9
  %10 = landingpad { i8*, i32 }
          cleanup
  %11 = extractvalue { i8*, i32 } %10, 0
  store i8* %11, i8** %exn.slot
  %12 = extractvalue { i8*, i32 } %10, 1
  store i32 %12, i32* %ehselector.slot
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %ref.tmp8)
          to label %invoke.cont.17 unwind label %terminate.lpad

invoke.cont.17:                                   ; preds = %lpad.10
  br label %ehcleanup.24

ehcleanup.24:                                     ; preds = %invoke.cont.17, %lpad.5, %ehcleanup
  invoke void @_ZN6PersonD2Ev(%class.Person* %p1)
          to label %invoke.cont.25 unwind label %terminate.lpad

invoke.cont.25:                                   ; preds = %ehcleanup.24
  br label %eh.resume

eh.resume:                                        ; preds = %invoke.cont.25
  %exn = load i8*, i8** %exn.slot
  %sel = load i32, i32* %ehselector.slot
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val.26 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val.26

terminate.lpad:                                   ; preds = %ehcleanup.24, %lpad.10, %lpad.1
  %13 = landingpad { i8*, i32 }
          catch i8* null
  %14 = extractvalue { i8*, i32 } %13, 0
  call void @__clang_call_terminate(i8* %14) #7
  unreachable
}

; Function Attrs: inlinehint uwtable
define linkonce_odr void @_ZN6PersonC2Ev(%class.Person* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.Person*, align 8
  store %class.Person* %this, %class.Person** %this.addr, align 8
  %this1 = load %class.Person*, %class.Person** %this.addr
  %nome = getelementptr inbounds %class.Person, %class.Person* %this1, i32 0, i32 0
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* %nome)
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN6Person7setnomeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.Person* %this, %"class.std::__cxx11::basic_string"* %s) #2 comdat align 2 {
entry:
  %this.addr = alloca %class.Person*, align 8
  store %class.Person* %this, %class.Person** %this.addr, align 8
  %this1 = load %class.Person*, %class.Person** %this.addr
  %call = call i64 @_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6lengthEv(%"class.std::__cxx11::basic_string"* %s)
  %cmp = icmp eq i64 %call, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %nome = getelementptr inbounds %class.Person, %class.Person* %this1, i32 0, i32 0
  %call2 = call dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEPKc(%"class.std::__cxx11::basic_string"* %nome, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.3, i32 0, i32 0))
  br label %if.end

if.else:                                          ; preds = %entry
  %nome3 = getelementptr inbounds %class.Person, %class.Person* %this1, i32 0, i32 0
  %call4 = call dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"* %nome3, %"class.std::__cxx11::basic_string"* dereferenceable(32) %s)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind
declare void @_ZNSaIcEC1Ev(%"class.std::allocator"*) #4

declare void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1EPKcRKS3_(%"class.std::__cxx11::basic_string"*, i8*, %"class.std::allocator"* dereferenceable(1)) #0

declare i32 @__gxx_personality_seh0(...)

declare void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"*) #0

; Function Attrs: noinline noreturn nounwind
define linkonce_odr hidden void @__clang_call_terminate(i8*) #5 comdat {
  %2 = call i8* @__cxa_begin_catch(i8* %0) #1
  call void @_ZSt9terminatev() #7
  unreachable
}

declare i8* @__cxa_begin_catch(i8*)

declare void @_ZSt9terminatev()

; Function Attrs: nounwind
declare void @_ZNSaIcED1Ev(%"class.std::allocator"*) #4

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN6Person7setpesoEi(%class.Person* %this, i32 %h) #6 comdat align 2 {
entry:
  %this.addr = alloca %class.Person*, align 8
  %h.addr = alloca i32, align 4
  store %class.Person* %this, %class.Person** %this.addr, align 8
  store i32 %h, i32* %h.addr, align 4
  %this1 = load %class.Person*, %class.Person** %this.addr
  %0 = load i32, i32* %h.addr, align 4
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %peso = getelementptr inbounds %class.Person, %class.Person* %this1, i32 0, i32 1
  store i32 0, i32* %peso, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %1 = load i32, i32* %h.addr, align 4
  %peso2 = getelementptr inbounds %class.Person, %class.Person* %this1, i32 0, i32 1
  store i32 %1, i32* %peso2, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272), i8*) #0

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* dereferenceable(272), %"class.std::__cxx11::basic_string"* dereferenceable(32)) #0

; Function Attrs: uwtable
define linkonce_odr void @_ZN6Person7getnomeEv(%"class.std::__cxx11::basic_string"* noalias sret %agg.result, %class.Person* %this) #2 comdat align 2 {
entry:
  %this.addr = alloca %class.Person*, align 8
  store %class.Person* %this, %class.Person** %this.addr, align 8
  %this1 = load %class.Person*, %class.Person** %this.addr
  %nome = getelementptr inbounds %class.Person, %class.Person* %this1, i32 0, i32 0
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* %agg.result, %"class.std::__cxx11::basic_string"* dereferenceable(32) %nome)
  ret void
}

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"*, i32) #0

; Function Attrs: nounwind uwtable
define linkonce_odr i32 @_ZN6Person7getpesoEv(%class.Person* %this) #6 comdat align 2 {
entry:
  %this.addr = alloca %class.Person*, align 8
  store %class.Person* %this, %class.Person** %this.addr, align 8
  %this1 = load %class.Person*, %class.Person** %this.addr
  %peso = getelementptr inbounds %class.Person, %class.Person* %this1, i32 0, i32 1
  %0 = load i32, i32* %peso, align 4
  ret i32 %0
}

; Function Attrs: inlinehint uwtable
define linkonce_odr void @_ZN6PersonD2Ev(%class.Person* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.Person*, align 8
  store %class.Person* %this, %class.Person** %this.addr, align 8
  %this1 = load %class.Person*, %class.Person** %this.addr
  %nome = getelementptr inbounds %class.Person, %class.Person* %this1, i32 0, i32 0
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %nome)
  ret void
}

declare void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"*) #0

declare i64 @_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6lengthEv(%"class.std::__cxx11::basic_string"*) #0

declare dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEPKc(%"class.std::__cxx11::basic_string"*, i8*) #0

declare dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_(%"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"* dereferenceable(32)) #0

declare void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"* dereferenceable(32)) #0

define internal void @_GLOBAL__sub_I_Classes.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noinline noreturn nounwind }
attributes #6 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
