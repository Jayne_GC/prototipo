; ModuleID = 'ITU.cpp'
target datalayout = "e-m:w-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-w64-windows-gnu"

%"class.std::ios_base::Init" = type { i8 }
%"class.std::basic_ostream" = type { i32 (...)**, %"class.std::basic_ios" }
%"class.std::basic_ios" = type { %"class.std::ios_base", %"class.std::basic_ostream"*, i8, i8, %"class.std::basic_streambuf"*, %"class.std::ctype"*, %"class.std::num_put"*, %"class.std::num_get"* }
%"class.std::ios_base" = type { i32 (...)**, i64, i64, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"class.std::locale" }
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"class.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type <{ i8*, i32, [4 x i8] }>
%"class.std::locale" = type { %"class.std::locale::_Impl"* }
%"class.std::locale::_Impl" = type { i32, %"class.std::locale::facet"**, i64, %"class.std::locale::facet"**, i8** }
%"class.std::locale::facet" = type <{ i32 (...)**, i32, [4 x i8] }>
%"class.std::basic_streambuf" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"class.std::locale" }
%"class.std::ctype" = type <{ %"class.std::locale::facet.base", [4 x i8], i32*, i8, [7 x i8], i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8, [6 x i8] }>
%"class.std::locale::facet.base" = type <{ i32 (...)**, i32 }>
%"class.std::num_put" = type { %"class.std::locale::facet.base", [4 x i8] }
%"class.std::num_get" = type { %"class.std::locale::facet.base", [4 x i8] }
%class.Y = type { %class.X.base, i32 }
%class.X.base = type <{ %class.W, i32 }>
%class.W = type { i32 (...)**, i32, i32 }
%class.X = type <{ %class.W, i32, [4 x i8] }>

$_ZN1YC2Ev = comdat any

$_ZN1Y1pEv = comdat any

$_ZN1Y1tEv = comdat any

$_ZN1XC2Ev = comdat any

$_ZN1W1mEv = comdat any

$_ZN1W1nEv = comdat any

$_ZN1W1rEv = comdat any

$_ZN1WC2Ev = comdat any

$_ZN1Y1sEv = comdat any

$_ZN1X1oEv = comdat any

$_ZTV1Y = comdat any

$_ZTS1Y = comdat any

$_ZTS1X = comdat any

$_ZTS1W = comdat any

$_ZTI1W = comdat any

$_ZTI1X = comdat any

$_ZTI1Y = comdat any

$_ZTV1X = comdat any

$_ZTV1W = comdat any

@_ZStL8__ioinit = internal global %"class.std::ios_base::Init" zeroinitializer, align 1
@_ZTV1Y = linkonce_odr unnamed_addr constant [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI1Y to i8*), i8* bitcast (void (%class.W*)* @_ZN1W1mEv to i8*), i8* bitcast (void (%class.W*)* @_ZN1W1nEv to i8*), i8* bitcast (void (%class.W*)* @_ZN1W1rEv to i8*)], comdat, align 8
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS1Y = linkonce_odr constant [3 x i8] c"1Y\00", comdat
@_ZTS1X = linkonce_odr constant [3 x i8] c"1X\00", comdat
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS1W = linkonce_odr constant [3 x i8] c"1W\00", comdat
@_ZTI1W = linkonce_odr constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([3 x i8], [3 x i8]* @_ZTS1W, i32 0, i32 0) }, comdat
@_ZTI1X = linkonce_odr constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([3 x i8], [3 x i8]* @_ZTS1X, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI1W to i8*) }, comdat
@_ZTI1Y = linkonce_odr constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([3 x i8], [3 x i8]* @_ZTS1Y, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI1X to i8*) }, comdat
@_ZTV1X = linkonce_odr unnamed_addr constant [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI1X to i8*), i8* bitcast (void (%class.W*)* @_ZN1W1mEv to i8*), i8* bitcast (void (%class.W*)* @_ZN1W1nEv to i8*), i8* bitcast (void (%class.W*)* @_ZN1W1rEv to i8*)], comdat, align 8
@_ZTV1W = linkonce_odr unnamed_addr constant [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI1W to i8*), i8* bitcast (void (%class.W*)* @_ZN1W1mEv to i8*), i8* bitcast (void (%class.W*)* @_ZN1W1nEv to i8*), i8* bitcast (void (%class.W*)* @_ZN1W1rEv to i8*)], comdat, align 8
@_ZSt4cout = external global %"class.std::basic_ostream", align 8
@.str = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_ITU.cpp, i8* null }]

define internal void @__cxx_global_var_init() #0 {
entry:
  call void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  %0 = call i32 @atexit(void ()* @__dtor__ZStL8__ioinit) #2
  ret void
}

declare void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"*) #0

; Function Attrs: nounwind
declare void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"*) #1

define internal void @__dtor__ZStL8__ioinit() #0 {
entry:
  call void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  ret void
}

; Function Attrs: nounwind
declare i32 @atexit(void ()*) #2

; Function Attrs: uwtable
define i32 @main() #3 {
entry:
  %retval = alloca i32, align 4
  %obj_y = alloca %class.Y, align 8
  store i32 0, i32* %retval
  call void @_ZN1YC2Ev(%class.Y* %obj_y) #2
  call void @_ZN1Y1pEv(%class.Y* %obj_y)
  call void @_ZN1Y1tEv(%class.Y* %obj_y)
  ret i32 0
}

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN1YC2Ev(%class.Y* %this) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.Y*, align 8
  store %class.Y* %this, %class.Y** %this.addr, align 8
  %this1 = load %class.Y*, %class.Y** %this.addr
  %0 = bitcast %class.Y* %this1 to %class.X*
  call void @_ZN1XC2Ev(%class.X* %0) #2
  %1 = bitcast %class.Y* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV1Y, i64 0, i64 2) to i32 (...)**), i32 (...)*** %1
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN1Y1pEv(%class.Y* %this) #3 comdat align 2 {
entry:
  %this.addr = alloca %class.Y*, align 8
  store %class.Y* %this, %class.Y** %this.addr, align 8
  %this1 = load %class.Y*, %class.Y** %this.addr
  %0 = bitcast %class.Y* %this1 to %class.W*
  %1 = bitcast %class.W* %0 to void (%class.W*)***
  %vtable = load void (%class.W*)**, void (%class.W*)*** %1
  %vfn = getelementptr inbounds void (%class.W*)*, void (%class.W*)** %vtable, i64 0
  %2 = load void (%class.W*)*, void (%class.W*)** %vfn
  call void %2(%class.W* %0)
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN1Y1tEv(%class.Y* %this) #3 comdat align 2 {
entry:
  %this.addr = alloca %class.Y*, align 8
  store %class.Y* %this, %class.Y** %this.addr, align 8
  %this1 = load %class.Y*, %class.Y** %this.addr
  call void @_ZN1Y1sEv(%class.Y* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN1XC2Ev(%class.X* %this) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.X*, align 8
  store %class.X* %this, %class.X** %this.addr, align 8
  %this1 = load %class.X*, %class.X** %this.addr
  %0 = bitcast %class.X* %this1 to %class.W*
  call void @_ZN1WC2Ev(%class.W* %0) #2
  %1 = bitcast %class.X* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV1X, i64 0, i64 2) to i32 (...)**), i32 (...)*** %1
  ret void
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN1W1mEv(%class.W* %this) unnamed_addr #5 comdat align 2 {
entry:
  %this.addr = alloca %class.W*, align 8
  store %class.W* %this, %class.W** %this.addr, align 8
  %this1 = load %class.W*, %class.W** %this.addr
  %v = getelementptr inbounds %class.W, %class.W* %this1, i32 0, i32 2
  store i32 1, i32* %v, align 4
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN1W1nEv(%class.W* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.W*, align 8
  store %class.W* %this, %class.W** %this.addr, align 8
  %this1 = load %class.W*, %class.W** %this.addr
  %v = getelementptr inbounds %class.W, %class.W* %this1, i32 0, i32 2
  %0 = load i32, i32* %v, align 4
  %u = getelementptr inbounds %class.W, %class.W* %this1, i32 0, i32 1
  %1 = load i32, i32* %u, align 4
  %add = add nsw i32 %0, %1
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* @_ZSt4cout, i32 %add)
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN1W1rEv(%class.W* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.W*, align 8
  store %class.W* %this, %class.W** %this.addr, align 8
  %this1 = load %class.W*, %class.W** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0))
  ret void
}

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN1WC2Ev(%class.W* %this) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.W*, align 8
  store %class.W* %this, %class.W** %this.addr, align 8
  %this1 = load %class.W*, %class.W** %this.addr
  %0 = bitcast %class.W* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV1W, i64 0, i64 2) to i32 (...)**), i32 (...)*** %0
  ret void
}

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"*, i32) #0

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272), i8*) #0

; Function Attrs: uwtable
define linkonce_odr void @_ZN1Y1sEv(%class.Y* %this) #3 comdat align 2 {
entry:
  %this.addr = alloca %class.Y*, align 8
  store %class.Y* %this, %class.Y** %this.addr, align 8
  %this1 = load %class.Y*, %class.Y** %this.addr
  %0 = bitcast %class.Y* %this1 to %class.X*
  call void @_ZN1X1oEv(%class.X* %0)
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN1X1oEv(%class.X* %this) #3 comdat align 2 {
entry:
  %this.addr = alloca %class.X*, align 8
  store %class.X* %this, %class.X** %this.addr, align 8
  %this1 = load %class.X*, %class.X** %this.addr
  %0 = bitcast %class.X* %this1 to %class.W*
  %1 = bitcast %class.W* %0 to void (%class.W*)***
  %vtable = load void (%class.W*)**, void (%class.W*)*** %1
  %vfn = getelementptr inbounds void (%class.W*)*, void (%class.W*)** %vtable, i64 1
  %2 = load void (%class.W*)*, void (%class.W*)** %vfn
  call void %2(%class.W* %0)
  ret void
}

define internal void @_GLOBAL__sub_I_ITU.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }
attributes #3 = { uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
