; ModuleID = 'YoYo.cpp'
target datalayout = "e-m:w-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-w64-windows-gnu"

%"class.std::ios_base::Init" = type { i8 }
%"class.std::basic_ostream" = type { i32 (...)**, %"class.std::basic_ios" }
%"class.std::basic_ios" = type { %"class.std::ios_base", %"class.std::basic_ostream"*, i8, i8, %"class.std::basic_streambuf"*, %"class.std::ctype"*, %"class.std::num_put"*, %"class.std::num_get"* }
%"class.std::ios_base" = type { i32 (...)**, i64, i64, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"class.std::locale" }
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"class.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type <{ i8*, i32, [4 x i8] }>
%"class.std::locale" = type { %"class.std::locale::_Impl"* }
%"class.std::locale::_Impl" = type { i32, %"class.std::locale::facet"**, i64, %"class.std::locale::facet"**, i8** }
%"class.std::locale::facet" = type <{ i32 (...)**, i32, [4 x i8] }>
%"class.std::basic_streambuf" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"class.std::locale" }
%"class.std::ctype" = type <{ %"class.std::locale::facet.base", [4 x i8], i32*, i8, [7 x i8], i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8, [6 x i8] }>
%"class.std::locale::facet.base" = type <{ i32 (...)**, i32 }>
%"class.std::num_put" = type { %"class.std::locale::facet.base", [4 x i8] }
%"class.std::num_get" = type { %"class.std::locale::facet.base", [4 x i8] }
%class.A = type { i32 (...)** }
%class.B = type { %class.A }
%class.C = type { %class.B }

$_ZN1AC2Ev = comdat any

$_ZN1BC2Ev = comdat any

$_ZN1CC2Ev = comdat any

$_ZN1A1dEv = comdat any

$_ZN1A1hEv = comdat any

$_ZN1A1iEv = comdat any

$_ZN1A1jEv = comdat any

$_ZN1A1lEv = comdat any

$_ZN1B1hEv = comdat any

$_ZN1B1iEv = comdat any

$_ZN1C1iEv = comdat any

$_ZN1C1jEv = comdat any

$_ZN1C1lEv = comdat any

$_ZN1B1kEv = comdat any

$_ZN1A1gEv = comdat any

$_ZTV1A = comdat any

$_ZTS1A = comdat any

$_ZTI1A = comdat any

$_ZTV1B = comdat any

$_ZTS1B = comdat any

$_ZTI1B = comdat any

$_ZTV1C = comdat any

$_ZTS1C = comdat any

$_ZTI1C = comdat any

@_ZStL8__ioinit = internal global %"class.std::ios_base::Init" zeroinitializer, align 1
@_ZSt4cout = external global %"class.std::basic_ostream", align 8
@.str = private unnamed_addr constant [19 x i8] c"Chamando com A...\0A\00", align 1
@.str.1 = private unnamed_addr constant [19 x i8] c"Chamando com B...\0A\00", align 1
@.str.2 = private unnamed_addr constant [19 x i8] c"Chamando com C...\0A\00", align 1
@_ZTV1A = linkonce_odr unnamed_addr constant [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI1A to i8*), i8* bitcast (void (%class.A*)* @_ZN1A1hEv to i8*), i8* bitcast (void (%class.A*)* @_ZN1A1iEv to i8*), i8* bitcast (i32 (%class.A*)* @_ZN1A1jEv to i8*), i8* bitcast (i32 (%class.A*)* @_ZN1A1lEv to i8*)], comdat, align 8
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS1A = linkonce_odr constant [3 x i8] c"1A\00", comdat
@_ZTI1A = linkonce_odr constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([3 x i8], [3 x i8]* @_ZTS1A, i32 0, i32 0) }, comdat
@.str.3 = private unnamed_addr constant [5 x i8] c"A.h\0A\00", align 1
@.str.4 = private unnamed_addr constant [5 x i8] c"A.i\0A\00", align 1
@.str.5 = private unnamed_addr constant [5 x i8] c"A.j\0A\00", align 1
@.str.6 = private unnamed_addr constant [5 x i8] c"A.l\0A\00", align 1
@_ZTV1B = linkonce_odr unnamed_addr constant [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI1B to i8*), i8* bitcast (void (%class.B*)* @_ZN1B1hEv to i8*), i8* bitcast (void (%class.B*)* @_ZN1B1iEv to i8*), i8* bitcast (i32 (%class.A*)* @_ZN1A1jEv to i8*), i8* bitcast (i32 (%class.A*)* @_ZN1A1lEv to i8*)], comdat, align 8
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS1B = linkonce_odr constant [3 x i8] c"1B\00", comdat
@_ZTI1B = linkonce_odr constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([3 x i8], [3 x i8]* @_ZTS1B, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI1A to i8*) }, comdat
@.str.7 = private unnamed_addr constant [5 x i8] c"B.h\0A\00", align 1
@.str.8 = private unnamed_addr constant [5 x i8] c"B.i\0A\00", align 1
@_ZTV1C = linkonce_odr unnamed_addr constant [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI1C to i8*), i8* bitcast (void (%class.B*)* @_ZN1B1hEv to i8*), i8* bitcast (void (%class.C*)* @_ZN1C1iEv to i8*), i8* bitcast (i32 (%class.C*)* @_ZN1C1jEv to i8*), i8* bitcast (i32 (%class.C*)* @_ZN1C1lEv to i8*)], comdat, align 8
@_ZTS1C = linkonce_odr constant [3 x i8] c"1C\00", comdat
@_ZTI1C = linkonce_odr constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([3 x i8], [3 x i8]* @_ZTS1C, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI1B to i8*) }, comdat
@.str.9 = private unnamed_addr constant [5 x i8] c"C.i\0A\00", align 1
@.str.10 = private unnamed_addr constant [5 x i8] c"C.j\0A\00", align 1
@.str.11 = private unnamed_addr constant [5 x i8] c"B.k\0A\00", align 1
@.str.12 = private unnamed_addr constant [5 x i8] c"C.l\0A\00", align 1
@.str.13 = private unnamed_addr constant [5 x i8] c"A.d\0A\00", align 1
@.str.14 = private unnamed_addr constant [5 x i8] c"A.g\0A\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_YoYo.cpp, i8* null }]

define internal void @__cxx_global_var_init() #0 {
entry:
  call void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  %0 = call i32 @atexit(void ()* @__dtor__ZStL8__ioinit) #2
  ret void
}

declare void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"*) #0

; Function Attrs: nounwind
declare void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"*) #1

define internal void @__dtor__ZStL8__ioinit() #0 {
entry:
  call void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  ret void
}

; Function Attrs: nounwind
declare i32 @atexit(void ()*) #2

; Function Attrs: uwtable
define i32 @main() #3 {
entry:
  %retval = alloca i32, align 4
  %obj_a = alloca %class.A, align 8
  %obj_b = alloca %class.B, align 8
  %obj_c = alloca %class.C, align 8
  store i32 0, i32* %retval
  call void @_ZN1AC2Ev(%class.A* %obj_a) #2
  call void @_ZN1BC2Ev(%class.B* %obj_b) #2
  call void @_ZN1CC2Ev(%class.C* %obj_c) #2
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i32 0, i32 0))
  call void @_ZN1A1dEv(%class.A* %obj_a)
  %call1 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.1, i32 0, i32 0))
  %0 = bitcast %class.B* %obj_b to %class.A*
  call void @_ZN1A1dEv(%class.A* %0)
  %call2 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.2, i32 0, i32 0))
  %1 = bitcast %class.C* %obj_c to %class.A*
  call void @_ZN1A1dEv(%class.A* %1)
  ret i32 0
}

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN1AC2Ev(%class.A* %this) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.A*, align 8
  store %class.A* %this, %class.A** %this.addr, align 8
  %this1 = load %class.A*, %class.A** %this.addr
  %0 = bitcast %class.A* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([6 x i8*], [6 x i8*]* @_ZTV1A, i64 0, i64 2) to i32 (...)**), i32 (...)*** %0
  ret void
}

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN1BC2Ev(%class.B* %this) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.B*, align 8
  store %class.B* %this, %class.B** %this.addr, align 8
  %this1 = load %class.B*, %class.B** %this.addr
  %0 = bitcast %class.B* %this1 to %class.A*
  call void @_ZN1AC2Ev(%class.A* %0) #2
  %1 = bitcast %class.B* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([6 x i8*], [6 x i8*]* @_ZTV1B, i64 0, i64 2) to i32 (...)**), i32 (...)*** %1
  ret void
}

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN1CC2Ev(%class.C* %this) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.C*, align 8
  store %class.C* %this, %class.C** %this.addr, align 8
  %this1 = load %class.C*, %class.C** %this.addr
  %0 = bitcast %class.C* %this1 to %class.B*
  call void @_ZN1BC2Ev(%class.B* %0) #2
  %1 = bitcast %class.C* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([6 x i8*], [6 x i8*]* @_ZTV1C, i64 0, i64 2) to i32 (...)**), i32 (...)*** %1
  ret void
}

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272), i8*) #0

; Function Attrs: uwtable
define linkonce_odr void @_ZN1A1dEv(%class.A* %this) #3 comdat align 2 {
entry:
  %this.addr = alloca %class.A*, align 8
  store %class.A* %this, %class.A** %this.addr, align 8
  %this1 = load %class.A*, %class.A** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.13, i32 0, i32 0))
  call void @_ZN1A1gEv(%class.A* %this1)
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN1A1hEv(%class.A* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.A*, align 8
  store %class.A* %this, %class.A** %this.addr, align 8
  %this1 = load %class.A*, %class.A** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.3, i32 0, i32 0))
  %0 = bitcast %class.A* %this1 to void (%class.A*)***
  %vtable = load void (%class.A*)**, void (%class.A*)*** %0
  %vfn = getelementptr inbounds void (%class.A*)*, void (%class.A*)** %vtable, i64 1
  %1 = load void (%class.A*)*, void (%class.A*)** %vfn
  call void %1(%class.A* %this1)
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN1A1iEv(%class.A* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.A*, align 8
  store %class.A* %this, %class.A** %this.addr, align 8
  %this1 = load %class.A*, %class.A** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.4, i32 0, i32 0))
  %0 = bitcast %class.A* %this1 to i32 (%class.A*)***
  %vtable = load i32 (%class.A*)**, i32 (%class.A*)*** %0
  %vfn = getelementptr inbounds i32 (%class.A*)*, i32 (%class.A*)** %vtable, i64 2
  %1 = load i32 (%class.A*)*, i32 (%class.A*)** %vfn
  %call2 = call i32 %1(%class.A* %this1)
  ret void
}

; Function Attrs: uwtable
define linkonce_odr i32 @_ZN1A1jEv(%class.A* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.A*, align 8
  store %class.A* %this, %class.A** %this.addr, align 8
  %this1 = load %class.A*, %class.A** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.5, i32 0, i32 0))
  ret i32 0
}

; Function Attrs: uwtable
define linkonce_odr i32 @_ZN1A1lEv(%class.A* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.A*, align 8
  store %class.A* %this, %class.A** %this.addr, align 8
  %this1 = load %class.A*, %class.A** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.6, i32 0, i32 0))
  ret i32 0
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN1B1hEv(%class.B* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.B*, align 8
  store %class.B* %this, %class.B** %this.addr, align 8
  %this1 = load %class.B*, %class.B** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.7, i32 0, i32 0))
  %0 = bitcast %class.B* %this1 to void (%class.B*)***
  %vtable = load void (%class.B*)**, void (%class.B*)*** %0
  %vfn = getelementptr inbounds void (%class.B*)*, void (%class.B*)** %vtable, i64 1
  %1 = load void (%class.B*)*, void (%class.B*)** %vfn
  call void %1(%class.B* %this1)
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN1B1iEv(%class.B* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.B*, align 8
  store %class.B* %this, %class.B** %this.addr, align 8
  %this1 = load %class.B*, %class.B** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.8, i32 0, i32 0))
  %0 = bitcast %class.B* %this1 to %class.A*
  %1 = bitcast %class.A* %0 to i32 (%class.A*)***
  %vtable = load i32 (%class.A*)**, i32 (%class.A*)*** %1
  %vfn = getelementptr inbounds i32 (%class.A*)*, i32 (%class.A*)** %vtable, i64 2
  %2 = load i32 (%class.A*)*, i32 (%class.A*)** %vfn
  %call2 = call i32 %2(%class.A* %0)
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN1C1iEv(%class.C* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.C*, align 8
  store %class.C* %this, %class.C** %this.addr, align 8
  %this1 = load %class.C*, %class.C** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.9, i32 0, i32 0))
  %0 = bitcast %class.C* %this1 to i32 (%class.C*)***
  %vtable = load i32 (%class.C*)**, i32 (%class.C*)*** %0
  %vfn = getelementptr inbounds i32 (%class.C*)*, i32 (%class.C*)** %vtable, i64 2
  %1 = load i32 (%class.C*)*, i32 (%class.C*)** %vfn
  %call2 = call i32 %1(%class.C* %this1)
  ret void
}

; Function Attrs: uwtable
define linkonce_odr i32 @_ZN1C1jEv(%class.C* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.C*, align 8
  store %class.C* %this, %class.C** %this.addr, align 8
  %this1 = load %class.C*, %class.C** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.10, i32 0, i32 0))
  %0 = bitcast %class.C* %this1 to %class.B*
  call void @_ZN1B1kEv(%class.B* %0)
  ret i32 0
}

; Function Attrs: uwtable
define linkonce_odr i32 @_ZN1C1lEv(%class.C* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.C*, align 8
  store %class.C* %this, %class.C** %this.addr, align 8
  %this1 = load %class.C*, %class.C** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.12, i32 0, i32 0))
  ret i32 0
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN1B1kEv(%class.B* %this) #3 comdat align 2 {
entry:
  %this.addr = alloca %class.B*, align 8
  store %class.B* %this, %class.B** %this.addr, align 8
  %this1 = load %class.B*, %class.B** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.11, i32 0, i32 0))
  %0 = bitcast %class.B* %this1 to %class.A*
  %1 = bitcast %class.A* %0 to i32 (%class.A*)***
  %vtable = load i32 (%class.A*)**, i32 (%class.A*)*** %1
  %vfn = getelementptr inbounds i32 (%class.A*)*, i32 (%class.A*)** %vtable, i64 3
  %2 = load i32 (%class.A*)*, i32 (%class.A*)** %vfn
  %call2 = call i32 %2(%class.A* %0)
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN1A1gEv(%class.A* %this) #3 comdat align 2 {
entry:
  %this.addr = alloca %class.A*, align 8
  store %class.A* %this, %class.A** %this.addr, align 8
  %this1 = load %class.A*, %class.A** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.14, i32 0, i32 0))
  %0 = bitcast %class.A* %this1 to void (%class.A*)***
  %vtable = load void (%class.A*)**, void (%class.A*)*** %0
  %vfn = getelementptr inbounds void (%class.A*)*, void (%class.A*)** %vtable, i64 0
  %1 = load void (%class.A*)*, void (%class.A*)** %vfn
  call void %1(%class.A* %this1)
  ret void
}

define internal void @_GLOBAL__sub_I_YoYo.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }
attributes #3 = { uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
