; ModuleID = 'Instrumentos.cpp'
target datalayout = "e-m:w-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-w64-windows-gnu"

%"class.std::ios_base::Init" = type { i8 }
%"class.std::basic_ostream" = type { i32 (...)**, %"class.std::basic_ios" }
%"class.std::basic_ios" = type { %"class.std::ios_base", %"class.std::basic_ostream"*, i8, i8, %"class.std::basic_streambuf"*, %"class.std::ctype"*, %"class.std::num_put"*, %"class.std::num_get"* }
%"class.std::ios_base" = type { i32 (...)**, i64, i64, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"class.std::locale" }
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"class.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type <{ i8*, i32, [4 x i8] }>
%"class.std::locale" = type { %"class.std::locale::_Impl"* }
%"class.std::locale::_Impl" = type { i32, %"class.std::locale::facet"**, i64, %"class.std::locale::facet"**, i8** }
%"class.std::locale::facet" = type <{ i32 (...)**, i32, [4 x i8] }>
%"class.std::basic_streambuf" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"class.std::locale" }
%"class.std::ctype" = type <{ %"class.std::locale::facet.base", [4 x i8], i32*, i8, [7 x i8], i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8, [6 x i8] }>
%"class.std::locale::facet.base" = type <{ i32 (...)**, i32 }>
%"class.std::num_put" = type { %"class.std::locale::facet.base", [4 x i8] }
%"class.std::num_get" = type { %"class.std::locale::facet.base", [4 x i8] }
%class.Sopro = type { %class.Instrumento, i32, %"class.std::__cxx11::basic_string" }
%class.Instrumento = type { i32 (...)**, %"class.std::__cxx11::basic_string" }
%"class.std::__cxx11::basic_string" = type { %"struct.std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider", i64, %union.anon }
%"struct.std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider" = type { i8* }
%union.anon = type { i64, [8 x i8] }
%class.Percussao = type { %class.Instrumento, i32, %"class.std::__cxx11::basic_string" }
%class.ComCordas = type { %class.Instrumento, i32, %"class.std::__cxx11::basic_string" }
%class.Metais = type { %class.Sopro, %"class.std::__cxx11::basic_string" }
%class.Madeiras = type { %class.Sopro, %"class.std::__cxx11::basic_string" }
%"class.std::allocator" = type { i8 }

$_ZN5SoproC2Ev = comdat any

$_ZN9PercussaoC2Ev = comdat any

$_ZN9ComCordasC2Ev = comdat any

$_ZN6MetaisC2Ev = comdat any

$_ZN8MadeirasC2Ev = comdat any

$_ZNK5Sopro5tocarE4nota = comdat any

$_ZNK9Percussao5tocarE4nota = comdat any

$_ZNK9ComCordas5tocarE4nota = comdat any

$_ZNK6Metais5tocarE4nota = comdat any

$_ZNK8Madeiras5tocarE4nota = comdat any

$_ZN6Metais7ajustarEi = comdat any

$_ZN8MadeirasD2Ev = comdat any

$_ZN6MetaisD2Ev = comdat any

$_ZN9ComCordasD2Ev = comdat any

$_ZN9PercussaoD2Ev = comdat any

$_ZN5SoproD2Ev = comdat any

$_ZN11InstrumentoC2Ev = comdat any

$_ZN11InstrumentoD2Ev = comdat any

$_ZNK5Sopro4OqueEv = comdat any

$_ZN5Sopro7ajustarEi = comdat any

$_ZNK9Percussao4OqueEv = comdat any

$_ZN9Percussao7ajustarEi = comdat any

$_ZNK9ComCordas4OqueEv = comdat any

$_ZN9ComCordas7ajustarEi = comdat any

$_ZNK6Metais4OqueEv = comdat any

$_ZNK8Madeiras4OqueEv = comdat any

$_ZN8Madeiras7ajustarEi = comdat any

$_ZTV5Sopro = comdat any

$_ZTS5Sopro = comdat any

$_ZTS11Instrumento = comdat any

$_ZTI11Instrumento = comdat any

$_ZTI5Sopro = comdat any

$_ZTV11Instrumento = comdat any

$_ZTV9Percussao = comdat any

$_ZTS9Percussao = comdat any

$_ZTI9Percussao = comdat any

$_ZTV9ComCordas = comdat any

$_ZTS9ComCordas = comdat any

$_ZTI9ComCordas = comdat any

$_ZTV6Metais = comdat any

$_ZTS6Metais = comdat any

$_ZTI6Metais = comdat any

$_ZTV8Madeiras = comdat any

$_ZTS8Madeiras = comdat any

$_ZTI8Madeiras = comdat any

@_ZStL8__ioinit = internal global %"class.std::ios_base::Init" zeroinitializer, align 1
@_ZTV5Sopro = linkonce_odr unnamed_addr constant [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI5Sopro to i8*), i8* bitcast (void (%class.Sopro*, i32)* @_ZNK5Sopro5tocarE4nota to i8*), i8* bitcast (void (%"class.std::__cxx11::basic_string"*, %class.Sopro*)* @_ZNK5Sopro4OqueEv to i8*), i8* bitcast (void (%class.Sopro*, i32)* @_ZN5Sopro7ajustarEi to i8*)], comdat, align 8
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS5Sopro = linkonce_odr constant [7 x i8] c"5Sopro\00", comdat
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS11Instrumento = linkonce_odr constant [14 x i8] c"11Instrumento\00", comdat
@_ZTI11Instrumento = linkonce_odr constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([14 x i8], [14 x i8]* @_ZTS11Instrumento, i32 0, i32 0) }, comdat
@_ZTI5Sopro = linkonce_odr constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @_ZTS5Sopro, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI11Instrumento to i8*) }, comdat
@_ZTV11Instrumento = linkonce_odr unnamed_addr constant [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI11Instrumento to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)], comdat, align 8
@.str = private unnamed_addr constant [6 x i8] c"Sopro\00", align 1
@_ZTV9Percussao = linkonce_odr unnamed_addr constant [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI9Percussao to i8*), i8* bitcast (void (%class.Percussao*, i32)* @_ZNK9Percussao5tocarE4nota to i8*), i8* bitcast (void (%"class.std::__cxx11::basic_string"*, %class.Percussao*)* @_ZNK9Percussao4OqueEv to i8*), i8* bitcast (void (%class.Percussao*, i32)* @_ZN9Percussao7ajustarEi to i8*)], comdat, align 8
@_ZTS9Percussao = linkonce_odr constant [11 x i8] c"9Percussao\00", comdat
@_ZTI9Percussao = linkonce_odr constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @_ZTS9Percussao, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI11Instrumento to i8*) }, comdat
@.str.1 = private unnamed_addr constant [10 x i8] c"Percussao\00", align 1
@_ZTV9ComCordas = linkonce_odr unnamed_addr constant [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI9ComCordas to i8*), i8* bitcast (void (%class.ComCordas*, i32)* @_ZNK9ComCordas5tocarE4nota to i8*), i8* bitcast (void (%"class.std::__cxx11::basic_string"*, %class.ComCordas*)* @_ZNK9ComCordas4OqueEv to i8*), i8* bitcast (void (%class.ComCordas*, i32)* @_ZN9ComCordas7ajustarEi to i8*)], comdat, align 8
@_ZTS9ComCordas = linkonce_odr constant [11 x i8] c"9ComCordas\00", comdat
@_ZTI9ComCordas = linkonce_odr constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @_ZTS9ComCordas, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI11Instrumento to i8*) }, comdat
@.str.2 = private unnamed_addr constant [10 x i8] c"ComCordas\00", align 1
@_ZTV6Metais = linkonce_odr unnamed_addr constant [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI6Metais to i8*), i8* bitcast (void (%class.Metais*, i32)* @_ZNK6Metais5tocarE4nota to i8*), i8* bitcast (void (%"class.std::__cxx11::basic_string"*, %class.Metais*)* @_ZNK6Metais4OqueEv to i8*), i8* bitcast (void (%class.Metais*, i32)* @_ZN6Metais7ajustarEi to i8*)], comdat, align 8
@_ZTS6Metais = linkonce_odr constant [8 x i8] c"6Metais\00", comdat
@_ZTI6Metais = linkonce_odr constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @_ZTS6Metais, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI5Sopro to i8*) }, comdat
@.str.3 = private unnamed_addr constant [7 x i8] c"Metais\00", align 1
@_ZTV8Madeiras = linkonce_odr unnamed_addr constant [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI8Madeiras to i8*), i8* bitcast (void (%class.Madeiras*, i32)* @_ZNK8Madeiras5tocarE4nota to i8*), i8* bitcast (void (%"class.std::__cxx11::basic_string"*, %class.Madeiras*)* @_ZNK8Madeiras4OqueEv to i8*), i8* bitcast (void (%class.Madeiras*, i32)* @_ZN8Madeiras7ajustarEi to i8*)], comdat, align 8
@_ZTS8Madeiras = linkonce_odr constant [10 x i8] c"8Madeiras\00", comdat
@_ZTI8Madeiras = linkonce_odr constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @_ZTS8Madeiras, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI5Sopro to i8*) }, comdat
@.str.4 = private unnamed_addr constant [9 x i8] c"Madeiras\00", align 1
@_ZSt4cout = external global %"class.std::basic_ostream", align 8
@.str.5 = private unnamed_addr constant [18 x i8] c" Ajustado para = \00", align 1
@.str.6 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.7 = private unnamed_addr constant [20 x i8] c"::tocar - Ajuste = \00", align 1
@.str.8 = private unnamed_addr constant [3 x i8] c"::\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_Instrumentos.cpp, i8* null }]

define internal void @__cxx_global_var_init() #0 {
entry:
  call void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  %0 = call i32 @atexit(void ()* @__dtor__ZStL8__ioinit) #2
  ret void
}

declare void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"*) #0

; Function Attrs: nounwind
declare void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"*) #1

define internal void @__dtor__ZStL8__ioinit() #0 {
entry:
  call void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  ret void
}

; Function Attrs: nounwind
declare i32 @atexit(void ()*) #2

; Function Attrs: uwtable
define i32 @main() #3 personality i8* bitcast (i32 (...)* @__gxx_personality_seh0 to i8*) {
entry:
  %flauta = alloca %class.Sopro, align 8
  %tambor = alloca %class.Percussao, align 8
  %exn.slot = alloca i8*
  %ehselector.slot = alloca i32
  %violino = alloca %class.ComCordas, align 8
  %Fliscorne = alloca %class.Metais, align 8
  %FlautaDoce = alloca %class.Madeiras, align 8
  call void @_ZN5SoproC2Ev(%class.Sopro* %flauta)
  invoke void @_ZN9PercussaoC2Ev(%class.Percussao* %tambor)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  invoke void @_ZN9ComCordasC2Ev(%class.ComCordas* %violino)
          to label %invoke.cont.2 unwind label %lpad.1

invoke.cont.2:                                    ; preds = %invoke.cont
  invoke void @_ZN6MetaisC2Ev(%class.Metais* %Fliscorne)
          to label %invoke.cont.4 unwind label %lpad.3

invoke.cont.4:                                    ; preds = %invoke.cont.2
  invoke void @_ZN8MadeirasC2Ev(%class.Madeiras* %FlautaDoce)
          to label %invoke.cont.6 unwind label %lpad.5

invoke.cont.6:                                    ; preds = %invoke.cont.4
  invoke void @_ZNK5Sopro5tocarE4nota(%class.Sopro* %flauta, i32 0)
          to label %invoke.cont.8 unwind label %lpad.7

invoke.cont.8:                                    ; preds = %invoke.cont.6
  invoke void @_ZNK9Percussao5tocarE4nota(%class.Percussao* %tambor, i32 0)
          to label %invoke.cont.9 unwind label %lpad.7

invoke.cont.9:                                    ; preds = %invoke.cont.8
  invoke void @_ZNK9ComCordas5tocarE4nota(%class.ComCordas* %violino, i32 0)
          to label %invoke.cont.10 unwind label %lpad.7

invoke.cont.10:                                   ; preds = %invoke.cont.9
  invoke void @_ZNK6Metais5tocarE4nota(%class.Metais* %Fliscorne, i32 0)
          to label %invoke.cont.11 unwind label %lpad.7

invoke.cont.11:                                   ; preds = %invoke.cont.10
  invoke void @_ZNK8Madeiras5tocarE4nota(%class.Madeiras* %FlautaDoce, i32 0)
          to label %invoke.cont.12 unwind label %lpad.7

invoke.cont.12:                                   ; preds = %invoke.cont.11
  invoke void @_ZN6Metais7ajustarEi(%class.Metais* %Fliscorne, i32 1)
          to label %invoke.cont.13 unwind label %lpad.7

invoke.cont.13:                                   ; preds = %invoke.cont.12
  call void @_ZN8MadeirasD2Ev(%class.Madeiras* %FlautaDoce) #2
  call void @_ZN6MetaisD2Ev(%class.Metais* %Fliscorne) #2
  call void @_ZN9ComCordasD2Ev(%class.ComCordas* %violino) #2
  call void @_ZN9PercussaoD2Ev(%class.Percussao* %tambor) #2
  call void @_ZN5SoproD2Ev(%class.Sopro* %flauta) #2
  ret i32 0

lpad:                                             ; preds = %entry
  %0 = landingpad { i8*, i32 }
          cleanup
  %1 = extractvalue { i8*, i32 } %0, 0
  store i8* %1, i8** %exn.slot
  %2 = extractvalue { i8*, i32 } %0, 1
  store i32 %2, i32* %ehselector.slot
  br label %ehcleanup.16

lpad.1:                                           ; preds = %invoke.cont
  %3 = landingpad { i8*, i32 }
          cleanup
  %4 = extractvalue { i8*, i32 } %3, 0
  store i8* %4, i8** %exn.slot
  %5 = extractvalue { i8*, i32 } %3, 1
  store i32 %5, i32* %ehselector.slot
  br label %ehcleanup.15

lpad.3:                                           ; preds = %invoke.cont.2
  %6 = landingpad { i8*, i32 }
          cleanup
  %7 = extractvalue { i8*, i32 } %6, 0
  store i8* %7, i8** %exn.slot
  %8 = extractvalue { i8*, i32 } %6, 1
  store i32 %8, i32* %ehselector.slot
  br label %ehcleanup.14

lpad.5:                                           ; preds = %invoke.cont.4
  %9 = landingpad { i8*, i32 }
          cleanup
  %10 = extractvalue { i8*, i32 } %9, 0
  store i8* %10, i8** %exn.slot
  %11 = extractvalue { i8*, i32 } %9, 1
  store i32 %11, i32* %ehselector.slot
  br label %ehcleanup

lpad.7:                                           ; preds = %invoke.cont.12, %invoke.cont.11, %invoke.cont.10, %invoke.cont.9, %invoke.cont.8, %invoke.cont.6
  %12 = landingpad { i8*, i32 }
          cleanup
  %13 = extractvalue { i8*, i32 } %12, 0
  store i8* %13, i8** %exn.slot
  %14 = extractvalue { i8*, i32 } %12, 1
  store i32 %14, i32* %ehselector.slot
  call void @_ZN8MadeirasD2Ev(%class.Madeiras* %FlautaDoce) #2
  br label %ehcleanup

ehcleanup:                                        ; preds = %lpad.7, %lpad.5
  call void @_ZN6MetaisD2Ev(%class.Metais* %Fliscorne) #2
  br label %ehcleanup.14

ehcleanup.14:                                     ; preds = %ehcleanup, %lpad.3
  call void @_ZN9ComCordasD2Ev(%class.ComCordas* %violino) #2
  br label %ehcleanup.15

ehcleanup.15:                                     ; preds = %ehcleanup.14, %lpad.1
  call void @_ZN9PercussaoD2Ev(%class.Percussao* %tambor) #2
  br label %ehcleanup.16

ehcleanup.16:                                     ; preds = %ehcleanup.15, %lpad
  call void @_ZN5SoproD2Ev(%class.Sopro* %flauta) #2
  br label %eh.resume

eh.resume:                                        ; preds = %ehcleanup.16
  %exn = load i8*, i8** %exn.slot
  %sel = load i32, i32* %ehselector.slot
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val.17 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val.17
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN5SoproC2Ev(%class.Sopro* %this) unnamed_addr #3 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_seh0 to i8*) {
entry:
  %this.addr = alloca %class.Sopro*, align 8
  %ref.tmp = alloca %"class.std::__cxx11::basic_string", align 8
  %exn.slot = alloca i8*
  %ehselector.slot = alloca i32
  store %class.Sopro* %this, %class.Sopro** %this.addr, align 8
  %this1 = load %class.Sopro*, %class.Sopro** %this.addr
  %0 = bitcast %class.Sopro* %this1 to %class.Instrumento*
  call void @_ZN11InstrumentoC2Ev(%class.Instrumento* %0) #2
  %1 = bitcast %class.Sopro* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV5Sopro, i64 0, i64 2) to i32 (...)**), i32 (...)*** %1
  %tipo = getelementptr inbounds %class.Sopro, %class.Sopro* %this1, i32 0, i32 2
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* %tipo) #2
  %tipo2 = getelementptr inbounds %class.Sopro, %class.Sopro* %this1, i32 0, i32 2
  %2 = bitcast %class.Sopro* %this1 to void (%"class.std::__cxx11::basic_string"*, %class.Sopro*)***
  %vtable = load void (%"class.std::__cxx11::basic_string"*, %class.Sopro*)**, void (%"class.std::__cxx11::basic_string"*, %class.Sopro*)*** %2
  %vfn = getelementptr inbounds void (%"class.std::__cxx11::basic_string"*, %class.Sopro*)*, void (%"class.std::__cxx11::basic_string"*, %class.Sopro*)** %vtable, i64 1
  %3 = load void (%"class.std::__cxx11::basic_string"*, %class.Sopro*)*, void (%"class.std::__cxx11::basic_string"*, %class.Sopro*)** %vfn
  invoke void %3(%"class.std::__cxx11::basic_string"* sret %ref.tmp, %class.Sopro* %this1)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  %call = invoke dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEOS4_(%"class.std::__cxx11::basic_string"* %tipo2, %"class.std::__cxx11::basic_string"* dereferenceable(32) %ref.tmp)
          to label %invoke.cont.4 unwind label %lpad.3

invoke.cont.4:                                    ; preds = %invoke.cont
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %ref.tmp) #2
  %4 = bitcast %class.Sopro* %this1 to void (%class.Sopro*, i32)***
  %vtable5 = load void (%class.Sopro*, i32)**, void (%class.Sopro*, i32)*** %4
  %vfn6 = getelementptr inbounds void (%class.Sopro*, i32)*, void (%class.Sopro*, i32)** %vtable5, i64 2
  %5 = load void (%class.Sopro*, i32)*, void (%class.Sopro*, i32)** %vfn6
  invoke void %5(%class.Sopro* %this1, i32 0)
          to label %invoke.cont.7 unwind label %lpad

invoke.cont.7:                                    ; preds = %invoke.cont.4
  ret void

lpad:                                             ; preds = %invoke.cont.4, %entry
  %6 = landingpad { i8*, i32 }
          cleanup
  %7 = extractvalue { i8*, i32 } %6, 0
  store i8* %7, i8** %exn.slot
  %8 = extractvalue { i8*, i32 } %6, 1
  store i32 %8, i32* %ehselector.slot
  br label %ehcleanup

lpad.3:                                           ; preds = %invoke.cont
  %9 = landingpad { i8*, i32 }
          cleanup
  %10 = extractvalue { i8*, i32 } %9, 0
  store i8* %10, i8** %exn.slot
  %11 = extractvalue { i8*, i32 } %9, 1
  store i32 %11, i32* %ehselector.slot
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %ref.tmp) #2
  br label %ehcleanup

ehcleanup:                                        ; preds = %lpad.3, %lpad
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %tipo) #2
  %12 = bitcast %class.Sopro* %this1 to %class.Instrumento*
  call void @_ZN11InstrumentoD2Ev(%class.Instrumento* %12) #2
  br label %eh.resume

eh.resume:                                        ; preds = %ehcleanup
  %exn = load i8*, i8** %exn.slot
  %sel = load i32, i32* %ehselector.slot
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val.9 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val.9
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN9PercussaoC2Ev(%class.Percussao* %this) unnamed_addr #3 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_seh0 to i8*) {
entry:
  %this.addr = alloca %class.Percussao*, align 8
  %ref.tmp = alloca %"class.std::__cxx11::basic_string", align 8
  %exn.slot = alloca i8*
  %ehselector.slot = alloca i32
  store %class.Percussao* %this, %class.Percussao** %this.addr, align 8
  %this1 = load %class.Percussao*, %class.Percussao** %this.addr
  %0 = bitcast %class.Percussao* %this1 to %class.Instrumento*
  call void @_ZN11InstrumentoC2Ev(%class.Instrumento* %0) #2
  %1 = bitcast %class.Percussao* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV9Percussao, i64 0, i64 2) to i32 (...)**), i32 (...)*** %1
  %tipo = getelementptr inbounds %class.Percussao, %class.Percussao* %this1, i32 0, i32 2
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* %tipo) #2
  %tipo2 = getelementptr inbounds %class.Percussao, %class.Percussao* %this1, i32 0, i32 2
  %2 = bitcast %class.Percussao* %this1 to void (%"class.std::__cxx11::basic_string"*, %class.Percussao*)***
  %vtable = load void (%"class.std::__cxx11::basic_string"*, %class.Percussao*)**, void (%"class.std::__cxx11::basic_string"*, %class.Percussao*)*** %2
  %vfn = getelementptr inbounds void (%"class.std::__cxx11::basic_string"*, %class.Percussao*)*, void (%"class.std::__cxx11::basic_string"*, %class.Percussao*)** %vtable, i64 1
  %3 = load void (%"class.std::__cxx11::basic_string"*, %class.Percussao*)*, void (%"class.std::__cxx11::basic_string"*, %class.Percussao*)** %vfn
  invoke void %3(%"class.std::__cxx11::basic_string"* sret %ref.tmp, %class.Percussao* %this1)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  %call = invoke dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEOS4_(%"class.std::__cxx11::basic_string"* %tipo2, %"class.std::__cxx11::basic_string"* dereferenceable(32) %ref.tmp)
          to label %invoke.cont.4 unwind label %lpad.3

invoke.cont.4:                                    ; preds = %invoke.cont
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %ref.tmp) #2
  %4 = bitcast %class.Percussao* %this1 to void (%class.Percussao*, i32)***
  %vtable5 = load void (%class.Percussao*, i32)**, void (%class.Percussao*, i32)*** %4
  %vfn6 = getelementptr inbounds void (%class.Percussao*, i32)*, void (%class.Percussao*, i32)** %vtable5, i64 2
  %5 = load void (%class.Percussao*, i32)*, void (%class.Percussao*, i32)** %vfn6
  invoke void %5(%class.Percussao* %this1, i32 0)
          to label %invoke.cont.7 unwind label %lpad

invoke.cont.7:                                    ; preds = %invoke.cont.4
  ret void

lpad:                                             ; preds = %invoke.cont.4, %entry
  %6 = landingpad { i8*, i32 }
          cleanup
  %7 = extractvalue { i8*, i32 } %6, 0
  store i8* %7, i8** %exn.slot
  %8 = extractvalue { i8*, i32 } %6, 1
  store i32 %8, i32* %ehselector.slot
  br label %ehcleanup

lpad.3:                                           ; preds = %invoke.cont
  %9 = landingpad { i8*, i32 }
          cleanup
  %10 = extractvalue { i8*, i32 } %9, 0
  store i8* %10, i8** %exn.slot
  %11 = extractvalue { i8*, i32 } %9, 1
  store i32 %11, i32* %ehselector.slot
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %ref.tmp) #2
  br label %ehcleanup

ehcleanup:                                        ; preds = %lpad.3, %lpad
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %tipo) #2
  %12 = bitcast %class.Percussao* %this1 to %class.Instrumento*
  call void @_ZN11InstrumentoD2Ev(%class.Instrumento* %12) #2
  br label %eh.resume

eh.resume:                                        ; preds = %ehcleanup
  %exn = load i8*, i8** %exn.slot
  %sel = load i32, i32* %ehselector.slot
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val.9 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val.9
}

declare i32 @__gxx_personality_seh0(...)

; Function Attrs: uwtable
define linkonce_odr void @_ZN9ComCordasC2Ev(%class.ComCordas* %this) unnamed_addr #3 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_seh0 to i8*) {
entry:
  %this.addr = alloca %class.ComCordas*, align 8
  %ref.tmp = alloca %"class.std::__cxx11::basic_string", align 8
  %exn.slot = alloca i8*
  %ehselector.slot = alloca i32
  store %class.ComCordas* %this, %class.ComCordas** %this.addr, align 8
  %this1 = load %class.ComCordas*, %class.ComCordas** %this.addr
  %0 = bitcast %class.ComCordas* %this1 to %class.Instrumento*
  call void @_ZN11InstrumentoC2Ev(%class.Instrumento* %0) #2
  %1 = bitcast %class.ComCordas* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV9ComCordas, i64 0, i64 2) to i32 (...)**), i32 (...)*** %1
  %tipo = getelementptr inbounds %class.ComCordas, %class.ComCordas* %this1, i32 0, i32 2
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* %tipo) #2
  %tipo2 = getelementptr inbounds %class.ComCordas, %class.ComCordas* %this1, i32 0, i32 2
  %2 = bitcast %class.ComCordas* %this1 to void (%"class.std::__cxx11::basic_string"*, %class.ComCordas*)***
  %vtable = load void (%"class.std::__cxx11::basic_string"*, %class.ComCordas*)**, void (%"class.std::__cxx11::basic_string"*, %class.ComCordas*)*** %2
  %vfn = getelementptr inbounds void (%"class.std::__cxx11::basic_string"*, %class.ComCordas*)*, void (%"class.std::__cxx11::basic_string"*, %class.ComCordas*)** %vtable, i64 1
  %3 = load void (%"class.std::__cxx11::basic_string"*, %class.ComCordas*)*, void (%"class.std::__cxx11::basic_string"*, %class.ComCordas*)** %vfn
  invoke void %3(%"class.std::__cxx11::basic_string"* sret %ref.tmp, %class.ComCordas* %this1)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  %call = invoke dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEOS4_(%"class.std::__cxx11::basic_string"* %tipo2, %"class.std::__cxx11::basic_string"* dereferenceable(32) %ref.tmp)
          to label %invoke.cont.4 unwind label %lpad.3

invoke.cont.4:                                    ; preds = %invoke.cont
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %ref.tmp) #2
  %4 = bitcast %class.ComCordas* %this1 to void (%class.ComCordas*, i32)***
  %vtable5 = load void (%class.ComCordas*, i32)**, void (%class.ComCordas*, i32)*** %4
  %vfn6 = getelementptr inbounds void (%class.ComCordas*, i32)*, void (%class.ComCordas*, i32)** %vtable5, i64 2
  %5 = load void (%class.ComCordas*, i32)*, void (%class.ComCordas*, i32)** %vfn6
  invoke void %5(%class.ComCordas* %this1, i32 0)
          to label %invoke.cont.7 unwind label %lpad

invoke.cont.7:                                    ; preds = %invoke.cont.4
  ret void

lpad:                                             ; preds = %invoke.cont.4, %entry
  %6 = landingpad { i8*, i32 }
          cleanup
  %7 = extractvalue { i8*, i32 } %6, 0
  store i8* %7, i8** %exn.slot
  %8 = extractvalue { i8*, i32 } %6, 1
  store i32 %8, i32* %ehselector.slot
  br label %ehcleanup

lpad.3:                                           ; preds = %invoke.cont
  %9 = landingpad { i8*, i32 }
          cleanup
  %10 = extractvalue { i8*, i32 } %9, 0
  store i8* %10, i8** %exn.slot
  %11 = extractvalue { i8*, i32 } %9, 1
  store i32 %11, i32* %ehselector.slot
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %ref.tmp) #2
  br label %ehcleanup

ehcleanup:                                        ; preds = %lpad.3, %lpad
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %tipo) #2
  %12 = bitcast %class.ComCordas* %this1 to %class.Instrumento*
  call void @_ZN11InstrumentoD2Ev(%class.Instrumento* %12) #2
  br label %eh.resume

eh.resume:                                        ; preds = %ehcleanup
  %exn = load i8*, i8** %exn.slot
  %sel = load i32, i32* %ehselector.slot
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val.9 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val.9
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN6MetaisC2Ev(%class.Metais* %this) unnamed_addr #3 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_seh0 to i8*) {
entry:
  %this.addr = alloca %class.Metais*, align 8
  %ref.tmp = alloca %"class.std::__cxx11::basic_string", align 8
  %exn.slot = alloca i8*
  %ehselector.slot = alloca i32
  store %class.Metais* %this, %class.Metais** %this.addr, align 8
  %this1 = load %class.Metais*, %class.Metais** %this.addr
  %0 = bitcast %class.Metais* %this1 to %class.Sopro*
  call void @_ZN5SoproC2Ev(%class.Sopro* %0)
  %1 = bitcast %class.Metais* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV6Metais, i64 0, i64 2) to i32 (...)**), i32 (...)*** %1
  %categoria = getelementptr inbounds %class.Metais, %class.Metais* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* %categoria) #2
  %categoria2 = getelementptr inbounds %class.Metais, %class.Metais* %this1, i32 0, i32 1
  %2 = bitcast %class.Metais* %this1 to void (%"class.std::__cxx11::basic_string"*, %class.Metais*)***
  %vtable = load void (%"class.std::__cxx11::basic_string"*, %class.Metais*)**, void (%"class.std::__cxx11::basic_string"*, %class.Metais*)*** %2
  %vfn = getelementptr inbounds void (%"class.std::__cxx11::basic_string"*, %class.Metais*)*, void (%"class.std::__cxx11::basic_string"*, %class.Metais*)** %vtable, i64 1
  %3 = load void (%"class.std::__cxx11::basic_string"*, %class.Metais*)*, void (%"class.std::__cxx11::basic_string"*, %class.Metais*)** %vfn
  invoke void %3(%"class.std::__cxx11::basic_string"* sret %ref.tmp, %class.Metais* %this1)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  %call = invoke dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEOS4_(%"class.std::__cxx11::basic_string"* %categoria2, %"class.std::__cxx11::basic_string"* dereferenceable(32) %ref.tmp)
          to label %invoke.cont.4 unwind label %lpad.3

invoke.cont.4:                                    ; preds = %invoke.cont
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %ref.tmp) #2
  %4 = bitcast %class.Metais* %this1 to void (%class.Metais*, i32)***
  %vtable5 = load void (%class.Metais*, i32)**, void (%class.Metais*, i32)*** %4
  %vfn6 = getelementptr inbounds void (%class.Metais*, i32)*, void (%class.Metais*, i32)** %vtable5, i64 2
  %5 = load void (%class.Metais*, i32)*, void (%class.Metais*, i32)** %vfn6
  invoke void %5(%class.Metais* %this1, i32 0)
          to label %invoke.cont.7 unwind label %lpad

invoke.cont.7:                                    ; preds = %invoke.cont.4
  ret void

lpad:                                             ; preds = %invoke.cont.4, %entry
  %6 = landingpad { i8*, i32 }
          cleanup
  %7 = extractvalue { i8*, i32 } %6, 0
  store i8* %7, i8** %exn.slot
  %8 = extractvalue { i8*, i32 } %6, 1
  store i32 %8, i32* %ehselector.slot
  br label %ehcleanup

lpad.3:                                           ; preds = %invoke.cont
  %9 = landingpad { i8*, i32 }
          cleanup
  %10 = extractvalue { i8*, i32 } %9, 0
  store i8* %10, i8** %exn.slot
  %11 = extractvalue { i8*, i32 } %9, 1
  store i32 %11, i32* %ehselector.slot
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %ref.tmp) #2
  br label %ehcleanup

ehcleanup:                                        ; preds = %lpad.3, %lpad
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %categoria) #2
  %12 = bitcast %class.Metais* %this1 to %class.Sopro*
  call void @_ZN5SoproD2Ev(%class.Sopro* %12) #2
  br label %eh.resume

eh.resume:                                        ; preds = %ehcleanup
  %exn = load i8*, i8** %exn.slot
  %sel = load i32, i32* %ehselector.slot
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val.9 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val.9
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN8MadeirasC2Ev(%class.Madeiras* %this) unnamed_addr #3 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_seh0 to i8*) {
entry:
  %this.addr = alloca %class.Madeiras*, align 8
  %ref.tmp = alloca %"class.std::__cxx11::basic_string", align 8
  %exn.slot = alloca i8*
  %ehselector.slot = alloca i32
  store %class.Madeiras* %this, %class.Madeiras** %this.addr, align 8
  %this1 = load %class.Madeiras*, %class.Madeiras** %this.addr
  %0 = bitcast %class.Madeiras* %this1 to %class.Sopro*
  call void @_ZN5SoproC2Ev(%class.Sopro* %0)
  %1 = bitcast %class.Madeiras* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV8Madeiras, i64 0, i64 2) to i32 (...)**), i32 (...)*** %1
  %categoria = getelementptr inbounds %class.Madeiras, %class.Madeiras* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* %categoria) #2
  %categoria2 = getelementptr inbounds %class.Madeiras, %class.Madeiras* %this1, i32 0, i32 1
  %2 = bitcast %class.Madeiras* %this1 to void (%"class.std::__cxx11::basic_string"*, %class.Madeiras*)***
  %vtable = load void (%"class.std::__cxx11::basic_string"*, %class.Madeiras*)**, void (%"class.std::__cxx11::basic_string"*, %class.Madeiras*)*** %2
  %vfn = getelementptr inbounds void (%"class.std::__cxx11::basic_string"*, %class.Madeiras*)*, void (%"class.std::__cxx11::basic_string"*, %class.Madeiras*)** %vtable, i64 1
  %3 = load void (%"class.std::__cxx11::basic_string"*, %class.Madeiras*)*, void (%"class.std::__cxx11::basic_string"*, %class.Madeiras*)** %vfn
  invoke void %3(%"class.std::__cxx11::basic_string"* sret %ref.tmp, %class.Madeiras* %this1)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  %call = invoke dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEOS4_(%"class.std::__cxx11::basic_string"* %categoria2, %"class.std::__cxx11::basic_string"* dereferenceable(32) %ref.tmp)
          to label %invoke.cont.4 unwind label %lpad.3

invoke.cont.4:                                    ; preds = %invoke.cont
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %ref.tmp) #2
  %4 = bitcast %class.Madeiras* %this1 to void (%class.Madeiras*, i32)***
  %vtable5 = load void (%class.Madeiras*, i32)**, void (%class.Madeiras*, i32)*** %4
  %vfn6 = getelementptr inbounds void (%class.Madeiras*, i32)*, void (%class.Madeiras*, i32)** %vtable5, i64 2
  %5 = load void (%class.Madeiras*, i32)*, void (%class.Madeiras*, i32)** %vfn6
  invoke void %5(%class.Madeiras* %this1, i32 0)
          to label %invoke.cont.7 unwind label %lpad

invoke.cont.7:                                    ; preds = %invoke.cont.4
  ret void

lpad:                                             ; preds = %invoke.cont.4, %entry
  %6 = landingpad { i8*, i32 }
          cleanup
  %7 = extractvalue { i8*, i32 } %6, 0
  store i8* %7, i8** %exn.slot
  %8 = extractvalue { i8*, i32 } %6, 1
  store i32 %8, i32* %ehselector.slot
  br label %ehcleanup

lpad.3:                                           ; preds = %invoke.cont
  %9 = landingpad { i8*, i32 }
          cleanup
  %10 = extractvalue { i8*, i32 } %9, 0
  store i8* %10, i8** %exn.slot
  %11 = extractvalue { i8*, i32 } %9, 1
  store i32 %11, i32* %ehselector.slot
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %ref.tmp) #2
  br label %ehcleanup

ehcleanup:                                        ; preds = %lpad.3, %lpad
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %categoria) #2
  %12 = bitcast %class.Madeiras* %this1 to %class.Sopro*
  call void @_ZN5SoproD2Ev(%class.Sopro* %12) #2
  br label %eh.resume

eh.resume:                                        ; preds = %ehcleanup
  %exn = load i8*, i8** %exn.slot
  %sel = load i32, i32* %ehselector.slot
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val.9 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val.9
}

; Function Attrs: uwtable
define linkonce_odr void @_ZNK5Sopro5tocarE4nota(%class.Sopro* %this, i32) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.Sopro*, align 8
  %.addr = alloca i32, align 4
  store %class.Sopro* %this, %class.Sopro** %this.addr, align 8
  store i32 %0, i32* %.addr, align 4
  %this1 = load %class.Sopro*, %class.Sopro** %this.addr
  %tipo = getelementptr inbounds %class.Sopro, %class.Sopro* %this1, i32 0, i32 2
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, %"class.std::__cxx11::basic_string"* dereferenceable(32) %tipo)
  %call2 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.7, i32 0, i32 0))
  %ajuste = getelementptr inbounds %class.Sopro, %class.Sopro* %this1, i32 0, i32 1
  %1 = load i32, i32* %ajuste, align 4
  %call3 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call2, i32 %1)
  %call4 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* %call3, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZNK9Percussao5tocarE4nota(%class.Percussao* %this, i32) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.Percussao*, align 8
  %.addr = alloca i32, align 4
  store %class.Percussao* %this, %class.Percussao** %this.addr, align 8
  store i32 %0, i32* %.addr, align 4
  %this1 = load %class.Percussao*, %class.Percussao** %this.addr
  %tipo = getelementptr inbounds %class.Percussao, %class.Percussao* %this1, i32 0, i32 2
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, %"class.std::__cxx11::basic_string"* dereferenceable(32) %tipo)
  %call2 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.7, i32 0, i32 0))
  %ajuste = getelementptr inbounds %class.Percussao, %class.Percussao* %this1, i32 0, i32 1
  %1 = load i32, i32* %ajuste, align 4
  %call3 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call2, i32 %1)
  %call4 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* %call3, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZNK9ComCordas5tocarE4nota(%class.ComCordas* %this, i32) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.ComCordas*, align 8
  %.addr = alloca i32, align 4
  store %class.ComCordas* %this, %class.ComCordas** %this.addr, align 8
  store i32 %0, i32* %.addr, align 4
  %this1 = load %class.ComCordas*, %class.ComCordas** %this.addr
  %tipo = getelementptr inbounds %class.ComCordas, %class.ComCordas* %this1, i32 0, i32 2
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, %"class.std::__cxx11::basic_string"* dereferenceable(32) %tipo)
  %call2 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.7, i32 0, i32 0))
  %ajuste = getelementptr inbounds %class.ComCordas, %class.ComCordas* %this1, i32 0, i32 1
  %1 = load i32, i32* %ajuste, align 4
  %call3 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call2, i32 %1)
  %call4 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* %call3, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZNK6Metais5tocarE4nota(%class.Metais* %this, i32) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.Metais*, align 8
  %.addr = alloca i32, align 4
  store %class.Metais* %this, %class.Metais** %this.addr, align 8
  store i32 %0, i32* %.addr, align 4
  %this1 = load %class.Metais*, %class.Metais** %this.addr
  %1 = bitcast %class.Metais* %this1 to %class.Sopro*
  %tipo = getelementptr inbounds %class.Sopro, %class.Sopro* %1, i32 0, i32 2
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, %"class.std::__cxx11::basic_string"* dereferenceable(32) %tipo)
  %call2 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.8, i32 0, i32 0))
  %categoria = getelementptr inbounds %class.Metais, %class.Metais* %this1, i32 0, i32 1
  %call3 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* dereferenceable(272) %call2, %"class.std::__cxx11::basic_string"* dereferenceable(32) %categoria)
  %call4 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call3, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.7, i32 0, i32 0))
  %2 = bitcast %class.Metais* %this1 to %class.Sopro*
  %ajuste = getelementptr inbounds %class.Sopro, %class.Sopro* %2, i32 0, i32 1
  %3 = load i32, i32* %ajuste, align 4
  %call5 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call4, i32 %3)
  %call6 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* %call5, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZNK8Madeiras5tocarE4nota(%class.Madeiras* %this, i32) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.Madeiras*, align 8
  %.addr = alloca i32, align 4
  store %class.Madeiras* %this, %class.Madeiras** %this.addr, align 8
  store i32 %0, i32* %.addr, align 4
  %this1 = load %class.Madeiras*, %class.Madeiras** %this.addr
  %1 = bitcast %class.Madeiras* %this1 to %class.Sopro*
  %tipo = getelementptr inbounds %class.Sopro, %class.Sopro* %1, i32 0, i32 2
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, %"class.std::__cxx11::basic_string"* dereferenceable(32) %tipo)
  %call2 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.8, i32 0, i32 0))
  %categoria = getelementptr inbounds %class.Madeiras, %class.Madeiras* %this1, i32 0, i32 1
  %call3 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* dereferenceable(272) %call2, %"class.std::__cxx11::basic_string"* dereferenceable(32) %categoria)
  %call4 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call3, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.7, i32 0, i32 0))
  %2 = bitcast %class.Madeiras* %this1 to %class.Sopro*
  %ajuste = getelementptr inbounds %class.Sopro, %class.Sopro* %2, i32 0, i32 1
  %3 = load i32, i32* %ajuste, align 4
  %call5 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call4, i32 %3)
  %call6 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* %call5, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN6Metais7ajustarEi(%class.Metais* %this, i32 %j) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.Metais*, align 8
  %j.addr = alloca i32, align 4
  %tmp = alloca %"class.std::__cxx11::basic_string", align 8
  store %class.Metais* %this, %class.Metais** %this.addr, align 8
  store i32 %j, i32* %j.addr, align 4
  %this1 = load %class.Metais*, %class.Metais** %this.addr
  %0 = load i32, i32* %j.addr, align 4
  %1 = bitcast %class.Metais* %this1 to %class.Sopro*
  %ajuste = getelementptr inbounds %class.Sopro, %class.Sopro* %1, i32 0, i32 1
  store i32 %0, i32* %ajuste, align 4
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.5, i32 0, i32 0))
  %2 = bitcast %class.Metais* %this1 to %class.Sopro*
  %ajuste2 = getelementptr inbounds %class.Sopro, %class.Sopro* %2, i32 0, i32 1
  %3 = load i32, i32* %ajuste2, align 4
  %call3 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call, i32 %3)
  %call4 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call3, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.6, i32 0, i32 0))
  %categoria = getelementptr inbounds %class.Metais, %class.Metais* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* %tmp, %"class.std::__cxx11::basic_string"* dereferenceable(32) %categoria)
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %tmp) #2
  ret void
}

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN8MadeirasD2Ev(%class.Madeiras* %this) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.Madeiras*, align 8
  store %class.Madeiras* %this, %class.Madeiras** %this.addr, align 8
  %this1 = load %class.Madeiras*, %class.Madeiras** %this.addr
  %0 = bitcast %class.Madeiras* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV8Madeiras, i64 0, i64 2) to i32 (...)**), i32 (...)*** %0
  %categoria = getelementptr inbounds %class.Madeiras, %class.Madeiras* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %categoria) #2
  %1 = bitcast %class.Madeiras* %this1 to %class.Sopro*
  call void @_ZN5SoproD2Ev(%class.Sopro* %1) #2
  ret void
}

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN6MetaisD2Ev(%class.Metais* %this) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.Metais*, align 8
  store %class.Metais* %this, %class.Metais** %this.addr, align 8
  %this1 = load %class.Metais*, %class.Metais** %this.addr
  %0 = bitcast %class.Metais* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV6Metais, i64 0, i64 2) to i32 (...)**), i32 (...)*** %0
  %categoria = getelementptr inbounds %class.Metais, %class.Metais* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %categoria) #2
  %1 = bitcast %class.Metais* %this1 to %class.Sopro*
  call void @_ZN5SoproD2Ev(%class.Sopro* %1) #2
  ret void
}

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN9ComCordasD2Ev(%class.ComCordas* %this) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.ComCordas*, align 8
  store %class.ComCordas* %this, %class.ComCordas** %this.addr, align 8
  %this1 = load %class.ComCordas*, %class.ComCordas** %this.addr
  %0 = bitcast %class.ComCordas* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV9ComCordas, i64 0, i64 2) to i32 (...)**), i32 (...)*** %0
  %tipo = getelementptr inbounds %class.ComCordas, %class.ComCordas* %this1, i32 0, i32 2
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %tipo) #2
  %1 = bitcast %class.ComCordas* %this1 to %class.Instrumento*
  call void @_ZN11InstrumentoD2Ev(%class.Instrumento* %1) #2
  ret void
}

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN9PercussaoD2Ev(%class.Percussao* %this) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.Percussao*, align 8
  store %class.Percussao* %this, %class.Percussao** %this.addr, align 8
  %this1 = load %class.Percussao*, %class.Percussao** %this.addr
  %0 = bitcast %class.Percussao* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV9Percussao, i64 0, i64 2) to i32 (...)**), i32 (...)*** %0
  %tipo = getelementptr inbounds %class.Percussao, %class.Percussao* %this1, i32 0, i32 2
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %tipo) #2
  %1 = bitcast %class.Percussao* %this1 to %class.Instrumento*
  call void @_ZN11InstrumentoD2Ev(%class.Instrumento* %1) #2
  ret void
}

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN5SoproD2Ev(%class.Sopro* %this) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.Sopro*, align 8
  store %class.Sopro* %this, %class.Sopro** %this.addr, align 8
  %this1 = load %class.Sopro*, %class.Sopro** %this.addr
  %0 = bitcast %class.Sopro* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV5Sopro, i64 0, i64 2) to i32 (...)**), i32 (...)*** %0
  %tipo = getelementptr inbounds %class.Sopro, %class.Sopro* %this1, i32 0, i32 2
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %tipo) #2
  %1 = bitcast %class.Sopro* %this1 to %class.Instrumento*
  call void @_ZN11InstrumentoD2Ev(%class.Instrumento* %1) #2
  ret void
}

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN11InstrumentoC2Ev(%class.Instrumento* %this) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.Instrumento*, align 8
  store %class.Instrumento* %this, %class.Instrumento** %this.addr, align 8
  %this1 = load %class.Instrumento*, %class.Instrumento** %this.addr
  %0 = bitcast %class.Instrumento* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV11Instrumento, i64 0, i64 2) to i32 (...)**), i32 (...)*** %0
  %tipo = getelementptr inbounds %class.Instrumento, %class.Instrumento* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"* %tipo) #2
  ret void
}

; Function Attrs: nounwind
declare void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev(%"class.std::__cxx11::basic_string"*) #1

declare dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEOS4_(%"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"* dereferenceable(32)) #0

; Function Attrs: nounwind
declare void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"*) #1

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN11InstrumentoD2Ev(%class.Instrumento* %this) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.Instrumento*, align 8
  store %class.Instrumento* %this, %class.Instrumento** %this.addr, align 8
  %this1 = load %class.Instrumento*, %class.Instrumento** %this.addr
  %0 = bitcast %class.Instrumento* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([5 x i8*], [5 x i8*]* @_ZTV11Instrumento, i64 0, i64 2) to i32 (...)**), i32 (...)*** %0
  %tipo = getelementptr inbounds %class.Instrumento, %class.Instrumento* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %tipo) #2
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZNK5Sopro4OqueEv(%"class.std::__cxx11::basic_string"* noalias sret %agg.result, %class.Sopro* %this) unnamed_addr #3 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_seh0 to i8*) {
entry:
  %this.addr = alloca %class.Sopro*, align 8
  %ref.tmp = alloca %"class.std::allocator", align 1
  %exn.slot = alloca i8*
  %ehselector.slot = alloca i32
  store %class.Sopro* %this, %class.Sopro** %this.addr, align 8
  %this1 = load %class.Sopro*, %class.Sopro** %this.addr
  call void @_ZNSaIcEC1Ev(%"class.std::allocator"* %ref.tmp) #2
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1EPKcRKS3_(%"class.std::__cxx11::basic_string"* %agg.result, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str, i32 0, i32 0), %"class.std::allocator"* dereferenceable(1) %ref.tmp)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  call void @_ZNSaIcED1Ev(%"class.std::allocator"* %ref.tmp) #2
  ret void

lpad:                                             ; preds = %entry
  %0 = landingpad { i8*, i32 }
          cleanup
  %1 = extractvalue { i8*, i32 } %0, 0
  store i8* %1, i8** %exn.slot
  %2 = extractvalue { i8*, i32 } %0, 1
  store i32 %2, i32* %ehselector.slot
  call void @_ZNSaIcED1Ev(%"class.std::allocator"* %ref.tmp) #2
  br label %eh.resume

eh.resume:                                        ; preds = %lpad
  %exn = load i8*, i8** %exn.slot
  %sel = load i32, i32* %ehselector.slot
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val.2 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val.2
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN5Sopro7ajustarEi(%class.Sopro* %this, i32 %j) unnamed_addr #5 comdat align 2 {
entry:
  %this.addr = alloca %class.Sopro*, align 8
  %j.addr = alloca i32, align 4
  store %class.Sopro* %this, %class.Sopro** %this.addr, align 8
  store i32 %j, i32* %j.addr, align 4
  %this1 = load %class.Sopro*, %class.Sopro** %this.addr
  %0 = load i32, i32* %j.addr, align 4
  %ajuste = getelementptr inbounds %class.Sopro, %class.Sopro* %this1, i32 0, i32 1
  store i32 %0, i32* %ajuste, align 4
  ret void
}

declare void @__cxa_pure_virtual()

; Function Attrs: nounwind
declare void @_ZNSaIcEC1Ev(%"class.std::allocator"*) #1

declare void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1EPKcRKS3_(%"class.std::__cxx11::basic_string"*, i8*, %"class.std::allocator"* dereferenceable(1)) #0

; Function Attrs: nounwind
declare void @_ZNSaIcED1Ev(%"class.std::allocator"*) #1

; Function Attrs: uwtable
define linkonce_odr void @_ZNK9Percussao4OqueEv(%"class.std::__cxx11::basic_string"* noalias sret %agg.result, %class.Percussao* %this) unnamed_addr #3 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_seh0 to i8*) {
entry:
  %this.addr = alloca %class.Percussao*, align 8
  %ref.tmp = alloca %"class.std::allocator", align 1
  %exn.slot = alloca i8*
  %ehselector.slot = alloca i32
  store %class.Percussao* %this, %class.Percussao** %this.addr, align 8
  %this1 = load %class.Percussao*, %class.Percussao** %this.addr
  call void @_ZNSaIcEC1Ev(%"class.std::allocator"* %ref.tmp) #2
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1EPKcRKS3_(%"class.std::__cxx11::basic_string"* %agg.result, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.1, i32 0, i32 0), %"class.std::allocator"* dereferenceable(1) %ref.tmp)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  call void @_ZNSaIcED1Ev(%"class.std::allocator"* %ref.tmp) #2
  ret void

lpad:                                             ; preds = %entry
  %0 = landingpad { i8*, i32 }
          cleanup
  %1 = extractvalue { i8*, i32 } %0, 0
  store i8* %1, i8** %exn.slot
  %2 = extractvalue { i8*, i32 } %0, 1
  store i32 %2, i32* %ehselector.slot
  call void @_ZNSaIcED1Ev(%"class.std::allocator"* %ref.tmp) #2
  br label %eh.resume

eh.resume:                                        ; preds = %lpad
  %exn = load i8*, i8** %exn.slot
  %sel = load i32, i32* %ehselector.slot
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val.2 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val.2
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN9Percussao7ajustarEi(%class.Percussao* %this, i32 %j) unnamed_addr #5 comdat align 2 {
entry:
  %this.addr = alloca %class.Percussao*, align 8
  %j.addr = alloca i32, align 4
  store %class.Percussao* %this, %class.Percussao** %this.addr, align 8
  store i32 %j, i32* %j.addr, align 4
  %this1 = load %class.Percussao*, %class.Percussao** %this.addr
  %0 = load i32, i32* %j.addr, align 4
  %ajuste = getelementptr inbounds %class.Percussao, %class.Percussao* %this1, i32 0, i32 1
  store i32 %0, i32* %ajuste, align 4
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZNK9ComCordas4OqueEv(%"class.std::__cxx11::basic_string"* noalias sret %agg.result, %class.ComCordas* %this) unnamed_addr #3 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_seh0 to i8*) {
entry:
  %this.addr = alloca %class.ComCordas*, align 8
  %ref.tmp = alloca %"class.std::allocator", align 1
  %exn.slot = alloca i8*
  %ehselector.slot = alloca i32
  store %class.ComCordas* %this, %class.ComCordas** %this.addr, align 8
  %this1 = load %class.ComCordas*, %class.ComCordas** %this.addr
  call void @_ZNSaIcEC1Ev(%"class.std::allocator"* %ref.tmp) #2
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1EPKcRKS3_(%"class.std::__cxx11::basic_string"* %agg.result, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.2, i32 0, i32 0), %"class.std::allocator"* dereferenceable(1) %ref.tmp)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  call void @_ZNSaIcED1Ev(%"class.std::allocator"* %ref.tmp) #2
  ret void

lpad:                                             ; preds = %entry
  %0 = landingpad { i8*, i32 }
          cleanup
  %1 = extractvalue { i8*, i32 } %0, 0
  store i8* %1, i8** %exn.slot
  %2 = extractvalue { i8*, i32 } %0, 1
  store i32 %2, i32* %ehselector.slot
  call void @_ZNSaIcED1Ev(%"class.std::allocator"* %ref.tmp) #2
  br label %eh.resume

eh.resume:                                        ; preds = %lpad
  %exn = load i8*, i8** %exn.slot
  %sel = load i32, i32* %ehselector.slot
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val.2 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val.2
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN9ComCordas7ajustarEi(%class.ComCordas* %this, i32 %j) unnamed_addr #5 comdat align 2 {
entry:
  %this.addr = alloca %class.ComCordas*, align 8
  %j.addr = alloca i32, align 4
  store %class.ComCordas* %this, %class.ComCordas** %this.addr, align 8
  store i32 %j, i32* %j.addr, align 4
  %this1 = load %class.ComCordas*, %class.ComCordas** %this.addr
  %0 = load i32, i32* %j.addr, align 4
  %ajuste = getelementptr inbounds %class.ComCordas, %class.ComCordas* %this1, i32 0, i32 1
  store i32 %0, i32* %ajuste, align 4
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZNK6Metais4OqueEv(%"class.std::__cxx11::basic_string"* noalias sret %agg.result, %class.Metais* %this) unnamed_addr #3 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_seh0 to i8*) {
entry:
  %this.addr = alloca %class.Metais*, align 8
  %ref.tmp = alloca %"class.std::allocator", align 1
  %exn.slot = alloca i8*
  %ehselector.slot = alloca i32
  store %class.Metais* %this, %class.Metais** %this.addr, align 8
  %this1 = load %class.Metais*, %class.Metais** %this.addr
  call void @_ZNSaIcEC1Ev(%"class.std::allocator"* %ref.tmp) #2
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1EPKcRKS3_(%"class.std::__cxx11::basic_string"* %agg.result, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0), %"class.std::allocator"* dereferenceable(1) %ref.tmp)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  call void @_ZNSaIcED1Ev(%"class.std::allocator"* %ref.tmp) #2
  ret void

lpad:                                             ; preds = %entry
  %0 = landingpad { i8*, i32 }
          cleanup
  %1 = extractvalue { i8*, i32 } %0, 0
  store i8* %1, i8** %exn.slot
  %2 = extractvalue { i8*, i32 } %0, 1
  store i32 %2, i32* %ehselector.slot
  call void @_ZNSaIcED1Ev(%"class.std::allocator"* %ref.tmp) #2
  br label %eh.resume

eh.resume:                                        ; preds = %lpad
  %exn = load i8*, i8** %exn.slot
  %sel = load i32, i32* %ehselector.slot
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val.2 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val.2
}

; Function Attrs: uwtable
define linkonce_odr void @_ZNK8Madeiras4OqueEv(%"class.std::__cxx11::basic_string"* noalias sret %agg.result, %class.Madeiras* %this) unnamed_addr #3 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_seh0 to i8*) {
entry:
  %this.addr = alloca %class.Madeiras*, align 8
  %ref.tmp = alloca %"class.std::allocator", align 1
  %exn.slot = alloca i8*
  %ehselector.slot = alloca i32
  store %class.Madeiras* %this, %class.Madeiras** %this.addr, align 8
  %this1 = load %class.Madeiras*, %class.Madeiras** %this.addr
  call void @_ZNSaIcEC1Ev(%"class.std::allocator"* %ref.tmp) #2
  invoke void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1EPKcRKS3_(%"class.std::__cxx11::basic_string"* %agg.result, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.4, i32 0, i32 0), %"class.std::allocator"* dereferenceable(1) %ref.tmp)
          to label %invoke.cont unwind label %lpad

invoke.cont:                                      ; preds = %entry
  call void @_ZNSaIcED1Ev(%"class.std::allocator"* %ref.tmp) #2
  ret void

lpad:                                             ; preds = %entry
  %0 = landingpad { i8*, i32 }
          cleanup
  %1 = extractvalue { i8*, i32 } %0, 0
  store i8* %1, i8** %exn.slot
  %2 = extractvalue { i8*, i32 } %0, 1
  store i32 %2, i32* %ehselector.slot
  call void @_ZNSaIcED1Ev(%"class.std::allocator"* %ref.tmp) #2
  br label %eh.resume

eh.resume:                                        ; preds = %lpad
  %exn = load i8*, i8** %exn.slot
  %sel = load i32, i32* %ehselector.slot
  %lpad.val = insertvalue { i8*, i32 } undef, i8* %exn, 0
  %lpad.val.2 = insertvalue { i8*, i32 } %lpad.val, i32 %sel, 1
  resume { i8*, i32 } %lpad.val.2
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN8Madeiras7ajustarEi(%class.Madeiras* %this, i32 %j) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.Madeiras*, align 8
  %j.addr = alloca i32, align 4
  %tmp = alloca %"class.std::__cxx11::basic_string", align 8
  store %class.Madeiras* %this, %class.Madeiras** %this.addr, align 8
  store i32 %j, i32* %j.addr, align 4
  %this1 = load %class.Madeiras*, %class.Madeiras** %this.addr
  %0 = load i32, i32* %j.addr, align 4
  %1 = bitcast %class.Madeiras* %this1 to %class.Sopro*
  %ajuste = getelementptr inbounds %class.Sopro, %class.Sopro* %1, i32 0, i32 1
  store i32 %0, i32* %ajuste, align 4
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.5, i32 0, i32 0))
  %2 = bitcast %class.Madeiras* %this1 to %class.Sopro*
  %ajuste2 = getelementptr inbounds %class.Sopro, %class.Sopro* %2, i32 0, i32 1
  %3 = load i32, i32* %ajuste2, align 4
  %call3 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call, i32 %3)
  %call4 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call3, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.6, i32 0, i32 0))
  %categoria = getelementptr inbounds %class.Madeiras, %class.Madeiras* %this1, i32 0, i32 1
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"* %tmp, %"class.std::__cxx11::basic_string"* dereferenceable(32) %categoria)
  call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev(%"class.std::__cxx11::basic_string"* %tmp) #2
  ret void
}

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272), i8*) #0

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"*, i32) #0

declare void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_(%"class.std::__cxx11::basic_string"*, %"class.std::__cxx11::basic_string"* dereferenceable(32)) #0

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE(%"class.std::basic_ostream"* dereferenceable(272), %"class.std::__cxx11::basic_string"* dereferenceable(32)) #0

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"*, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)*) #0

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_(%"class.std::basic_ostream"* dereferenceable(272)) #0

define internal void @_GLOBAL__sub_I_Instrumentos.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }
attributes #3 = { uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
