; ModuleID = 'Heranca_Multipla.cpp'
target datalayout = "e-m:w-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-w64-windows-gnu"

%"class.std::ios_base::Init" = type { i8 }
%"class.std::basic_ostream" = type { i32 (...)**, %"class.std::basic_ios" }
%"class.std::basic_ios" = type { %"class.std::ios_base", %"class.std::basic_ostream"*, i8, i8, %"class.std::basic_streambuf"*, %"class.std::ctype"*, %"class.std::num_put"*, %"class.std::num_get"* }
%"class.std::ios_base" = type { i32 (...)**, i64, i64, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"class.std::locale" }
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"class.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type <{ i8*, i32, [4 x i8] }>
%"class.std::locale" = type { %"class.std::locale::_Impl"* }
%"class.std::locale::_Impl" = type { i32, %"class.std::locale::facet"**, i64, %"class.std::locale::facet"**, i8** }
%"class.std::locale::facet" = type <{ i32 (...)**, i32, [4 x i8] }>
%"class.std::basic_streambuf" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"class.std::locale" }
%"class.std::ctype" = type <{ %"class.std::locale::facet.base", [4 x i8], i32*, i8, [7 x i8], i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8, [6 x i8] }>
%"class.std::locale::facet.base" = type <{ i32 (...)**, i32 }>
%"class.std::num_put" = type { %"class.std::locale::facet.base", [4 x i8] }
%"class.std::num_get" = type { %"class.std::locale::facet.base", [4 x i8] }
%class.caminhao = type { %class.veiculo_rodoviario, %class.veiculo_A, %class.veiculo_B, i32 }
%class.veiculo_rodoviario = type { %class.veiculo_C, i32, i32 }
%class.veiculo_C = type { i32 }
%class.veiculo_A = type { i32 }
%class.veiculo_B = type { i32 }
%class.automovel = type { %class.veiculo_rodoviario, i32 }

$_ZN18veiculo_rodoviario9get_rodasEv = comdat any

$_ZN18veiculo_rodoviario8get_passEv = comdat any

$_ZN9automovel8get_tipoEv = comdat any

$_ZN9veiculo_A7A_set_AEi = comdat any

$_ZN9veiculo_A7A_get_AEv = comdat any

$_ZN9veiculo_B7B_set_BEi = comdat any

$_ZN9veiculo_B7B_get_BEv = comdat any

$_ZN9veiculo_C7C_set_CEi = comdat any

$_ZN9veiculo_C7C_get_CEv = comdat any

$_ZN18veiculo_rodoviario9set_rodasEi = comdat any

$_ZN18veiculo_rodoviario8set_passEi = comdat any

$_ZN8caminhao9set_cargaEi = comdat any

$_ZN9automovel8set_tipoE4tipo = comdat any

@_ZStL8__ioinit = internal global %"class.std::ios_base::Init" zeroinitializer, align 1
@_ZSt4cout = external global %"class.std::basic_ostream", align 8
@.str = private unnamed_addr constant [8 x i8] c"rodas: \00", align 1
@.str.1 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.2 = private unnamed_addr constant [14 x i8] c"passageiros: \00", align 1
@.str.3 = private unnamed_addr constant [31 x i8] c"carga (capacidade em litros): \00", align 1
@.str.4 = private unnamed_addr constant [7 x i8] c"tipo: \00", align 1
@.str.5 = private unnamed_addr constant [5 x i8] c"van\0A\00", align 1
@.str.6 = private unnamed_addr constant [7 x i8] c"carro\0A\00", align 1
@.str.7 = private unnamed_addr constant [7 x i8] c"vagao\0A\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_Heranca_Multipla.cpp, i8* null }]

define internal void @__cxx_global_var_init() #0 {
entry:
  call void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  %0 = call i32 @atexit(void ()* @__dtor__ZStL8__ioinit) #2
  ret void
}

declare void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"*) #0

; Function Attrs: nounwind
declare void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"*) #1

define internal void @__dtor__ZStL8__ioinit() #0 {
entry:
  call void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  ret void
}

; Function Attrs: nounwind
declare i32 @atexit(void ()*) #2

; Function Attrs: uwtable
define void @_ZN8caminhao7mostrarEv(%class.caminhao* %this) #3 align 2 {
entry:
  %this.addr = alloca %class.caminhao*, align 8
  store %class.caminhao* %this, %class.caminhao** %this.addr, align 8
  %this1 = load %class.caminhao*, %class.caminhao** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str, i32 0, i32 0))
  %0 = bitcast %class.caminhao* %this1 to %class.veiculo_rodoviario*
  %call2 = call i32 @_ZN18veiculo_rodoviario9get_rodasEv(%class.veiculo_rodoviario* %0)
  %call3 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call, i32 %call2)
  %call4 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call3, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.1, i32 0, i32 0))
  %call5 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.2, i32 0, i32 0))
  %1 = bitcast %class.caminhao* %this1 to %class.veiculo_rodoviario*
  %call6 = call i32 @_ZN18veiculo_rodoviario8get_passEv(%class.veiculo_rodoviario* %1)
  %call7 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call5, i32 %call6)
  %call8 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call7, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.1, i32 0, i32 0))
  %call9 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.3, i32 0, i32 0))
  %carga = getelementptr inbounds %class.caminhao, %class.caminhao* %this1, i32 0, i32 3
  %2 = load i32, i32* %carga, align 4
  %call10 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call9, i32 %2)
  %call11 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call10, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.1, i32 0, i32 0))
  ret void
}

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272), i8*) #0

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"*, i32) #0

; Function Attrs: nounwind uwtable
define linkonce_odr i32 @_ZN18veiculo_rodoviario9get_rodasEv(%class.veiculo_rodoviario* %this) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.veiculo_rodoviario*, align 8
  store %class.veiculo_rodoviario* %this, %class.veiculo_rodoviario** %this.addr, align 8
  %this1 = load %class.veiculo_rodoviario*, %class.veiculo_rodoviario** %this.addr
  %rodas = getelementptr inbounds %class.veiculo_rodoviario, %class.veiculo_rodoviario* %this1, i32 0, i32 1
  %0 = load i32, i32* %rodas, align 4
  ret i32 %0
}

; Function Attrs: nounwind uwtable
define linkonce_odr i32 @_ZN18veiculo_rodoviario8get_passEv(%class.veiculo_rodoviario* %this) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.veiculo_rodoviario*, align 8
  store %class.veiculo_rodoviario* %this, %class.veiculo_rodoviario** %this.addr, align 8
  %this1 = load %class.veiculo_rodoviario*, %class.veiculo_rodoviario** %this.addr
  %passageiros = getelementptr inbounds %class.veiculo_rodoviario, %class.veiculo_rodoviario* %this1, i32 0, i32 2
  %0 = load i32, i32* %passageiros, align 4
  ret i32 %0
}

; Function Attrs: uwtable
define void @_ZN9automovel7mostrarEv(%class.automovel* %this) #3 align 2 {
entry:
  %this.addr = alloca %class.automovel*, align 8
  store %class.automovel* %this, %class.automovel** %this.addr, align 8
  %this1 = load %class.automovel*, %class.automovel** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str, i32 0, i32 0))
  %0 = bitcast %class.automovel* %this1 to %class.veiculo_rodoviario*
  %call2 = call i32 @_ZN18veiculo_rodoviario9get_rodasEv(%class.veiculo_rodoviario* %0)
  %call3 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call, i32 %call2)
  %call4 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call3, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.1, i32 0, i32 0))
  %call5 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.2, i32 0, i32 0))
  %1 = bitcast %class.automovel* %this1 to %class.veiculo_rodoviario*
  %call6 = call i32 @_ZN18veiculo_rodoviario8get_passEv(%class.veiculo_rodoviario* %1)
  %call7 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call5, i32 %call6)
  %call8 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call7, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.1, i32 0, i32 0))
  %call9 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.4, i32 0, i32 0))
  %call10 = call i32 @_ZN9automovel8get_tipoEv(%class.automovel* %this1)
  switch i32 %call10, label %sw.epilog [
    i32 1, label %sw.bb
    i32 0, label %sw.bb.12
    i32 2, label %sw.bb.14
  ]

sw.bb:                                            ; preds = %entry
  %call11 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.5, i32 0, i32 0))
  br label %sw.epilog

sw.bb.12:                                         ; preds = %entry
  %call13 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.6, i32 0, i32 0))
  br label %sw.epilog

sw.bb.14:                                         ; preds = %entry
  %call15 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.7, i32 0, i32 0))
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.bb.14, %entry, %sw.bb.12, %sw.bb
  ret void
}

; Function Attrs: nounwind uwtable
define linkonce_odr i32 @_ZN9automovel8get_tipoEv(%class.automovel* %this) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.automovel*, align 8
  store %class.automovel* %this, %class.automovel** %this.addr, align 8
  %this1 = load %class.automovel*, %class.automovel** %this.addr
  %car_tipo = getelementptr inbounds %class.automovel, %class.automovel* %this1, i32 0, i32 1
  %0 = load i32, i32* %car_tipo, align 4
  ret i32 %0
}

; Function Attrs: uwtable
define i32 @main() #3 {
entry:
  %retval = alloca i32, align 4
  %t1 = alloca %class.caminhao, align 4
  %t2 = alloca %class.caminhao, align 4
  %c = alloca %class.automovel, align 4
  store i32 0, i32* %retval
  %0 = bitcast %class.caminhao* %t1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i64 12
  %1 = bitcast i8* %add.ptr to %class.veiculo_A*
  call void @_ZN9veiculo_A7A_set_AEi(%class.veiculo_A* %1, i32 1)
  %2 = bitcast %class.caminhao* %t1 to i8*
  %add.ptr1 = getelementptr inbounds i8, i8* %2, i64 12
  %3 = bitcast i8* %add.ptr1 to %class.veiculo_A*
  %call = call i32 @_ZN9veiculo_A7A_get_AEv(%class.veiculo_A* %3)
  %4 = bitcast %class.caminhao* %t1 to i8*
  %add.ptr2 = getelementptr inbounds i8, i8* %4, i64 16
  %5 = bitcast i8* %add.ptr2 to %class.veiculo_B*
  call void @_ZN9veiculo_B7B_set_BEi(%class.veiculo_B* %5, i32 2)
  %6 = bitcast %class.caminhao* %t1 to i8*
  %add.ptr3 = getelementptr inbounds i8, i8* %6, i64 16
  %7 = bitcast i8* %add.ptr3 to %class.veiculo_B*
  %call4 = call i32 @_ZN9veiculo_B7B_get_BEv(%class.veiculo_B* %7)
  %8 = bitcast %class.caminhao* %t1 to %class.veiculo_C*
  call void @_ZN9veiculo_C7C_set_CEi(%class.veiculo_C* %8, i32 3)
  %9 = bitcast %class.caminhao* %t1 to %class.veiculo_C*
  %call5 = call i32 @_ZN9veiculo_C7C_get_CEv(%class.veiculo_C* %9)
  %10 = bitcast %class.caminhao* %t1 to %class.veiculo_rodoviario*
  call void @_ZN18veiculo_rodoviario9set_rodasEi(%class.veiculo_rodoviario* %10, i32 18)
  %11 = bitcast %class.caminhao* %t1 to %class.veiculo_rodoviario*
  call void @_ZN18veiculo_rodoviario8set_passEi(%class.veiculo_rodoviario* %11, i32 2)
  call void @_ZN8caminhao9set_cargaEi(%class.caminhao* %t1, i32 3200)
  %12 = bitcast %class.caminhao* %t2 to %class.veiculo_rodoviario*
  call void @_ZN18veiculo_rodoviario9set_rodasEi(%class.veiculo_rodoviario* %12, i32 6)
  %13 = bitcast %class.caminhao* %t2 to %class.veiculo_rodoviario*
  call void @_ZN18veiculo_rodoviario8set_passEi(%class.veiculo_rodoviario* %13, i32 3)
  call void @_ZN8caminhao9set_cargaEi(%class.caminhao* %t2, i32 1200)
  call void @_ZN8caminhao7mostrarEv(%class.caminhao* %t1)
  %call6 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.1, i32 0, i32 0))
  call void @_ZN8caminhao7mostrarEv(%class.caminhao* %t2)
  %call7 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.1, i32 0, i32 0))
  %14 = bitcast %class.automovel* %c to %class.veiculo_rodoviario*
  call void @_ZN18veiculo_rodoviario9set_rodasEi(%class.veiculo_rodoviario* %14, i32 4)
  %15 = bitcast %class.automovel* %c to %class.veiculo_rodoviario*
  call void @_ZN18veiculo_rodoviario8set_passEi(%class.veiculo_rodoviario* %15, i32 6)
  call void @_ZN9automovel8set_tipoE4tipo(%class.automovel* %c, i32 1)
  call void @_ZN9automovel7mostrarEv(%class.automovel* %c)
  ret i32 0
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN9veiculo_A7A_set_AEi(%class.veiculo_A* %this, i32 %value) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.veiculo_A*, align 8
  %value.addr = alloca i32, align 4
  store %class.veiculo_A* %this, %class.veiculo_A** %this.addr, align 8
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %class.veiculo_A*, %class.veiculo_A** %this.addr
  %0 = load i32, i32* %value.addr, align 4
  %A_value = getelementptr inbounds %class.veiculo_A, %class.veiculo_A* %this1, i32 0, i32 0
  store i32 %0, i32* %A_value, align 4
  ret void
}

; Function Attrs: nounwind uwtable
define linkonce_odr i32 @_ZN9veiculo_A7A_get_AEv(%class.veiculo_A* %this) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.veiculo_A*, align 8
  store %class.veiculo_A* %this, %class.veiculo_A** %this.addr, align 8
  %this1 = load %class.veiculo_A*, %class.veiculo_A** %this.addr
  %A_value = getelementptr inbounds %class.veiculo_A, %class.veiculo_A* %this1, i32 0, i32 0
  %0 = load i32, i32* %A_value, align 4
  ret i32 %0
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN9veiculo_B7B_set_BEi(%class.veiculo_B* %this, i32 %value) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.veiculo_B*, align 8
  %value.addr = alloca i32, align 4
  store %class.veiculo_B* %this, %class.veiculo_B** %this.addr, align 8
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %class.veiculo_B*, %class.veiculo_B** %this.addr
  %0 = load i32, i32* %value.addr, align 4
  %B_value = getelementptr inbounds %class.veiculo_B, %class.veiculo_B* %this1, i32 0, i32 0
  store i32 %0, i32* %B_value, align 4
  ret void
}

; Function Attrs: nounwind uwtable
define linkonce_odr i32 @_ZN9veiculo_B7B_get_BEv(%class.veiculo_B* %this) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.veiculo_B*, align 8
  store %class.veiculo_B* %this, %class.veiculo_B** %this.addr, align 8
  %this1 = load %class.veiculo_B*, %class.veiculo_B** %this.addr
  %B_value = getelementptr inbounds %class.veiculo_B, %class.veiculo_B* %this1, i32 0, i32 0
  %0 = load i32, i32* %B_value, align 4
  ret i32 %0
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN9veiculo_C7C_set_CEi(%class.veiculo_C* %this, i32 %value) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.veiculo_C*, align 8
  %value.addr = alloca i32, align 4
  store %class.veiculo_C* %this, %class.veiculo_C** %this.addr, align 8
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %class.veiculo_C*, %class.veiculo_C** %this.addr
  %0 = load i32, i32* %value.addr, align 4
  %C_value = getelementptr inbounds %class.veiculo_C, %class.veiculo_C* %this1, i32 0, i32 0
  store i32 %0, i32* %C_value, align 4
  ret void
}

; Function Attrs: nounwind uwtable
define linkonce_odr i32 @_ZN9veiculo_C7C_get_CEv(%class.veiculo_C* %this) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.veiculo_C*, align 8
  store %class.veiculo_C* %this, %class.veiculo_C** %this.addr, align 8
  %this1 = load %class.veiculo_C*, %class.veiculo_C** %this.addr
  %C_value = getelementptr inbounds %class.veiculo_C, %class.veiculo_C* %this1, i32 0, i32 0
  %0 = load i32, i32* %C_value, align 4
  ret i32 %0
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN18veiculo_rodoviario9set_rodasEi(%class.veiculo_rodoviario* %this, i32 %num) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.veiculo_rodoviario*, align 8
  %num.addr = alloca i32, align 4
  store %class.veiculo_rodoviario* %this, %class.veiculo_rodoviario** %this.addr, align 8
  store i32 %num, i32* %num.addr, align 4
  %this1 = load %class.veiculo_rodoviario*, %class.veiculo_rodoviario** %this.addr
  %0 = load i32, i32* %num.addr, align 4
  %rodas = getelementptr inbounds %class.veiculo_rodoviario, %class.veiculo_rodoviario* %this1, i32 0, i32 1
  store i32 %0, i32* %rodas, align 4
  ret void
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN18veiculo_rodoviario8set_passEi(%class.veiculo_rodoviario* %this, i32 %num) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.veiculo_rodoviario*, align 8
  %num.addr = alloca i32, align 4
  store %class.veiculo_rodoviario* %this, %class.veiculo_rodoviario** %this.addr, align 8
  store i32 %num, i32* %num.addr, align 4
  %this1 = load %class.veiculo_rodoviario*, %class.veiculo_rodoviario** %this.addr
  %0 = load i32, i32* %num.addr, align 4
  %passageiros = getelementptr inbounds %class.veiculo_rodoviario, %class.veiculo_rodoviario* %this1, i32 0, i32 2
  store i32 %0, i32* %passageiros, align 4
  ret void
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN8caminhao9set_cargaEi(%class.caminhao* %this, i32 %size) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.caminhao*, align 8
  %size.addr = alloca i32, align 4
  store %class.caminhao* %this, %class.caminhao** %this.addr, align 8
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.caminhao*, %class.caminhao** %this.addr
  %0 = load i32, i32* %size.addr, align 4
  %carga = getelementptr inbounds %class.caminhao, %class.caminhao* %this1, i32 0, i32 3
  store i32 %0, i32* %carga, align 4
  ret void
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN9automovel8set_tipoE4tipo(%class.automovel* %this, i32 %t) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.automovel*, align 8
  %t.addr = alloca i32, align 4
  store %class.automovel* %this, %class.automovel** %this.addr, align 8
  store i32 %t, i32* %t.addr, align 4
  %this1 = load %class.automovel*, %class.automovel** %this.addr
  %0 = load i32, i32* %t.addr, align 4
  %car_tipo = getelementptr inbounds %class.automovel, %class.automovel* %this1, i32 0, i32 1
  store i32 %0, i32* %car_tipo, align 4
  ret void
}

define internal void @_GLOBAL__sub_I_Heranca_Multipla.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }
attributes #3 = { uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
