; ModuleID = 'Shape.cpp'
target datalayout = "e-m:w-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-w64-windows-gnu"

%"class.std::ios_base::Init" = type { i8 }
%"class.std::basic_ostream" = type { i32 (...)**, %"class.std::basic_ios" }
%"class.std::basic_ios" = type { %"class.std::ios_base", %"class.std::basic_ostream"*, i8, i8, %"class.std::basic_streambuf"*, %"class.std::ctype"*, %"class.std::num_put"*, %"class.std::num_get"* }
%"class.std::ios_base" = type { i32 (...)**, i64, i64, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"class.std::locale" }
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"class.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type <{ i8*, i32, [4 x i8] }>
%"class.std::locale" = type { %"class.std::locale::_Impl"* }
%"class.std::locale::_Impl" = type { i32, %"class.std::locale::facet"**, i64, %"class.std::locale::facet"**, i8** }
%"class.std::locale::facet" = type <{ i32 (...)**, i32, [4 x i8] }>
%"class.std::basic_streambuf" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"class.std::locale" }
%"class.std::ctype" = type <{ %"class.std::locale::facet.base", [4 x i8], i32*, i8, [7 x i8], i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8, [6 x i8] }>
%"class.std::locale::facet.base" = type <{ i32 (...)**, i32 }>
%"class.std::num_put" = type { %"class.std::locale::facet.base", [4 x i8] }
%"class.std::num_get" = type { %"class.std::locale::facet.base", [4 x i8] }
%class.Shape = type { i32 (...)**, i32, i32 }
%class.Rectangle = type { %class.Shape }
%class.Triangle = type { %class.Shape }

$_ZN9RectangleC2Eii = comdat any

$_ZN8TriangleC2Eii = comdat any

$_ZN5ShapeC2Eii = comdat any

$_ZN9Rectangle4areaEv = comdat any

$_ZN5Shape4areaEv = comdat any

$_ZN8Triangle4areaEv = comdat any

$_ZTV9Rectangle = comdat any

$_ZTS9Rectangle = comdat any

$_ZTS5Shape = comdat any

$_ZTI5Shape = comdat any

$_ZTI9Rectangle = comdat any

$_ZTV5Shape = comdat any

$_ZTV8Triangle = comdat any

$_ZTS8Triangle = comdat any

$_ZTI8Triangle = comdat any

@_ZStL8__ioinit = internal global %"class.std::ios_base::Init" zeroinitializer, align 1
@_ZSt4cout = external global %"class.std::basic_ostream", align 8
@.str = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@_ZTV9Rectangle = linkonce_odr unnamed_addr constant [3 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI9Rectangle to i8*), i8* bitcast (i32 (%class.Rectangle*)* @_ZN9Rectangle4areaEv to i8*)], comdat, align 8
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS9Rectangle = linkonce_odr constant [11 x i8] c"9Rectangle\00", comdat
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS5Shape = linkonce_odr constant [7 x i8] c"5Shape\00", comdat
@_ZTI5Shape = linkonce_odr constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @_ZTS5Shape, i32 0, i32 0) }, comdat
@_ZTI9Rectangle = linkonce_odr constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @_ZTS9Rectangle, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI5Shape to i8*) }, comdat
@_ZTV5Shape = linkonce_odr unnamed_addr constant [3 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI5Shape to i8*), i8* bitcast (i32 (%class.Shape*)* @_ZN5Shape4areaEv to i8*)], comdat, align 8
@.str.1 = private unnamed_addr constant [20 x i8] c"Parent class area :\00", align 1
@.str.2 = private unnamed_addr constant [23 x i8] c"Rectangle class area :\00", align 1
@_ZTV8Triangle = linkonce_odr unnamed_addr constant [3 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI8Triangle to i8*), i8* bitcast (i32 (%class.Triangle*)* @_ZN8Triangle4areaEv to i8*)], comdat, align 8
@_ZTS8Triangle = linkonce_odr constant [10 x i8] c"8Triangle\00", comdat
@_ZTI8Triangle = linkonce_odr constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @_ZTS8Triangle, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI5Shape to i8*) }, comdat
@.str.3 = private unnamed_addr constant [22 x i8] c"Triangle class area :\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_Shape.cpp, i8* null }]

define internal void @__cxx_global_var_init() #0 {
entry:
  call void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  %0 = call i32 @atexit(void ()* @__dtor__ZStL8__ioinit) #2
  ret void
}

declare void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"*) #0

; Function Attrs: nounwind
declare void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"*) #1

define internal void @__dtor__ZStL8__ioinit() #0 {
entry:
  call void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  ret void
}

; Function Attrs: nounwind
declare i32 @atexit(void ()*) #2

; Function Attrs: uwtable
define i32 @main() #3 {
entry:
  %retval = alloca i32, align 4
  %shape = alloca %class.Shape*, align 8
  %rec = alloca %class.Rectangle, align 8
  %tri = alloca %class.Triangle, align 8
  store i32 0, i32* %retval
  call void @_ZN9RectangleC2Eii(%class.Rectangle* %rec, i32 10, i32 7)
  call void @_ZN8TriangleC2Eii(%class.Triangle* %tri, i32 10, i32 5)
  %0 = bitcast %class.Rectangle* %rec to %class.Shape*
  store %class.Shape* %0, %class.Shape** %shape, align 8
  %1 = load %class.Shape*, %class.Shape** %shape, align 8
  %2 = bitcast %class.Shape* %1 to i32 (%class.Shape*)***
  %vtable = load i32 (%class.Shape*)**, i32 (%class.Shape*)*** %2
  %vfn = getelementptr inbounds i32 (%class.Shape*)*, i32 (%class.Shape*)** %vtable, i64 0
  %3 = load i32 (%class.Shape*)*, i32 (%class.Shape*)** %vfn
  %call = call i32 %3(%class.Shape* %1)
  %call1 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* @_ZSt4cout, i32 %call)
  %call2 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call1, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0))
  %4 = bitcast %class.Triangle* %tri to %class.Shape*
  store %class.Shape* %4, %class.Shape** %shape, align 8
  %5 = load %class.Shape*, %class.Shape** %shape, align 8
  %6 = bitcast %class.Shape* %5 to i32 (%class.Shape*)***
  %vtable3 = load i32 (%class.Shape*)**, i32 (%class.Shape*)*** %6
  %vfn4 = getelementptr inbounds i32 (%class.Shape*)*, i32 (%class.Shape*)** %vtable3, i64 0
  %7 = load i32 (%class.Shape*)*, i32 (%class.Shape*)** %vfn4
  %call5 = call i32 %7(%class.Shape* %5)
  %call6 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* @_ZSt4cout, i32 %call5)
  %call7 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) %call6, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str, i32 0, i32 0))
  ret i32 0
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN9RectangleC2Eii(%class.Rectangle* %this, i32 %a, i32 %b) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.Rectangle*, align 8
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  store %class.Rectangle* %this, %class.Rectangle** %this.addr, align 8
  store i32 %a, i32* %a.addr, align 4
  store i32 %b, i32* %b.addr, align 4
  %this1 = load %class.Rectangle*, %class.Rectangle** %this.addr
  %0 = bitcast %class.Rectangle* %this1 to %class.Shape*
  %1 = load i32, i32* %a.addr, align 4
  %2 = load i32, i32* %b.addr, align 4
  call void @_ZN5ShapeC2Eii(%class.Shape* %0, i32 %1, i32 %2)
  %3 = bitcast %class.Rectangle* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([3 x i8*], [3 x i8*]* @_ZTV9Rectangle, i64 0, i64 2) to i32 (...)**), i32 (...)*** %3
  ret void
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN8TriangleC2Eii(%class.Triangle* %this, i32 %a, i32 %b) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.Triangle*, align 8
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  store %class.Triangle* %this, %class.Triangle** %this.addr, align 8
  store i32 %a, i32* %a.addr, align 4
  store i32 %b, i32* %b.addr, align 4
  %this1 = load %class.Triangle*, %class.Triangle** %this.addr
  %0 = bitcast %class.Triangle* %this1 to %class.Shape*
  %1 = load i32, i32* %a.addr, align 4
  %2 = load i32, i32* %b.addr, align 4
  call void @_ZN5ShapeC2Eii(%class.Shape* %0, i32 %1, i32 %2)
  %3 = bitcast %class.Triangle* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([3 x i8*], [3 x i8*]* @_ZTV8Triangle, i64 0, i64 2) to i32 (...)**), i32 (...)*** %3
  ret void
}

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272), i8*) #0

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"*, i32) #0

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN5ShapeC2Eii(%class.Shape* %this, i32 %a, i32 %b) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.Shape*, align 8
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  store %class.Shape* %this, %class.Shape** %this.addr, align 8
  store i32 %a, i32* %a.addr, align 4
  store i32 %b, i32* %b.addr, align 4
  %this1 = load %class.Shape*, %class.Shape** %this.addr
  %0 = bitcast %class.Shape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([3 x i8*], [3 x i8*]* @_ZTV5Shape, i64 0, i64 2) to i32 (...)**), i32 (...)*** %0
  %1 = load i32, i32* %a.addr, align 4
  %width = getelementptr inbounds %class.Shape, %class.Shape* %this1, i32 0, i32 1
  store i32 %1, i32* %width, align 4
  %2 = load i32, i32* %b.addr, align 4
  %height = getelementptr inbounds %class.Shape, %class.Shape* %this1, i32 0, i32 2
  store i32 %2, i32* %height, align 4
  ret void
}

; Function Attrs: uwtable
define linkonce_odr i32 @_ZN9Rectangle4areaEv(%class.Rectangle* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.Rectangle*, align 8
  store %class.Rectangle* %this, %class.Rectangle** %this.addr, align 8
  %this1 = load %class.Rectangle*, %class.Rectangle** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.2, i32 0, i32 0))
  %call2 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* %call, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
  %0 = bitcast %class.Rectangle* %this1 to %class.Shape*
  %width = getelementptr inbounds %class.Shape, %class.Shape* %0, i32 0, i32 1
  %1 = load i32, i32* %width, align 4
  %2 = bitcast %class.Rectangle* %this1 to %class.Shape*
  %height = getelementptr inbounds %class.Shape, %class.Shape* %2, i32 0, i32 2
  %3 = load i32, i32* %height, align 4
  %mul = mul nsw i32 %1, %3
  ret i32 %mul
}

; Function Attrs: uwtable
define linkonce_odr i32 @_ZN5Shape4areaEv(%class.Shape* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.Shape*, align 8
  store %class.Shape* %this, %class.Shape** %this.addr, align 8
  %this1 = load %class.Shape*, %class.Shape** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.1, i32 0, i32 0))
  %call2 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* %call, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
  ret i32 0
}

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"*, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)*) #0

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_(%"class.std::basic_ostream"* dereferenceable(272)) #0

; Function Attrs: uwtable
define linkonce_odr i32 @_ZN8Triangle4areaEv(%class.Triangle* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.Triangle*, align 8
  store %class.Triangle* %this, %class.Triangle** %this.addr, align 8
  %this1 = load %class.Triangle*, %class.Triangle** %this.addr
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.3, i32 0, i32 0))
  %call2 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* %call, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
  %0 = bitcast %class.Triangle* %this1 to %class.Shape*
  %width = getelementptr inbounds %class.Shape, %class.Shape* %0, i32 0, i32 1
  %1 = load i32, i32* %width, align 4
  %2 = bitcast %class.Triangle* %this1 to %class.Shape*
  %height = getelementptr inbounds %class.Shape, %class.Shape* %2, i32 0, i32 2
  %3 = load i32, i32* %height, align 4
  %mul = mul nsw i32 %1, %3
  %div = sdiv i32 %mul, 2
  ret i32 %div
}

define internal void @_GLOBAL__sub_I_Shape.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }
attributes #3 = { uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
