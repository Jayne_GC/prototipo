; ModuleID = 'Calculadora.cpp'
target datalayout = "e-m:w-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-w64-windows-gnu"

%"class.std::ios_base::Init" = type { i8 }
%"class.std::basic_ostream" = type { i32 (...)**, %"class.std::basic_ios" }
%"class.std::basic_ios" = type { %"class.std::ios_base", %"class.std::basic_ostream"*, i8, i8, %"class.std::basic_streambuf"*, %"class.std::ctype"*, %"class.std::num_put"*, %"class.std::num_get"* }
%"class.std::ios_base" = type { i32 (...)**, i64, i64, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"class.std::locale" }
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"class.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type <{ i8*, i32, [4 x i8] }>
%"class.std::locale" = type { %"class.std::locale::_Impl"* }
%"class.std::locale::_Impl" = type { i32, %"class.std::locale::facet"**, i64, %"class.std::locale::facet"**, i8** }
%"class.std::locale::facet" = type <{ i32 (...)**, i32, [4 x i8] }>
%"class.std::basic_streambuf" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"class.std::locale" }
%"class.std::ctype" = type <{ %"class.std::locale::facet.base", [4 x i8], i32*, i8, [7 x i8], i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8, [6 x i8] }>
%"class.std::locale::facet.base" = type <{ i32 (...)**, i32 }>
%"class.std::num_put" = type { %"class.std::locale::facet.base", [4 x i8] }
%"class.std::num_get" = type { %"class.std::locale::facet.base", [4 x i8] }
%"class.std::basic_istream" = type { i32 (...)**, i64, %"class.std::basic_ios" }
%class.triangle = type { %class.shape }
%class.shape = type { i32 (...)**, double, double }
%class.rectangle = type { %class.shape }

$_ZN8triangleC2Edd = comdat any

$_ZN9rectangleC2Edd = comdat any

$_ZN5shapeC2Edd = comdat any

$_ZTS5shape = comdat any

$_ZTI5shape = comdat any

$_ZTV5shape = comdat any

@_ZStL8__ioinit = internal global %"class.std::ios_base::Init" zeroinitializer, align 1
@_ZSt4cout = external global %"class.std::basic_ostream", align 8
@.str = private unnamed_addr constant [27 x i8] c"\0ATHE AREA OF TRIANGLE IS :\00", align 1
@.str.1 = private unnamed_addr constant [28 x i8] c"\0ATHE AREA OF RECTANGLE IS :\00", align 1
@.str.2 = private unnamed_addr constant [37 x i8] c"\0A\0APLEASE ENTER THE TRIANGLE DETAILS\0A\00", align 1
@.str.3 = private unnamed_addr constant [16 x i8] c"BASE         = \00", align 1
@_ZSt3cin = external global %"class.std::basic_istream", align 8
@.str.4 = private unnamed_addr constant [17 x i8] c"\0AHYPOTENUSE   = \00", align 1
@.str.5 = private unnamed_addr constant [38 x i8] c"\0A\0APLEASE ENTER THE RECTANGLE DETAILS\0A\00", align 1
@.str.6 = private unnamed_addr constant [12 x i8] c"\0ALENGTH  = \00", align 1
@.str.7 = private unnamed_addr constant [12 x i8] c"\0ABREADTH = \00", align 1
@.str.8 = private unnamed_addr constant [34 x i8] c"\0A\0A\0A\0A\0A------------MENU------------\00", align 1
@.str.9 = private unnamed_addr constant [19 x i8] c"\0A\0A1. TRIANGLE AREA\00", align 1
@.str.10 = private unnamed_addr constant [19 x i8] c"\0A2. RECTANGLE AREA\00", align 1
@.str.11 = private unnamed_addr constant [9 x i8] c"\0A3. EXIT\00", align 1
@.str.12 = private unnamed_addr constant [21 x i8] c"\0AENTER YOUR CHOICE :\00", align 1
@.str.13 = private unnamed_addr constant [16 x i8] c"\0AINVALID CHOICE\00", align 1
@_ZTV8triangle = unnamed_addr constant [3 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI8triangle to i8*), i8* bitcast (void (%class.triangle*)* @_ZN8triangle9disp_areaEv to i8*)], align 8
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS8triangle = constant [10 x i8] c"8triangle\00"
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS5shape = linkonce_odr constant [7 x i8] c"5shape\00", comdat
@_ZTI5shape = linkonce_odr constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @_ZTS5shape, i32 0, i32 0) }, comdat
@_ZTI8triangle = constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @_ZTS8triangle, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI5shape to i8*) }
@_ZTV9rectangle = unnamed_addr constant [3 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI9rectangle to i8*), i8* bitcast (void (%class.rectangle*)* @_ZN9rectangle9disp_areaEv to i8*)], align 8
@_ZTS9rectangle = constant [11 x i8] c"9rectangle\00"
@_ZTI9rectangle = constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @_ZTS9rectangle, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI5shape to i8*) }
@_ZTV5shape = linkonce_odr unnamed_addr constant [3 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI5shape to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)], comdat, align 8
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_Calculadora.cpp, i8* null }]

define internal void @__cxx_global_var_init() #0 {
entry:
  call void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  %0 = call i32 @atexit(void ()* @__dtor__ZStL8__ioinit) #2
  ret void
}

declare void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"*) #0

; Function Attrs: nounwind
declare void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"*) #1

define internal void @__dtor__ZStL8__ioinit() #0 {
entry:
  call void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  ret void
}

; Function Attrs: nounwind
declare i32 @atexit(void ()*) #2

; Function Attrs: uwtable
define void @_ZN8triangle9disp_areaEv(%class.triangle* %this) unnamed_addr #3 align 2 {
entry:
  %this.addr = alloca %class.triangle*, align 8
  %a1 = alloca double, align 8
  store %class.triangle* %this, %class.triangle** %this.addr, align 8
  %this1 = load %class.triangle*, %class.triangle** %this.addr
  %0 = bitcast %class.triangle* %this1 to %class.shape*
  %b = getelementptr inbounds %class.shape, %class.shape* %0, i32 0, i32 1
  %1 = load double, double* %b, align 8
  %2 = bitcast %class.triangle* %this1 to %class.shape*
  %h = getelementptr inbounds %class.shape, %class.shape* %2, i32 0, i32 2
  %3 = load double, double* %h, align 8
  %mul = fmul double %1, %3
  %mul2 = fmul double %mul, 1.000000e+00
  %div = fdiv double %mul2, 2.000000e+00
  store double %div, double* %a1, align 8
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0))
  %4 = load double, double* %a1, align 8
  %call3 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEd(%"class.std::basic_ostream"* %call, double %4)
  %call4 = call i32 @getch()
  ret void
}

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272), i8*) #0

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEd(%"class.std::basic_ostream"*, double) #0

declare i32 @getch() #0

; Function Attrs: uwtable
define void @_ZN9rectangle9disp_areaEv(%class.rectangle* %this) unnamed_addr #3 align 2 {
entry:
  %this.addr = alloca %class.rectangle*, align 8
  %a1 = alloca double, align 8
  store %class.rectangle* %this, %class.rectangle** %this.addr, align 8
  %this1 = load %class.rectangle*, %class.rectangle** %this.addr
  %0 = bitcast %class.rectangle* %this1 to %class.shape*
  %b = getelementptr inbounds %class.shape, %class.shape* %0, i32 0, i32 1
  %1 = load double, double* %b, align 8
  %2 = bitcast %class.rectangle* %this1 to %class.shape*
  %h = getelementptr inbounds %class.shape, %class.shape* %2, i32 0, i32 2
  %3 = load double, double* %h, align 8
  %mul = fmul double %1, %3
  store double %mul, double* %a1, align 8
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.1, i32 0, i32 0))
  %4 = load double, double* %a1, align 8
  %call2 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEd(%"class.std::basic_ostream"* %call, double %4)
  %call3 = call i32 @getch()
  ret void
}

; Function Attrs: uwtable
define i32 @main() #3 {
entry:
  %retval = alloca i32, align 4
  %b1 = alloca double, align 8
  %h1 = alloca double, align 8
  %t1 = alloca %class.triangle, align 8
  %r1 = alloca %class.rectangle, align 8
  %list = alloca [2 x %class.shape*], align 16
  %ans = alloca i32, align 4
  store i32 0, i32* %retval
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.2, i32 0, i32 0))
  %call1 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.3, i32 0, i32 0))
  %call2 = call dereferenceable(280) %"class.std::basic_istream"* @_ZNSirsERd(%"class.std::basic_istream"* @_ZSt3cin, double* dereferenceable(8) %b1)
  %call3 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.4, i32 0, i32 0))
  %call4 = call dereferenceable(280) %"class.std::basic_istream"* @_ZNSirsERd(%"class.std::basic_istream"* @_ZSt3cin, double* dereferenceable(8) %h1)
  %0 = load double, double* %b1, align 8
  %1 = load double, double* %h1, align 8
  call void @_ZN8triangleC2Edd(%class.triangle* %t1, double %0, double %1)
  %call5 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.5, i32 0, i32 0))
  %call6 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.6, i32 0, i32 0))
  %call7 = call dereferenceable(280) %"class.std::basic_istream"* @_ZNSirsERd(%"class.std::basic_istream"* @_ZSt3cin, double* dereferenceable(8) %b1)
  %call8 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.7, i32 0, i32 0))
  %call9 = call dereferenceable(280) %"class.std::basic_istream"* @_ZNSirsERd(%"class.std::basic_istream"* @_ZSt3cin, double* dereferenceable(8) %h1)
  %2 = load double, double* %b1, align 8
  %3 = load double, double* %h1, align 8
  call void @_ZN9rectangleC2Edd(%class.rectangle* %r1, double %2, double %3)
  %4 = bitcast %class.triangle* %t1 to %class.shape*
  %arrayidx = getelementptr inbounds [2 x %class.shape*], [2 x %class.shape*]* %list, i32 0, i64 0
  store %class.shape* %4, %class.shape** %arrayidx, align 8
  %5 = bitcast %class.rectangle* %r1 to %class.shape*
  %arrayidx10 = getelementptr inbounds [2 x %class.shape*], [2 x %class.shape*]* %list, i32 0, i64 1
  store %class.shape* %5, %class.shape** %arrayidx10, align 8
  br label %while.body

while.body:                                       ; preds = %entry, %if.else.26, %if.end.29
  %call11 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.8, i32 0, i32 0))
  %call12 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.9, i32 0, i32 0))
  %call13 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.10, i32 0, i32 0))
  %call14 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.11, i32 0, i32 0))
  %call15 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.12, i32 0, i32 0))
  %call16 = call dereferenceable(280) %"class.std::basic_istream"* @_ZNSirsERi(%"class.std::basic_istream"* @_ZSt3cin, i32* dereferenceable(4) %ans)
  %6 = load i32, i32* %ans, align 4
  %cmp = icmp eq i32 %6, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %arrayidx17 = getelementptr inbounds [2 x %class.shape*], [2 x %class.shape*]* %list, i32 0, i64 0
  %7 = load %class.shape*, %class.shape** %arrayidx17, align 8
  %8 = bitcast %class.shape* %7 to void (%class.shape*)***
  %vtable = load void (%class.shape*)**, void (%class.shape*)*** %8
  %vfn = getelementptr inbounds void (%class.shape*)*, void (%class.shape*)** %vtable, i64 0
  %9 = load void (%class.shape*)*, void (%class.shape*)** %vfn
  call void %9(%class.shape* %7)
  br label %if.end.29

if.else:                                          ; preds = %while.body
  %10 = load i32, i32* %ans, align 4
  %cmp18 = icmp eq i32 %10, 2
  br i1 %cmp18, label %if.then.19, label %if.else.23

if.then.19:                                       ; preds = %if.else
  %arrayidx20 = getelementptr inbounds [2 x %class.shape*], [2 x %class.shape*]* %list, i32 0, i64 1
  %11 = load %class.shape*, %class.shape** %arrayidx20, align 8
  %12 = bitcast %class.shape* %11 to void (%class.shape*)***
  %vtable21 = load void (%class.shape*)**, void (%class.shape*)*** %12
  %vfn22 = getelementptr inbounds void (%class.shape*)*, void (%class.shape*)** %vtable21, i64 0
  %13 = load void (%class.shape*)*, void (%class.shape*)** %vfn22
  call void %13(%class.shape* %11)
  br label %if.end

if.else.23:                                       ; preds = %if.else
  %14 = load i32, i32* %ans, align 4
  %cmp24 = icmp eq i32 %14, 3
  br i1 %cmp24, label %if.then.25, label %if.else.26

if.then.25:                                       ; preds = %if.else.23
  call void @exit(i32 1) #6
  unreachable

if.else.26:                                       ; preds = %if.else.23
  %call27 = call dereferenceable(272) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(272) @_ZSt4cout, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.13, i32 0, i32 0))
  %call28 = call i32 @getch()
  br label %while.body

if.end:                                           ; preds = %if.then.19
  br label %if.end.29

if.end.29:                                        ; preds = %if.end, %if.then
  br label %while.body

return:                                           ; No predecessors!
  %15 = load i32, i32* %retval
  ret i32 %15
}

declare dereferenceable(280) %"class.std::basic_istream"* @_ZNSirsERd(%"class.std::basic_istream"*, double* dereferenceable(8)) #0

; Function Attrs: uwtable
define linkonce_odr void @_ZN8triangleC2Edd(%class.triangle* %this, double %b1, double %h1) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.triangle*, align 8
  %b1.addr = alloca double, align 8
  %h1.addr = alloca double, align 8
  store %class.triangle* %this, %class.triangle** %this.addr, align 8
  store double %b1, double* %b1.addr, align 8
  store double %h1, double* %h1.addr, align 8
  %this1 = load %class.triangle*, %class.triangle** %this.addr
  %0 = bitcast %class.triangle* %this1 to %class.shape*
  %1 = load double, double* %b1.addr, align 8
  %2 = load double, double* %h1.addr, align 8
  call void @_ZN5shapeC2Edd(%class.shape* %0, double %1, double %2)
  %3 = bitcast %class.triangle* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([3 x i8*], [3 x i8*]* @_ZTV8triangle, i64 0, i64 2) to i32 (...)**), i32 (...)*** %3
  ret void
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN9rectangleC2Edd(%class.rectangle* %this, double %b1, double %h1) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.rectangle*, align 8
  %b1.addr = alloca double, align 8
  %h1.addr = alloca double, align 8
  store %class.rectangle* %this, %class.rectangle** %this.addr, align 8
  store double %b1, double* %b1.addr, align 8
  store double %h1, double* %h1.addr, align 8
  %this1 = load %class.rectangle*, %class.rectangle** %this.addr
  %0 = bitcast %class.rectangle* %this1 to %class.shape*
  %1 = load double, double* %b1.addr, align 8
  %2 = load double, double* %h1.addr, align 8
  call void @_ZN5shapeC2Edd(%class.shape* %0, double %1, double %2)
  %3 = bitcast %class.rectangle* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([3 x i8*], [3 x i8*]* @_ZTV9rectangle, i64 0, i64 2) to i32 (...)**), i32 (...)*** %3
  ret void
}

declare dereferenceable(280) %"class.std::basic_istream"* @_ZNSirsERi(%"class.std::basic_istream"*, i32* dereferenceable(4)) #0

; Function Attrs: noreturn nounwind
declare void @exit(i32) #5

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN5shapeC2Edd(%class.shape* %this, double %b1, double %h1) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.shape*, align 8
  %b1.addr = alloca double, align 8
  %h1.addr = alloca double, align 8
  store %class.shape* %this, %class.shape** %this.addr, align 8
  store double %b1, double* %b1.addr, align 8
  store double %h1, double* %h1.addr, align 8
  %this1 = load %class.shape*, %class.shape** %this.addr
  %0 = bitcast %class.shape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([3 x i8*], [3 x i8*]* @_ZTV5shape, i64 0, i64 2) to i32 (...)**), i32 (...)*** %0
  %1 = load double, double* %b1.addr, align 8
  %b = getelementptr inbounds %class.shape, %class.shape* %this1, i32 0, i32 1
  store double %1, double* %b, align 8
  %2 = load double, double* %h1.addr, align 8
  %h = getelementptr inbounds %class.shape, %class.shape* %this1, i32 0, i32 2
  store double %2, double* %h, align 8
  ret void
}

declare void @__cxa_pure_virtual()

define internal void @_GLOBAL__sub_I_Calculadora.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }
attributes #3 = { uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
