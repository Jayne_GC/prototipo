; ModuleID = 'IISD.cpp'
target datalayout = "e-m:w-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-w64-windows-gnu"

%"class.std::ios_base::Init" = type { i8 }
%class.D = type { %class.T }
%class.T = type { i32 (...)**, i32, i32 }

$_ZN1DC2Ev = comdat any

$_ZN1D1mEv = comdat any

$_ZN1TC2Ev = comdat any

$_ZN1T1mEv = comdat any

$_ZN1D1eEv = comdat any

$_ZTV1D = comdat any

$_ZTS1D = comdat any

$_ZTS1T = comdat any

$_ZTI1T = comdat any

$_ZTI1D = comdat any

$_ZTV1T = comdat any

@_ZStL8__ioinit = internal global %"class.std::ios_base::Init" zeroinitializer, align 1
@_ZTV1D = linkonce_odr unnamed_addr constant [3 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI1D to i8*), i8* bitcast (void (%class.D*)* @_ZN1D1mEv to i8*)], comdat, align 8
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS1D = linkonce_odr constant [3 x i8] c"1D\00", comdat
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS1T = linkonce_odr constant [3 x i8] c"1T\00", comdat
@_ZTI1T = linkonce_odr constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([3 x i8], [3 x i8]* @_ZTS1T, i32 0, i32 0) }, comdat
@_ZTI1D = linkonce_odr constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i64 2) to i8*), i8* getelementptr inbounds ([3 x i8], [3 x i8]* @_ZTS1D, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI1T to i8*) }, comdat
@_ZTV1T = linkonce_odr unnamed_addr constant [3 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI1T to i8*), i8* bitcast (void (%class.T*)* @_ZN1T1mEv to i8*)], comdat, align 8
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_IISD.cpp, i8* null }]

define internal void @__cxx_global_var_init() #0 {
entry:
  call void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  %0 = call i32 @atexit(void ()* @__dtor__ZStL8__ioinit) #2
  ret void
}

declare void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"*) #0

; Function Attrs: nounwind
declare void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"*) #1

define internal void @__dtor__ZStL8__ioinit() #0 {
entry:
  call void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  ret void
}

; Function Attrs: nounwind
declare i32 @atexit(void ()*) #2

; Function Attrs: uwtable
define i32 @main() #3 {
entry:
  %retval = alloca i32, align 4
  %obj_d = alloca %class.D, align 8
  store i32 0, i32* %retval
  call void @_ZN1DC2Ev(%class.D* %obj_d) #2
  call void @_ZN1D1mEv(%class.D* %obj_d)
  ret i32 0
}

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN1DC2Ev(%class.D* %this) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.D*, align 8
  store %class.D* %this, %class.D** %this.addr, align 8
  %this1 = load %class.D*, %class.D** %this.addr
  %0 = bitcast %class.D* %this1 to %class.T*
  call void @_ZN1TC2Ev(%class.T* %0) #2
  %1 = bitcast %class.D* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([3 x i8*], [3 x i8*]* @_ZTV1D, i64 0, i64 2) to i32 (...)**), i32 (...)*** %1
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN1D1mEv(%class.D* %this) unnamed_addr #3 comdat align 2 {
entry:
  %this.addr = alloca %class.D*, align 8
  store %class.D* %this, %class.D** %this.addr, align 8
  %this1 = load %class.D*, %class.D** %this.addr
  call void @_ZN1D1eEv(%class.D* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind uwtable
define linkonce_odr void @_ZN1TC2Ev(%class.T* %this) unnamed_addr #4 comdat align 2 {
entry:
  %this.addr = alloca %class.T*, align 8
  store %class.T* %this, %class.T** %this.addr, align 8
  %this1 = load %class.T*, %class.T** %this.addr
  %0 = bitcast %class.T* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ([3 x i8*], [3 x i8*]* @_ZTV1T, i64 0, i64 2) to i32 (...)**), i32 (...)*** %0
  ret void
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN1T1mEv(%class.T* %this) unnamed_addr #5 comdat align 2 {
entry:
  %this.addr = alloca %class.T*, align 8
  store %class.T* %this, %class.T** %this.addr, align 8
  %this1 = load %class.T*, %class.T** %this.addr
  %x = getelementptr inbounds %class.T, %class.T* %this1, i32 0, i32 1
  store i32 1, i32* %x, align 4
  ret void
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN1D1eEv(%class.D* %this) #5 comdat align 2 {
entry:
  %this.addr = alloca %class.D*, align 8
  store %class.D* %this, %class.D** %this.addr, align 8
  %this1 = load %class.D*, %class.D** %this.addr
  %0 = bitcast %class.D* %this1 to %class.T*
  %y = getelementptr inbounds %class.T, %class.T* %0, i32 0, i32 2
  store i32 2, i32* %y, align 4
  ret void
}

define internal void @_GLOBAL__sub_I_IISD.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }
attributes #3 = { uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
