; ModuleID = 'Atributos.cpp'
target datalayout = "e-m:w-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-w64-windows-gnu"

%"class.std::ios_base::Init" = type { i8 }
%"class.std::basic_ostream" = type { i32 (...)**, %"class.std::basic_ios" }
%"class.std::basic_ios" = type { %"class.std::ios_base", %"class.std::basic_ostream"*, i8, i8, %"class.std::basic_streambuf"*, %"class.std::ctype"*, %"class.std::num_put"*, %"class.std::num_get"* }
%"class.std::ios_base" = type { i32 (...)**, i64, i64, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"class.std::locale" }
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"class.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type <{ i8*, i32, [4 x i8] }>
%"class.std::locale" = type { %"class.std::locale::_Impl"* }
%"class.std::locale::_Impl" = type { i32, %"class.std::locale::facet"**, i64, %"class.std::locale::facet"**, i8** }
%"class.std::locale::facet" = type <{ i32 (...)**, i32, [4 x i8] }>
%"class.std::basic_streambuf" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"class.std::locale" }
%"class.std::ctype" = type <{ %"class.std::locale::facet.base", [4 x i8], i32*, i8, [7 x i8], i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8, [6 x i8] }>
%"class.std::locale::facet.base" = type <{ i32 (...)**, i32 }>
%"class.std::num_put" = type { %"class.std::locale::facet.base", [4 x i8] }
%"class.std::num_get" = type { %"class.std::locale::facet.base", [4 x i8] }
%class.Y = type { %class.X, i32, i32, i32 }
%class.X = type { %class.W, i32 }
%class.W = type { i32, i32, i32, i32 }

$_ZN1Y1mEv = comdat any

$_ZN1X1nEv = comdat any

$_ZN1Y1oEv = comdat any

$_ZN1Y1pEv = comdat any

@_ZStL8__ioinit = internal global %"class.std::ios_base::Init" zeroinitializer, align 1
@_ZSt4cout = external global %"class.std::basic_ostream", align 8
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_Atributos.cpp, i8* null }]

define internal void @__cxx_global_var_init() #0 {
entry:
  call void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  %0 = call i32 @atexit(void ()* @__dtor__ZStL8__ioinit) #2
  ret void
}

declare void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"*) #0

; Function Attrs: nounwind
declare void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"*) #1

define internal void @__dtor__ZStL8__ioinit() #0 {
entry:
  call void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  ret void
}

; Function Attrs: nounwind
declare i32 @atexit(void ()*) #2

; Function Attrs: uwtable
define i32 @main() #3 {
entry:
  %retval = alloca i32, align 4
  %obj_y = alloca %class.Y, align 4
  store i32 0, i32* %retval
  call void @_ZN1Y1mEv(%class.Y* %obj_y)
  %0 = bitcast %class.Y* %obj_y to %class.X*
  call void @_ZN1X1nEv(%class.X* %0)
  %v1 = getelementptr inbounds %class.Y, %class.Y* %obj_y, i32 0, i32 3
  %1 = load i32, i32* %v1, align 4
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  call void @_ZN1Y1oEv(%class.Y* %obj_y)
  br label %if.end

if.else:                                          ; preds = %entry
  call void @_ZN1Y1pEv(%class.Y* %obj_y)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret i32 0
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN1Y1mEv(%class.Y* %this) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.Y*, align 8
  store %class.Y* %this, %class.Y** %this.addr, align 8
  %this1 = load %class.Y*, %class.Y** %this.addr
  %v = getelementptr inbounds %class.Y, %class.Y* %this1, i32 0, i32 1
  store i32 2, i32* %v, align 4
  ret void
}

; Function Attrs: uwtable
define linkonce_odr void @_ZN1X1nEv(%class.X* %this) #3 comdat align 2 {
entry:
  %this.addr = alloca %class.X*, align 8
  store %class.X* %this, %class.X** %this.addr, align 8
  %this1 = load %class.X*, %class.X** %this.addr
  %0 = bitcast %class.X* %this1 to %class.W*
  %v = getelementptr inbounds %class.W, %class.W* %0, i32 0, i32 1
  %1 = load i32, i32* %v, align 4
  %call = call dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* @_ZSt4cout, i32 %1)
  ret void
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN1Y1oEv(%class.Y* %this) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.Y*, align 8
  store %class.Y* %this, %class.Y** %this.addr, align 8
  %this1 = load %class.Y*, %class.Y** %this.addr
  %v1 = getelementptr inbounds %class.Y, %class.Y* %this1, i32 0, i32 3
  store i32 2, i32* %v1, align 4
  ret void
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN1Y1pEv(%class.Y* %this) #4 comdat align 2 {
entry:
  %this.addr = alloca %class.Y*, align 8
  store %class.Y* %this, %class.Y** %this.addr, align 8
  %this1 = load %class.Y*, %class.Y** %this.addr
  %v2 = getelementptr inbounds %class.Y, %class.Y* %this1, i32 0, i32 2
  store i32 2, i32* %v2, align 4
  ret void
}

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"*, i32) #0

define internal void @_GLOBAL__sub_I_Atributos.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }
attributes #3 = { uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
