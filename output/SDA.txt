
SDA - ANOMALIA DE DEFINI��O DE ESTADO
� uma anomalia que ocorre quando as intera��es de estado de um descendente n�o s�o consistentes com as do ancestral, ou seja, quando os m�todos da classe herdeira n�o fornecem as mesmas defini��es para as vari�veis de estado dos m�todos da classe herdada que foram sobrescritos.
SA�DA: Foram comparados todos os m�todos polim�rficos existentes entre si, os que apresentam diverg�ncias quanto �s defini��es ou chamadas de fun��es ser�o exibidos abaixo.
TESTE: Os seguintes m�todos podem apresentar esta anomalia entre si e devem ser testados a fim de verificar as intera��es de estado.

------------------------------------------------------------------------------------------

Poss�vel SDA nos seguintes metodos polimorficos entre si: 
_ZN1A1jEv _ZN1C1jEv 
Os metodos diferem quanto as CHAMADAS, por favor conferir as informacoes abaixo: 
	Metodo: _ZN1A1jEv
	Metodo: _ZN1C1jEv
		Tipo: Chamada -    	Metodo_Chamado: _ZN1B1kEv   	Metodo_Chamado_Classe: B   	Objeto_nome: this_value   	Objeto_classe: C
